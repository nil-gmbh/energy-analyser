package data;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;

import com.opencsv.CSVReader;

import exceptions.DateException;
import exceptions.Exceptions;
import exceptions.ValueException;

/**
 * CSVFiles' storage. SINGLETON
 * @author Panda
 *
 */
public class CsvData {
	
	private static CsvData singleton = new CsvData(); 
	private CsvData(){}
	public static CsvData getInstance(){
		return singleton;
	}
	
	private int currentLineNumber = 0;
	private ArrayList<ProjectRecord> projectRecords = new ArrayList<ProjectRecord>();
	private List<String> csvFileLines = new ArrayList<String>();
	public final static List<SimpleDateFormat> dateFormats = new ArrayList<SimpleDateFormat>() {
		{
			add(new SimpleDateFormat("dd.MM.yyyy"));
			add(new SimpleDateFormat("dd-MM-yyyy"));
			add(new SimpleDateFormat("dd/MM/yyyy"));
			add(new SimpleDateFormat("yyyy.MM.dd"));
			add(new SimpleDateFormat("yyyy-MM-dd"));
			add(new SimpleDateFormat("yyyy/MM/dd"));
			add(new SimpleDateFormat("dd.MM.yyyy HH:mm"));
			add(new SimpleDateFormat("HH:mm"));
			add(new SimpleDateFormat("HH:mm:ss"));
			
		}
	};
	private String[] partsOfdate = null;
	private String[] partsOftimes = null;
	private Double powerValue =  null;
	private String unitValue = null;
	public int dateIndex;
	public int timeIndex;
	public int powerIndex;
	public int unitIndex;
	
	public String[] getPartsOfdate() {
		return partsOfdate;
	}

	public String getUnitValue() {
		return unitValue;
	}

	/**
	 * Cleaning for new CSVFile
	 */
	public void firstUse() {
		currentLineNumber = 0;
		projectRecords = new ArrayList<ProjectRecord>();
		csvFileLines = new ArrayList<String>();
		partsOfdate = null;
		partsOftimes = null;
		powerValue = null;
		unitValue = null;
		dateIndex = -1;
		timeIndex= -1;
		powerIndex = -1;
		unitIndex = -1;
		Exceptions.setErrorCount(0);
		DateException.setCounter(0);
		ValueException.setCounter(0);
	}

	public ArrayList<ProjectRecord> getRecords() {
		return projectRecords;
	}

	public long recordsSize(){
		return projectRecords.size();
	}
	
	public List<String> getCsvFileLines() {
		return csvFileLines;
	}

	public long fileSize(){
		return csvFileLines.size();
	}
	
	public int getCurrentLineNumber() {
		return currentLineNumber;
	}

	public void setCurrentLineNumber(int lineNumber) {
		singleton.currentLineNumber = lineNumber;
	}

	/**
	 * Checks if is new line
	 * @return true or false
	 */
	public boolean nextLine() {
		return ++currentLineNumber<=csvFileLines.size()-1;
		
	}

	/**
	 * Finds date format compares to known patterns
	 * @param dateValue - > date in String format
	 * @return parts of date or dateException
	 * @throws DateException
	 */
	private String[] findDate(String dateValue) throws DateException {
		SimpleDateFormat sdf;
		String dateMark = null;

		if(!dateValue.equals("")|!dateValue.equals(null)){
			for (int i = 0; i < 7; i++) {
				sdf = dateFormats.get(i);
				try {
					
					if(i==0){
						String [] str = dateValue.split("\\s");
						dateValue = str[0];	
					}
					sdf.parse(dateValue);
					
					if (sdf.toPattern().contains("."))
						dateMark = "\\.";
					else if (sdf.toPattern().contains("-"))
						dateMark = "-";
					else if (sdf.toPattern().contains("/"))
						dateMark = "/";
					
					return dateValue.split(dateMark);

				} catch (ParseException e) {
					
				}

			}
		}else throw new DateException();
		throw new DateException();
	}

	/**
	 * Finds time (Date format) compares to known patterns
	 * @param timeValue in String format
	 * @return parts of time(Date type) or DateException
	 * @throws DateException
	 */
	private String[] findTime(String timeValue) throws DateException {

		SimpleDateFormat sdf;
		if(!timeValue.equals("")|!timeValue.equals(null)){
			for (int i = 6; i < 9; i++) {
				sdf = dateFormats.get(i);
				try {
					sdf.parse(timeValue);
					if(i==6){
						String [] str = timeValue.split("\\s");
						return str[str.length-1].split(":");
					}else
						return timeValue.split(":");
				} catch (ParseException e) {

				}

			}
		}else throw new DateException();
		throw new DateException();
	}

	/**
	 * Finds out if given value can be parsed into double value
	 * @param energyValue in String format
	 * @return (double) energy or ValueException
	 * @throws ValueException
	 */
	private Double findEnergyValue(String energyValue) throws ValueException {
		Double energy;

		if(!energyValue.equals("")|!energyValue.equals(null)){
			energyValue = energyValue.replace(",",".");

			try {
				energy = Double.parseDouble(energyValue);
				return energy;
			} catch (NumberFormatException e) {

			}
		}
		else throw new ValueException();
		throw new ValueException();

	}

	
	/**
	 * Finds unit value compared to Watt
	 * @param UnitValue
	 * @return Watt or ValueException
	 * @throws ValueException
	 */
	private String findUnitValue(String UnitValue) throws ValueException{
		
		UnitValue = UnitValue.toUpperCase();
		
		boolean powerValueIsParsed=false;
		if(powerValue!=null) 
			powerValueIsParsed=true;
		
		if(!UnitValue.equals("")|!UnitValue.equals(null)){
			if(UnitValue.equals("W")){
				return "W";
			}else if(UnitValue.equals("KW")){ //kW 
				if(powerValueIsParsed)
					powerValue=powerValue*1000;
				return "W";
			}else if(UnitValue.equals("MW")){ //MW
				if(powerValueIsParsed)
					powerValue=powerValue*1000000;
				return "W";
			}else if(UnitValue.equals("E")){ //incorrect 
				return "W";
			}else throw new ValueException();
		}else throw new ValueException();

	}

	
	/**
	 * Checks if imported values are correct.
	 * WARNING: 
	 * User can input power values in german number format, but If there is not any fractional part, user can pass over value with the dot without comma part.
	 * For Example:
	 * Correct value: 1.500,0;
	 * User output: 1.500 => meanings 1 500
	 * 
	 * @param dateCombo
	 * @param timeCombo
	 * @param energyCombo
	 * @param unitCombo
	 * @param log
	 * @param tillTheEndOfFile - is it checked
	 * @param remeberChoice - is it checked
	 * @return true - all values are correct, false - if not
	 */
	public boolean tryToParse(
			Combo dateCombo,
			Combo timeCombo,
			Combo energyCombo,
			Combo unitCombo,
			Text log, 
			boolean tillTheEndOfFile, 
			boolean remeberChoice) {
		
		Display dispaly = Display.getCurrent();
		Color yellow = dispaly.getSystemColor(SWT.COLOR_YELLOW);
		
		String[] line = (displayLine(currentLineNumber)).split(";");
		
		//DATE
		try {
			partsOfdate = findDate(line[dateIndex]);
		} catch (DateException edd) {
			partsOfdate = null;
			if(!tillTheEndOfFile){
				dateCombo.setBackground(yellow);
				log.append("\n" + edd.wrongDateFormat());
			}
		
		}catch(ArrayIndexOutOfBoundsException eda){
			try {
				partsOfdate = findDate(dateCombo.getText());
			} catch (DateException edd) {
				partsOfdate = null;
				if(!tillTheEndOfFile) {
					log.append("\n" + edd.wrongDateFormat());
					dateCombo.setBackground(yellow);
				}			
			}
			
			
		}
		
		//TIME
		try {
			partsOftimes = findTime(line[timeIndex]);
		} catch (DateException etd) {
			partsOftimes = null;
			if(!tillTheEndOfFile) {
				timeCombo.setBackground(yellow);
				log.append("\n" + etd.wrongTimeFormat());
			}
			
		}catch(ArrayIndexOutOfBoundsException eta){
			try {
				partsOftimes = findDate(timeCombo.getText());
			} catch (DateException etd) {
				partsOftimes = null;
				if(!tillTheEndOfFile) {
					timeCombo.setBackground(yellow);
					log.append("\n" + etd.wrongTimeFormat());
				}
			}
			
		}
		
		//POWER
		try {
			powerValue = findEnergyValue(line[powerIndex]);
		} catch (ValueException epv) {
			
			try {
				powerValue = findEnergyValue(""+tryGermanNumber(line[powerIndex]));
			} catch (ValueException e) {
				powerValue = null;
				if(!tillTheEndOfFile) {
					energyCombo.setBackground(yellow);
					log.append("\n" + epv.wrongPowerValue());
				}
			}
			
			
		}catch(ArrayIndexOutOfBoundsException epa){
			try{
				if(!tillTheEndOfFile & !remeberChoice) 
					powerValue = findEnergyValue(energyCombo.getText());
				else powerValue = findEnergyValue("");
			} catch (ValueException epv2) {
				try {
					powerValue = findEnergyValue(""+tryGermanNumber(energyCombo.getText()));
				} catch (ValueException e) {
					powerValue = null;
					if(!tillTheEndOfFile) {
						energyCombo.setBackground(yellow);
						log.append("\n" + epv2.wrongPowerValue());
					}
				}
			}
		}
		
		//UNIT
		try {
			unitValue = findUnitValue(line[unitIndex]);
		} catch (ValueException euv) {
			unitValue = null;
			if(!tillTheEndOfFile) {
				unitCombo.setBackground(yellow);
				log.append("\n" + euv.wrongUnitValue());
			}
			
		}catch(ArrayIndexOutOfBoundsException eua){
			try {
				unitValue = findUnitValue(unitCombo.getText());
			} catch (ValueException e4) {
				unitValue = null;
				if(!tillTheEndOfFile) {
					unitCombo.setBackground(yellow);
					log.append("\n" + e4.wrongUnitValue());
				}
			}
		}
		

		return ((partsOfdate != null & partsOftimes != null & powerValue != null & unitValue != null));
		
	}

	/**
	 * Joins day and time to Date Format
	 * @param dateParts
	 * @param timeParts
	 * @return date OR null - if there are parts of day and time
	 */
	private Date addTimeToDate(String[] dateParts, String[] timeParts) {
		if (dateParts != null & timeParts != null) {
			Calendar cal = Calendar.getInstance();
			cal.set(Integer.parseInt(dateParts[2]), Integer.parseInt(dateParts[1]) - 1, Integer.parseInt(dateParts[0]));
			cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeParts[0]));
			cal.set(Calendar.MINUTE, Integer.parseInt(timeParts[1]));
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);

			Date newDateTime = cal.getTime();
			return newDateTime;
		}
		return null;
	}

	/**
	 * Create new file record (ProjectRecord).
	 */
	public void loadDataIntoRecords() {
		Date dateTime = addTimeToDate(partsOfdate, partsOftimes);
		ProjectRecord record = new ProjectRecord(dateTime, powerValue, unitValue);
		projectRecords.add(record);
	}

	/**
	 * Loads lines of CSVFile into Array. Skips lines starts with '#' - comment.
	 * @param filePath
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void loadDataFromFile(String filePath) throws FileNotFoundException, IOException

	{
		csvFileLines.clear();

		Scanner reader = new Scanner (new FileReader(filePath));
		String nextLine;
		while (reader.hasNextLine()) {

			nextLine = reader.nextLine();
				if (!nextLine.startsWith("#"))
					csvFileLines.add(nextLine);
			}


		reader.close();

	}

	/** 
	 * Returns current file's line
	 * @param numberOfLine
	 * @return line of Project
	 */
	public String displayLine(int numberOfLine) {
		String line;
		try {
			line = csvFileLines.get(numberOfLine);
		} catch (ArrayIndexOutOfBoundsException a) {
			line = "End of File";
		} catch (IndexOutOfBoundsException e){
			line = "End of File";
		}
		return line;
	}

	/**
	 * Saves selection's index. Used for setting user's choice as default data value.
	 * @param dateCombo
	 * @param timeCombo
	 * @param powerCombo
	 * @param unitCombo
	 */
	public void saveColumnsIndex(Combo dateCombo, Combo timeCombo,Combo powerCombo,Combo unitCombo){
		
		dateIndex = dateCombo.getSelectionIndex();
		timeIndex = timeCombo.getSelectionIndex();
		powerIndex = powerCombo.getSelectionIndex();
		unitIndex = unitCombo.getSelectionIndex();
		
	}
	/**
	 * Checks if given value has german number pattern (#.###,##). If true, changes into default pattern (####,##),
	 * otherwise gives null.
	 * @param value
	 * @return (string) changed value or null
	 */
	public Double tryGermanNumber(String value){
		Number number;
		
		try {
			number = NumberFormat.getNumberInstance(Locale.GERMAN).parse(value);
		} catch (ParseException e) {
			number = null;
			return null;
		}
		
		return number.doubleValue();
	}
	
}