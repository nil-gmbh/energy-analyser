/**
 * 
 */
package data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Model for CSVFile record (into database)
 * @author Panda
 *
 */
public class ProjectRecord implements Serializable {
	
	private Date date = null;
	private Double powerValue = null;
	private String unit = null;
	private DateFormat df = null;
	
	public ProjectRecord(Date date, Double powerValue, String unit) {
		super();
		this.date = date;
		this.powerValue = powerValue;
		this.unit = unit;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Double getPowerValue() {
		return powerValue;
	}
	public void setValue(Double powerValue) {
		this.powerValue = powerValue;
	}

	/**
	 * Display time in format HH:mm
	 * @return HH:mm
	 */
	public String displaySimpleTime(){
		df = CsvData.dateFormats.get(7);
		return df.format(date);
	}
	/**
	 * Display date in format dd.MM.yyyy
	 * @return dd.MM.yyyy
	 */
	public String displaySimpleDate(){
		df = CsvData.dateFormats.get(0); 
		return df.format(date);
	}
	
	
	
}
