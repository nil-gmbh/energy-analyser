
package data;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

import javax.security.auth.Subject;

import enums.HistogramDisplayMode;
import enums.MonthName;
import enums.StatisticsDisplayMode;

/**
 * Storage for DBRecordsStatisticsView and PowerReductionView.
 * Both use project in the same time.
 * SINGLETON & OBSERVABLE
 * @author Panda
 *
 */
public class ProjectResources extends Observable{

private static ProjectResources singleton = new ProjectResources();
	
	private ProjectResources(){}
	
	public static ProjectResources getInstance() {
		return singleton;
	}
	
	private boolean newUnusedProject,newReductionChart;
	private String projectName,units;
	private long quantity;
	private int reductionPeaksQuantity,projectPeaksQuantity,peaksCutLineSpinnerValue,constPeaksNumber,constPeaksSeriesQuantity,constCriticalPeakSeriesQuantity;
	private Date projectFirstDate,projectLastDate,histogramPrevDay,histogramNextDate,reductionChartPrevDate,reductionChartNextDate;
	private double projectTotalPower,projectReductionPower,projectTotalCost,projectSavedCost,peaksCutLine,price;
	private Double max,min,mean;
	private String[] monthName = MonthName.stringValues();
	private ArrayList<Date> criticalConstPeaks;
	private Map<Date,Double> currentCounstPeaksSeries;
	private Map<Integer,ArrayList<Integer>> monthsInYear;
	private Map<String,double[]> customStatistics;
	private ArrayList<ProjectRecord> currentDbRecords,projectRecords;
	private ArrayList<ReductionPeakRecord> projectPeaks;
	private ArrayList<ReductionChartRecord> reductionChartRecords,currentReductionChartRecords;
	
	
	/**
	 * Cleans variables, when project is changed.
	 * 
	 * @param firstUse -> project first use
	 */
	public void clean(boolean firstUse){
		
		max = 0.0;
		min = 0.0;
		mean = 0.0;
		if(firstUse){
			projectName = null;
			quantity = 0;
			projectFirstDate = null;
			projectLastDate = null;
			units = null;
			monthsInYear = new HashMap<Integer, ArrayList<Integer>>();
			customStatistics = new HashMap<String, double[]>();
			currentCounstPeaksSeries = new HashMap<Date, Double>();
			newUnusedProject = false;
			criticalConstPeaks = new ArrayList<Date>();
			projectRecords = new ArrayList<ProjectRecord>();
			currentDbRecords = new ArrayList<ProjectRecord>();
			reductionChartRecords = new ArrayList<ReductionChartRecord>();
			currentReductionChartRecords = new ArrayList<ReductionChartRecord>();
		}
		
	}
	
	public ArrayList<ProjectRecord> getCurrentDbRecords() {
		return currentDbRecords;
	}

	/**
	 * Sets current records from database, while it's unused, into projectRecord
	 * records => project original records
	 * @param records
	 */
	public void setCurrentDbRecords(ArrayList<ProjectRecord> records) {
		singleton.currentDbRecords = records;	
		if(newUnusedProject){
			singleton.projectRecords = records;
		}
	}
	

	public ArrayList<ProjectRecord> getProjectRecords() {
		return projectRecords;
	}

	public Map<String, double[]> getCustomStatistics() {
		return customStatistics;
	}

	public void setCustomStatistics(Map<String, double[]> map) {
		singleton.customStatistics = map;
	}

	public ArrayList<ReductionPeakRecord> getProjectPeaks() {
		return projectPeaks;
	}

	public void setProjectPeaks(ArrayList<ReductionPeakRecord> peaks) {
		this.projectPeaks = peaks;
	}


	/**
	 * In the meantime reduction chart records are assign into current reduction chart records
	 * @return reduction chart records
	 */
	public ArrayList<ReductionChartRecord> getReductionChartRecords() {
		currentReductionChartRecords = reductionChartRecords;
		return reductionChartRecords;
	}

	public void setReductionChartRecords(ArrayList<ReductionChartRecord> chartRecord) {
		this.reductionChartRecords = chartRecord;
		currentReductionChartRecords = chartRecord;
	}

	/**
	 * Meantime, after if new Reduction chart was generated, reduction statistics was assigned.
	 * @return ReductionChartRecords
	 */
	public ArrayList<ReductionChartRecord> getCurrentReductionChartRecords() {
		if(newReductionChart){
			projectPeaksQuantity = reductionPeaksQuantity;
			projectTotalPower = totalPowerAmount();
			projectReductionPower = reduction();
			projectTotalCost = totalCost();
			projectSavedCost = savedCost();
			newReductionChart = false;
		}
		return currentReductionChartRecords;
	}

	public void setCurrentReductionChartRecords(ArrayList<ReductionChartRecord> currentChartRecords) {
		this.currentReductionChartRecords = currentChartRecords;
	}

	public boolean isNewUnusedProject() {
		return newUnusedProject;
	}

	public void setNewUnusedProject(boolean newProject) {
		this.newUnusedProject = newProject;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String name) {
		singleton.projectName = name;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		singleton.quantity = quantity;
	}

	public Date getProjectFirstDate() {
		return projectFirstDate;
	}

	public void setProjectFirstDate(Date firstDate) {
		singleton.projectFirstDate = firstDate;
	}

	public Date getProjectLastDate() {
		return projectLastDate;
	}

	public void setProjectLastDate(Date lastDate) {
		singleton.projectLastDate = lastDate;
		
	}

	public Date getHistogramPrevDay() {
		return histogramPrevDay;
	}

	public void setHistogramPrevDay(Date prevDay) {
		singleton.histogramPrevDay = prevDay;
	}

	public Date getHistogramNextDate() {
		return histogramNextDate;
	}

	public void setHistogramNextDate(Date nextDate) {
		singleton.histogramNextDate = nextDate;
	}

	public Date getReductionChartPrevDate() {
		return reductionChartPrevDate;
	}

	public void setReductionChartPrevDate(Date cuttedChartPrevDate) {
		this.reductionChartPrevDate = cuttedChartPrevDate;
	}

	public Date getReductionChartNextDate() {
		return reductionChartNextDate;
	}

	public void setReductionChartNextDate(Date cuttedChartNextDate) {
		this.reductionChartNextDate = cuttedChartNextDate;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		singleton.units = units;
	}

	public Double getMax() {
		return max;
	}

	public void setMax(Double max) {
		singleton.max = max;
	}

	public Double getMin() {
		return min;
	}

	public void setMin(Double min) {
		singleton.min = min;
		
	}

	public Double getMean() {
		return mean;
	}

	/**
	 * Mean is saved to second decimal palace.
	 * @param mean
	 */
	public void setMean(Double mean) {
		DecimalFormat df = new DecimalFormat();
		df.applyPattern("##.##");
		df.setMaximumFractionDigits(2);
		String stringMean = df.format(mean);
		stringMean = stringMean.replace(",", ".");
		singleton.mean = Double.parseDouble(stringMean);
		
		if(newUnusedProject & !projectName.equals(null)){
			singleton.setChanged();
			singleton.notifyObservers();
		}
	}
	
	/**
	 * @return spinner Value
	 */
	public int getPeaksCutLineValue() {
		return peaksCutLineSpinnerValue;
	}

	/**
	 * Sets spinner value
	 * @param peakCutLineValue
	 */
	public void setPeaksCutLineValue(int peakCutLineValue) {
		this.peaksCutLineSpinnerValue = peakCutLineValue;
	}

	public double getPeaksCutLine() {
		return peaksCutLine;
	}

	public void setPeaksCutLine(double cutLine) {
		this.peaksCutLine = cutLine;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public long getCurrentReductionPeaksQuantity() {
		return reductionPeaksQuantity;
	}

	
	public void setNewConsumptionChart(boolean newConsumptionChart) {
		this.newReductionChart = newConsumptionChart;
	}

	public long getProjectPeaksQuantity() {
		return projectPeaksQuantity;
	}

	public void setProjectPeaksQuantity(int projectCuts) {
		this.projectPeaksQuantity = projectCuts;
	}

	public double getProjectTotalPower() {
		return projectTotalPower;
	}

	public void setProjectTotalPower(double projectTotalPower) {
		this.projectTotalPower = projectTotalPower;
	}

	public double getProjectReduction() {
		return projectReductionPower;
	}

	public void setProjectReduction(double projectReduction) {
		this.projectReductionPower = projectReduction;
	}

	public double getProjectTotalCost() {
		return projectTotalCost;
	}

	public void setProjectTotalCost(double projectTotalCost) {
		this.projectTotalCost = projectTotalCost;
	}

	public double getProjectSavedCost() {
		return projectSavedCost;
	}

	
	public void setProjectSavedCost(double projectSavedCost) {
		this.projectSavedCost = projectSavedCost;
	}
	
	public int getConstPeaksProcessQuantity() {
		return constPeaksSeriesQuantity;
	}

	public void setConstPeaksProcessQuantity(int constPeaksProcessQuantity) {
		this.constPeaksSeriesQuantity = constPeaksProcessQuantity;
	}

	public void setConstPeaksNumber(int constPeaksNumber) {
		this.constPeaksNumber = constPeaksNumber;
	}

	public int getConstPeaksNumber() {
		return constPeaksNumber;
	}

	/**
	 * 
	 * @return current quantity of critical constant peaks series
	 */
	public int getConstCriticalPeakSeriesQuantity() {
		Date firstDate = currentReductionChartRecords.get(0).getDate(), 
				lastDate = currentReductionChartRecords.get(currentReductionChartRecords.size()-1).getDate();
		int peaksCut = 0,quantity = 0;

		for(ReductionChartRecord record: currentReductionChartRecords){
			Date date = record.getDate();
			//check current date interval
			if((date.equals(firstDate)||date.after(firstDate)) & (date.equals(lastDate)||date.before(lastDate))){
				//check if record had a const peak
				if(record.hadConstansPeak()){
					peaksCut++;
				}else{
					//check if they are found const peaks 
					if(peaksCut>0){
						if(peaksCut>=constPeaksNumber){
							quantity++;
						}
						peaksCut=0;
					}
				}
				
				
			}
		}
		constCriticalPeakSeriesQuantity = quantity;
		return constCriticalPeakSeriesQuantity;
	}
	

	public Map<Date, Double> getCurrentCounstPeaksSeries() {
		return currentCounstPeaksSeries;
	}

	public void setCurrentCounstPeaksSeries(Map<Date, Double> currentCounstPeaksSeries) {
		this.currentCounstPeaksSeries = currentCounstPeaksSeries;
	}

	public ArrayList<Date> getCriticalConstPeaks() {
		
		return criticalConstPeaks;
	}

	/**
	 * Attributes months in year according to project's records.
	 */
	public void loadMonthsInYearValues(){
		ArrayList<Integer> monthsList = new ArrayList<Integer>();
		Calendar cal = Calendar.getInstance();
		int year,month,yearKey = 0;
		for(ProjectRecord record: currentDbRecords){
			cal.setTime(record.getDate());
			year = cal.get(Calendar.YEAR);
			month = cal.get(Calendar.MONTH);
			
			if(yearKey==year){
				if(!monthsList.contains(month)) 
					monthsList.add(month);
			}else{
				if(yearKey != 0) 
					monthsInYear.put(yearKey, monthsList);
				yearKey = year;
				
				if(monthsList.size()!=0) 
					monthsList = new ArrayList<Integer>();
				monthsList.add(month);
					
			}
		}
		
		monthsInYear.put(yearKey, monthsList);
	}

	/**
	 * Return months of given year according to project records.
	 * @param year
	 * @return year's months
	 */
	public String[] monthsToDisplay(int year){
		ArrayList<Integer> months = monthsInYear.get(year);
		String [] monthsName = new String[months.size()];
		int number=0,i=0;
		for(Integer month: months){
			if(month == number) {
				monthsName[i] = monthName[number];
				i++;
			}
			number++;
		}
		
		return monthsName;
	}

	/**
	 * Returns years included in records
	 * @return years
	 */
	public String[] yearsToDisplay() {
		Set<Integer> yearsSet = monthsInYear.keySet();
		String[] years = new String[yearsSet.size()];
		int i=0;
		for(Integer year: yearsSet){
			years[i] = Integer.toString(year);
			i++;

		}
		return years;
	}
	
			
	/**
	 * Returns sorted dateValues(integer value in String format). Some of them needs to be determine in time (timeSpecification). For:
	 * HistogramDisplayMode.PROJECT || HistogramDisplayMode.CUSTOM || StatisticsDisplayMode.WEEKS
	 * Example: Week in Year, days for months, years in project.
	 * Function sorts dateValues in numeric order.
	 * @return (String[]) XAxis CategorySeries dates 
	 */
	public String[] statisticsDateValuesKeys(){
		Set<String> keySet = customStatistics.keySet();
		String[] keys = keySet.toArray(new String[keySet.size()]),
				sortedKeys = new String[keySet.size()],
				timeSpecification = new String[keySet.size()];
		ArrayList<Integer> idSpecification = new ArrayList<>(),
				keyList = new ArrayList<>();
		
		//HistogramDisplayMode.PROJECT || HistogramDisplayMode.CUSTOM || StatisticsDisplayMode.WEEKS
		if(keys[0].contains("-")){
			for(int i=0;i<keys.length;i++){
				String[] str = keys[i].split("-");
				keys[i]=str[1];
				keyList.add(Integer.parseInt(keys[i]));
				timeSpecification[i]=str[0];
				idSpecification.add(Integer.parseInt(timeSpecification[i]));
			}
			
			// sorting keys
			Set<Integer> treeKeySet = new TreeSet<Integer>(keyList);
			keyList = new ArrayList<Integer>(treeKeySet);
			int i = 0;
			for(Integer key: treeKeySet){
				keys[i] = key.toString();
				i++;
			}
			
			
			//sorting time specification
			Set<Integer> idSet = new TreeSet<Integer>(idSpecification);
			idSpecification = new ArrayList<Integer>(idSet);
			
			
			//join together
			int sortedKeyNumber=0,keyNumber=0;
			Iterator<Integer> iter = idSpecification.iterator();
			while(iter.hasNext()){
				int id = iter.next();
				for(i=0;i<timeSpecification.length;i++){
					if(timeSpecification[i].equals(""+id)){
						sortedKeys[sortedKeyNumber] = timeSpecification[i]+"-"+keys[keyNumber];
						sortedKeyNumber++;
						keyNumber++;
					}
				}
				keyNumber=0;
			}
			
		}else{
			int[] sortArray = new int[keys.length];
			for(int i=0;i<sortArray.length;i++){
				sortArray[i]=Integer.parseInt(keys[i]);
			}
			Arrays.sort(sortArray);
			for(int i=0;i<sortArray.length;i++){
			  	sortedKeys[i]=String.valueOf(sortArray[i]);
			}
		}
		
		return sortedKeys;
	}
	
	/**
	 * Returns table with statistics values - min,max,mean(avg).
	 * @param dateValue -> date in String format
	 * @return (double[]) min, max, mean
	 */
	public double[] statisticsValues(String dateValue){
		
		return customStatistics.get(dateValue);
	}
	
	/**
	 * Returns total project's dates.
	 * @return (Date[]) dates
	 */
	public Date[] projectDates(){
		Date[] dates = new Date[projectRecords.size()];
		int i=0;
		for(ProjectRecord record: projectRecords){
			dates[i] = record.getDate();
			i++;
		}
		return dates;
	}
	
	/**
	 * Returns total project energy.
	 * @return (double[] energy)
	 */
	public double[] projectEnergy(){
		double[] energy = new double[projectRecords.size()];
		int i=0;
		for(ProjectRecord record: projectRecords){
			energy[i] = record.getPowerValue();
			i++;
		}
		return energy;
	}
	
	/**
	 * Returns current histogram chart's dates
	 * @return (Date[]) dates
	 */
	public Date[] chartDates(){
		Date[] dates = new Date[currentDbRecords.size()];
		int i=0;
		for(ProjectRecord record: currentDbRecords){
			dates[i] = record.getDate();
			i++;
		}
		return dates;
	}
	
	/**
	 * Returns current histogram chart's energy
	 * @return (double[] energy)
	 */
	public double[] chartEnergy(){
		double[] energy = new double[currentDbRecords.size()];
		int i=0;
		for(ProjectRecord record: currentDbRecords){
			energy[i] = record.getPowerValue();
			i++;
		}
		return energy;
	}
	
	/**
	 * Returns current reduction chart power amount
	 * @return (double) power amount
	 */
	public double totalPowerAmount(){
		double powerAmount=0.0;
		
		for(ReductionChartRecord record:currentReductionChartRecords){
			powerAmount +=record.getPower();
		}
		return powerAmount;
	}
	
	/**
	 * Return reduction for current reduction chart.
	 * @return (double) reduction
	 */
	public double reduction(){
		double reduction=0.0;
		
		Date firstDate = currentReductionChartRecords.get(0).getDate(), 
				lastDate = currentReductionChartRecords.get(currentReductionChartRecords.size()-1).getDate();
		for(ReductionPeakRecord record:projectPeaks){
			Date date = record.getDate();
			if((date.equals(firstDate)||date.after(firstDate)) & (date.equals(lastDate)||date.before(lastDate))){
				reduction +=record.getReduction();
			}
			
		}
		return reduction;
	}
	
	/**
	 * Return total cost of current reduction chart records
	 * @return (double) cost
	 */
	public double totalCost(){
		double cost=0.0;
		
		for(ReductionChartRecord record:currentReductionChartRecords){
			cost +=record.getCost();
		}
		return cost;
	}
	
	/**
	 * Return total saved cost of current reduction chart records
	 * @return (double) saved cost
	 */
	public double savedCost(){
		double cost=0.0;
		Date firstDate = currentReductionChartRecords.get(0).getDate(), 
				lastDate = currentReductionChartRecords.get(currentReductionChartRecords.size()-1).getDate();
		for(ReductionPeakRecord record:projectPeaks){
			Date date = record.getDate();
			if((date.equals(firstDate)||date.after(firstDate)) & (date.equals(lastDate)||date.before(lastDate))){
				cost +=record.getCost();
			}
			
			
		}
		return cost;
	}
	
	/**
	 * Checks if records in given date interval are more then 1. Function is used to checking previous & next Dates 
	 * for PowerResourceView.
	 * @param firstDate
	 * @param lastDate
	 * @return true : quantity>1, otherwise false;
	 */
	public boolean checkDateRecords(Date firstDate,Date lastDate){

		int quantity=0;
		for(ReductionChartRecord record: reductionChartRecords){
			Date date = record.getDate();
			if((date.equals(firstDate)||date.after(firstDate)) & (date.equals(lastDate)||date.before(lastDate))){
				quantity++;
			}
		}
		
		if(quantity>1)
			return true;
		else
			return false;
	}

	/**
	 * Sets current reduction peaks' quantity.
	 */
	public void loadCurrentReductionPeaksQuantity() {
		int quantity=0;
			for(ReductionChartRecord record: currentReductionChartRecords){
				quantity+=record.reductionPeaksQuantity();
			}
			
		reductionPeaksQuantity = quantity;
	}
	
	/**
	 * Updates current constant peaks series quantity according to reduction chart displayed time interval
	 */
	public void loadCurrentConstPeaksSeriesQuantity(){
		Date firstDate = currentReductionChartRecords.get(0).getDate(), 
				lastDate = currentReductionChartRecords.get(currentReductionChartRecords.size()-1).getDate();
		int peaksCut = 0,quantity = 0;

		for(ReductionChartRecord record: currentReductionChartRecords){
			Date date = record.getDate();
			//check current date interval
			if((date.equals(firstDate)||date.after(firstDate)) & (date.equals(lastDate)||date.before(lastDate))){
				//check if record had a const peak
				if(record.hadConstansPeak()){
					peaksCut++;
				}else{
					//check if they are found const peaks 
					if(peaksCut>0){
						quantity++;
						peaksCut=0;
					}
				}
				
				
			}
		}
		constPeaksSeriesQuantity = quantity;
	}
	
	/**
	 * Add critical constant peaks' dates in current date interval.
	 */
	public void loadCriticalConstPeaks(){
		Date firstDate = currentReductionChartRecords.get(0).getDate(), 
				lastDate = currentReductionChartRecords.get(currentReductionChartRecords.size()-1).getDate();
		int peaksCut = 0;

		for(ReductionChartRecord record: currentReductionChartRecords){
			Date date = record.getDate();
			//check current date interval
			if((date.equals(firstDate)||date.after(firstDate)) & (date.equals(lastDate)||date.before(lastDate))){
				//check if record had a const peak
				if(record.hadConstansPeak()){
					peaksCut++;
				}else{
					//check if they are found const peaks 
					if(peaksCut>0){
						if(peaksCut>=constPeaksNumber){
							criticalConstPeaks.add(date);
						}
						peaksCut=0;
					}
				}
				
				
			}
		}
	}
}
