package data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Model for new power consumption Record. One of constructors has implemented information about occurrence of 
 * constant peaks (reductions peaks occur after itself): 
 * If reduction record' mode id NEW & has value the same as peakCutLine - had constant peak (before was moved to another record) 
 * @author Panda
 *
 */
public class ReductionChartRecord implements Serializable {
	/**
	 * generated UID
	 */
	private static final long serialVersionUID = -5951090885288215243L;
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");;
	private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");;
	
	private Date date;
	private double power;
	private double powerModification;
	private double cost;
	private String mode;
	private ArrayList<ReductionPeakRecord> reductionPeaks;
	private boolean constansPeak;

	
	/**
	 * MODE is type of record: NEW OR OLD,
	 * CONSTAN PEAK - true if power was above peakCutLine value & reduction peak was moved to next record
	 * @param date
	 * @param power
	 * @param powerModification
	 * @param cost
	 * @param mode
	 * @param reductionPeaks
	 * @param constansPeak
	 */
	public ReductionChartRecord(Date date, double power, double powerModification, double cost, String mode, ArrayList<ReductionPeakRecord> reductionPeaks,
			boolean constansPeak) {
		super();
		this.date = date;
		this.power = power;
		this.powerModification = powerModification;
		this.cost = cost;
		this.mode = mode;
		this.reductionPeaks = reductionPeaks;
		this.constansPeak = constansPeak;
	}


	public Date getDate() {
		return date;
	}

	public double getPower() {
		return power;
	}

	public double getCost() {
		return cost;
	}

	public String getMode() {
		return mode;
	}

	public ArrayList<ReductionPeakRecord> getReductionPeaks() {
		return reductionPeaks;
	}
	
	/**
	 * Ask if original value (form project) was cut and if reduction peak was moved.
	 * @return true/false
	 */
	public boolean hadConstansPeak() {
		return constansPeak;
	}


	/** Display time in format HH:mm
	 * @return time from Date format
	 */
	public String displaySimpleTime(){
		return TIME_FORMAT.format(date);
	}
	/**Display date in format dd.MM.yyyy
	 * @return dd.MM.yyyy
	 */
	public String displaySimpleDate(){
		
		return DATE_FORMAT.format(date);
	}
	

	public double getPowerModification() {
		return powerModification;
	}


	public void setPowerModification(double powerModification) {
		this.powerModification = powerModification;
	}
	
	
	/**
	 * Calculated total peaks' reduction for current reduction chart record. Function is used in tableViewer's label provider.
	 * @return (double) reduction value
	 */
	public double reduction(){
		double reduction = 0.0;
		try {
			for(ReductionPeakRecord peak: reductionPeaks){
				reduction+=peak.getReduction();
			}
		} catch (NullPointerException e) {
			reduction += 0.0;
		}
		
		return reduction;
	}
	
	/**
	 * Calculated cost of project peaks -> saved cost. Function is used in tableViewer's label provider.
	 * @return (double) saved cost
	 */
	public double savedCost(){
		double cost=0.0;
		
		try {
			for(ReductionPeakRecord peak: reductionPeaks){
				cost +=peak.getCost();
			}
		} catch (NullPointerException e) {
			cost+=0.0;
		}
		return cost;
	}
	
	
	/**
	 * Reduction peaks' quantity for current reduction chart record. Function is used in tableViewer's label provider.
	 * @return (integer) cuts' quantity
	 */
	public int reductionPeaksQuantity(){
		int quantity=0;
		
		try {
			quantity+=reductionPeaks.size();
		} catch (NullPointerException e) {
			quantity+=0.0;
		}
		return quantity;
	}

}
