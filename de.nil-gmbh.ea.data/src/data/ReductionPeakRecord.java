package data;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Bean as model for Peak Record
 * @author Panda
 *
 */
public class ReductionPeakRecord implements Serializable {
	private Date date;
	private double reduction;
	private double cost;
	private SimpleDateFormat sdf;
	
	public ReductionPeakRecord(Date date, double reduction, double price) {
		super();
		this.date = date;
		this.reduction = reduction;
		this.cost = price;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public double getReduction() {
		return reduction;
	}
	public void setReduction(double reductionValue) {
		this.reduction = reductionValue;
	}
	
	/**
	 * Display time in format HH:mm
	 * @return time from Date format
	 */
	public String displaySimpleTime(){
		sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(date);
	}
	/**
	 * Display date in format dd.MM.yyyy
	 * @return dd.MM.yyyy
	 */
	public String displaySimpleDate(){
		sdf = new SimpleDateFormat("dd.MM.yyyy");
		return sdf.format(date);
	}
	
	
	
}
