package enums;

/**
 * Histogram chart display mode (DBRecordsStatisticsView)
 * @author Panda
 *
 */
public enum HistogramDisplayMode {
	PROJECT,
	YEAR,
	MONTH,
	DAY,
	CUSTOM
}


