package enums;

/**
 * Full months' names to display
 * @author Panda
 *
 */
public enum MonthName {
	JAN,
	FEB,
	MAR,
	APR,
	MAY,
	JUN,
	JUL,
	AUG,
	SEP,
	OCT,
	NOV,
	DEC;

	
	/* (non-Javadoc)
	 * return full month's name
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		String name="";
		switch(this.name()){
			case "JAN": {
				name = "January";
				break;
			}case "FEB": {
				name = "Febuary";
				break;
			}case "MAR": {
				name = "March";
				break;
			}case "APR": {
				name = "April";
				break;
			}case "MAY": {
				name = "May";
				break;
			}case "JUN": {
				name = "June";
				break;
			}case "JUL": {
				name = "July";
				break;
			}case "AUG": {
				name = "August";
				break;
			}case "SEP": {
				name = "September";
				break;
			}case "OCT": {
				name = "October";
				break;
			}case "NOV": {
				name = "November";
				break;
			}case "DEC": {
				name = "December";
				break;
			}
		}
		return name;
	}
	
	/**
	 * Returns all months in year
	 * @return full months' names
	 */
	public static String[] stringValues(){
		String[] monthsNames = new String[MonthName.values().length];
		int i=0;
		for(MonthName name:MonthName.values()){
			monthsNames[i] = name.toString();
					
			i++;
		}
		return monthsNames;
	}	
	
	/**
	 * Return month based on given number (0-11).
	 * @param numberOfTableItem
	 * @return full month's name
	 */
	public static String valueOf(int numberOfTableItem){
		String[] monthNames= stringValues();
		
		return monthNames[numberOfTableItem].toString();
		
	}
	
}

