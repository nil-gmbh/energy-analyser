package enums;

/**
 * Power reduction chart display mode (PowerReductionView)
 * @author Panda
 *
 */
public enum ReductionDisplayMode {
	OVERLAPPED,
	MONTH,
	DAY,
	CUSTOM,
	CUTTED_PEAKS
}
