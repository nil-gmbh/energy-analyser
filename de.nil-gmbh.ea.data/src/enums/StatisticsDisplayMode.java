package enums;

/**
 * Statistics Chart display mode (DBRecordsStatisticsView)
 * @author Panda
 *
 */
public enum StatisticsDisplayMode {
	DIAGRAM,
	YEARS,
	MONTHS,
	WEEKS,
	DAYS,
	HOURS
}
