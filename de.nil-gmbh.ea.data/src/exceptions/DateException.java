package exceptions;

import java.util.Date;

/**
 * Date and time exception for application propose. Has own error's counter.
 * @author Panda
 *
 */
public class DateException extends Exceptions{

	private static int counter=0;
	
	public static void setCounter(int counter) {
		DateException.counter = counter;
	}

	public DateException() {
		super();
		countErrors();
		counter++;
	}

	public static int getCount() {
		return counter;
	}

	public String wrongDateFormat(){
		return Message()+" Wrong Date Format\n";
	}

	public String wrongTimeFormat(){
		return Message()+" Wrong Time Format\n";
	}
	
	
	
	
}
