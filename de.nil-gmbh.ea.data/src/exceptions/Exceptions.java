package exceptions;

/**
 * Model for aplication's exceptions (date,time,power, unit - values).
 * Count error's quantity, send message.
 * @author Panda
 *
 */
public class Exceptions extends Exception{
	
	private static int errorCount=0;
	
	public static void setErrorCount(int errorCount) {
		Exceptions.errorCount = errorCount;
	}

	public String Message(){
		return "Error = ";
	}
	
	public void countErrors(){
		errorCount++;
	}
	
	public static int getCount(){
		return errorCount;
	}

	Exceptions(){
		
	}
}
