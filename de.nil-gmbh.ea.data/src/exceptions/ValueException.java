package exceptions;

/**
 * Power and Unit value exception for application propose. Has own error's counter.
 * @author Panda
 *
 */
public class ValueException extends Exceptions{

	private static int counter=0;
	
	public static void setCounter(int counter) {
		ValueException.counter = counter;
	}

	public ValueException() {
		
		super();
		countErrors();
		counter++;
	}
	
	
	public String wrongPowerValue(){
		return Message()+" Wrong Power Value";
	}
	
	public String wrongUnitValue(){
		return Message()+" Wrong Unit Value";
	}

	public static int getCount() {
		return counter;
	}
	
	
}
