package database;

import java.lang.reflect.Array;
import java.net.ConnectException;
import java.text.ParseException;
import java.util.ArrayList;

import org.bson.Document;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.MongoSocketException;
import com.mongodb.MongoSocketOpenException;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import data.ReductionPeakRecord;
import data.ReductionChartRecord;
import data.CsvData;
import data.ProjectResources;
import data.ProjectRecord;

/**
 * Insert CVSFile & reduction chart records into MongoDB
 * @author Panda
 *
 */
public class Insert {

	public static MongoClient mongoClient = null;
	public static DB db = null;
	
	/**
	 * Function generate query to insert CSVFiles data into database.
	 * i -> Loop counter
	 * @param filePath
	 * @throws ParseException
	 */
	public static void loadProjectFileRecordsIntoDatabase(String filePath) throws ParseException{

		DB db = mongoClient.getDB("dataanalyst");
		DBCollection dbCollection = db.getCollection("insert_data");
		CsvData csvData = CsvData.getInstance();
		ArrayList<ProjectRecord> dataRecords = csvData.getRecords();
		BasicDBObject record = null;
		for(int i=0;i<dataRecords.size();i++)
		{
			record = new BasicDBObject("project_name",filePath);
			record.put("date",dataRecords.get(i).getDate());
			record.put("value",dataRecords.get(i).getPowerValue());
			record.put("unit",dataRecords.get(i).getUnit());
			dbCollection.insert(record);
		}
		
		mongoClient.close();
	}
	
	/**
	 * Generate query to insert proposition of new power consumption (reduction chart)
	 * @throws ParseException
	 */
	public static void loadReductionChartRecordsIntoDatabase() throws ParseException{
		DB db = mongoClient.getDB("dataanalyst");
		DBCollection dbCollection = db.getCollection("output_data");
		ProjectResources projectResources = ProjectResources.getInstance();
		ArrayList<ReductionChartRecord> records = projectResources.getReductionChartRecords();
		ArrayList<ReductionPeakRecord> cuts;
		
		BasicDBObject document = new BasicDBObject();
		document.put("project_name", projectResources.getProjectName());
		document.put("cut_line", projectResources.getPeaksCutLine());
		document.put("price",projectResources.getPrice());
		document.put("total_power_amount", projectResources.getProjectTotalPower());
		document.put("total_cost", projectResources.getProjectTotalCost());
		document.put("peaks_quantity", projectResources.getProjectPeaksQuantity());
		document.put("reduction", projectResources.getProjectReduction());
		document.put("saved_cost", projectResources.getProjectSavedCost());
		
		BasicDBList cuttedRecords = new BasicDBList();
		
		for(ReductionChartRecord record : records){

			BasicDBObject cuttedRecord = new BasicDBObject();
			cuttedRecord.put("date", record.getDate());
			cuttedRecord.put("energy", record.getPower());
			cuttedRecord.put("cost", record.getCost());
			cuttedRecord.put("mode", record.getMode());
			
			cuts = record.getReductionPeaks();
			try {
				BasicDBList cutsList = new BasicDBList();
				for(ReductionPeakRecord cut: cuts){
					BasicDBObject cutRecord = new BasicDBObject();
					cutRecord.put("location_date", cut.getDate());
					cutRecord.put("reduction", cut.getReduction());
					cutsList.add(cutRecord);
				}
				
				cuttedRecord.put("cuts",cutsList);
			} catch (NullPointerException e) {
				cuttedRecord.put("cuts",null);
			}
			cuttedRecords.add(cuttedRecord);
		}
		document.put("records", cuttedRecords);
		dbCollection.insert(document);
		
		
		mongoClient.close();
	}
	

	/**
	 * Enable to create a Mongo instance
	 * @throws MongoException
	 */
	public static void connectToDB() throws MongoException{
		
		mongoClient = new MongoClient("localhost", 27017);

	}
	
}
