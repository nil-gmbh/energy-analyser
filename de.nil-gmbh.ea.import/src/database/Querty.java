package database;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SimpleTimeZone;
import java.util.TreeMap;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bson.BSON;
import org.bson.BasicBSONObject;
import org.eclipse.jface.viewers.StructuredSelection;
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MapReduceCommand;
import com.mongodb.MapReduceOutput;
import com.mongodb.MongoClient;
import data.ProjectResources;
import data.ProjectRecord;
import enums.HistogramDisplayMode;
import enums.StatisticsDisplayMode;


/**
 * Gets DB records from MongoDB
 * 
 * @author Panda
 *
 */
public class Querty {

	public static MongoClient mongoClient;
	public static DB db;
	public static DBCollection dbCollection;
	private static ProjectResources projectResources = ProjectResources.getInstance();
	
	
	/**
	 * Create a Mongo instance
	 */
	public static void connectToDB(){
			mongoClient = new MongoClient("localhost", 27017);
			db = mongoClient.getDB("dataanalyst");
		
	}
	
	/**
	 * Finds & returns all project names from db
	 * @return projectNames
	 */
	public static String[] loadProjectsList(){
		
		dbCollection = db.getCollection("insert_data");
		List projectsList = dbCollection.distinct("project_name");
		String[] projectNames = (String[]) projectsList.toArray(new String[projectsList.size()]);
		return projectNames;
	}
	
	/**
	 * Finds db records between given date interval
	 * @param firstDate of date interval
	 * @param lastDate of date interval
	 */
	public static void loadRecords(Date firstDate, Date lastDate){
		ArrayList<ProjectRecord> records = new ArrayList<ProjectRecord>();
		
		dbCollection = db.getCollection("insert_data");
		
		BasicDBObject querty = new BasicDBObject("$gte",firstDate).append("$lte",lastDate),
			whereQuerty= new BasicDBObject("project_name",projectResources.getProjectName()).append("date",querty);

		DBCursor dbCursor = dbCollection.find(whereQuerty);
		
		for(DBObject dbLine: dbCursor){
			Date date = (Date) dbLine.get( "date" );
			Double value = (Double) dbLine.get( "value" );
			String unit = (String) dbLine.get( "unit" );
			records.add(new ProjectRecord(date,value,unit));
		}
		projectResources.setCurrentDbRecords(records);
		dbCursor.close();
	}
	
	/**
	 * Calculate project's records quantity
	 */
	public static void countProjectRecords(){
		BasicDBObject whereQuerty = new BasicDBObject("project_name",projectResources.getProjectName());
		projectResources.setQuantity(dbCollection.find(whereQuerty).count());
	}
	

	/** 
	 * Gets first or last date's records 
	 * @param line -> a number of line in project
	 *  0 = first line of Project
	 *  -1 = last line of Project
	 */
	public static void loadProjectDate(int line){
		BasicDBObject whereQuerty= new BasicDBObject("project_name",projectResources.getProjectName()),
				dbSort= new BasicDBObject("_id",-1),
				dbLine = null;
		
		if(line==0){
			dbLine = (BasicDBObject) dbCollection.findOne(whereQuerty);
			projectResources.setProjectFirstDate((Date) dbLine.get("date"));
		}else if(line==-1){
			//sorts collection descending and gets first records
			DBCursor dbCursor = dbCollection.find(whereQuerty).sort(dbSort);
			dbLine = (BasicDBObject) dbCursor.next();
			projectResources.setProjectLastDate((Date) dbLine.get("date"));
			dbCursor.close();
		}
		
	}
	
	/**
	 * Gets units record of Project
	 */
	public static void loadUnitRecord(){
		
		BasicDBObject whereQuerty= new BasicDBObject("project_name",projectResources.getProjectName());
		List list = dbCollection.distinct("unit",whereQuerty);
		projectResources.setUnits(list.toString());
		
	}
	
	/**
	 * Gets min max and mean values from db for DisplayMode.PROJECT & StatisticsDisplayMode.DIAGRAM (DBRecordsStatisticView)
	 * @param firstDate of date interval
	 * @param lastDate of date interval
	 */
	public static void loadStatisticsValues(Date firstDate,Date lastDate){

		//prepare date to calculate and to display
		String map = "function () {"+
				"var x = {value:this.value};"+
		    "emit(this.project_name, { min : x , max : x,sum:this.value,num:1} )"+
		"}";
		
		//calculate data (min,max,sum,quantity-num) & return result
		String reduce = "function (key, values) {"+
				"var res = values[0];"+
				"for ( var i=1; i<values.length; i++ ) {"+
					"if ( values[i].min.value < res.min.value ) "+
					"res.min = values[i].min;"+
					"if ( values[i].max.value > res.max.value )"+ 
					"res.max = values[i].max;"+
		
					" res.sum += values[i].sum;"+
					"res.num+= values[i].num;"+
    		"}"+
    		"return res;"+
		"}";
		
		//calculate mean - average
		String finalize = "function (who,res){"+
			"res.avg = res.sum/res.num;"+
			"return res;"+
			"}";
		
		BasicDBObject dateQuerty = new BasicDBObject("$gte",firstDate).append("$lte",lastDate),
			query = new BasicDBObject("project_name",projectResources.getProjectName()).append("date", dateQuerty);
		
		MapReduceCommand cmd = new MapReduceCommand(dbCollection, map, reduce,
                null, MapReduceCommand.OutputType.INLINE, query);

		cmd.setFinalize(finalize);
		
		MapReduceOutput out = dbCollection.mapReduce(cmd);
		for (DBObject dbObj : out.results()) {

			DBObject dbLine = (DBObject) dbObj.get("value"),
					dbKey;	
			
			dbKey = (DBObject) dbLine.get("min");
			projectResources.setMin((Double) dbKey.get("value"));
			
			dbKey = (DBObject) dbLine.get("max");
			projectResources.setMax((Double) dbKey.get("value"));
			
			projectResources.setMean((Double) dbLine.get("avg"));
			
		}
		
	}
	
	
	
	/**
	 * Gets statistics values for statisticsChart. Each statistics perspective has their own keyName and keyQuerty. 
	 * Some one of them has to be define by time - objectName, objectQuerty.
	 * @param firstDate of date interval
	 * @param lastDate of date interval
	 * @param statisticsSelection: 
	 *  StatisticsDisplayMode.YEARS,
	 *  StatisticsDisplayMode.MONTHS,
	 *  StatisticsDisplayMode.WEEKS,
	 *  StatisticsDisplayMode.DAYS,
	 *  StatisticsDisplayMode.HOURS;
	 * @param needsToSpecifyTime - true, if need to be determinate by time. 
	 * For: HistogramDisplayMode.PROJECT || HistogramDisplayMode.CUSTOM || StatisticsDisplayMode.WEEKS
	 */
	public static void loadStatisticsValues(Date firstDate,Date lastDate,StructuredSelection statisticsSelection,boolean needsToSpecifyTime){
		
		String keyName = null,keyQuery = null,
				objectQuery = null,objectName = null;
		Map<String,double[]> map= new HashMap<>();
		Map<Integer,double[]> sortedMap;	
		
		if(!needsToSpecifyTime){
			sortedMap = new TreeMap<>();
		}else sortedMap = new HashMap<>();
		
		switch((StatisticsDisplayMode)statisticsSelection.getFirstElement()){
		
			case YEARS:{
				keyName = "year";
				keyQuery = "year:this.date.getYear()+1900";
				objectQuery = "year:this.date.getYear()+1900";
				objectName = "year";
				break;
			}case MONTHS:{
				keyName = "month";
				keyQuery = "month:this.date.getMonth()+1";
				objectQuery = "year:this.date.getYear()+1900";
				objectName = "year";
				break;
			}case WEEKS:{
				keyName="week";
				keyQuery="week: this.date.getWeekNumber()";
				objectName="year";
				objectQuery="year:this.date.getYear()+1900";
				break;
			}case DAYS:{
				keyName = "day";
				keyQuery = "day:this.date.getDate()";
				objectQuery = "month:this.date.getMonth()+1";
				objectName = "month";
				break;
			}case HOURS:{
				keyName = "hour";
				keyQuery = "hour:this.date.getHours()";
				objectQuery = "day:this.date.getDay()";
				objectName = "day";
				break;
			}default:
			break;
		}
		
		
		//prepare date to calculate and to display
		String m = "function () {"+
				"var x = {value:this.value};"+
				"Date.prototype.getWeekNumber = function(){"+
            "var d = new Date(+this);"+
            "d.setDate(d.getDate()+4-(d.getDay()||7));"+
            "return Math.ceil((((d-new Date(d.getFullYear(),0,1))/8.64e7)+1)/7);"+
        "};"+
		    "emit({"+objectQuery+","+keyQuery+"}, { min : x , max : x,sum:this.value,num:1} )"+
		"}";
		
		//calculate data (min,max,sum,quantity-num) & return result
		String r = "function (key, values) {"+
				"var res = values[0];"+
				"for ( var i=1; i<values.length; i++ ) {"+
					"if ( values[i].min.value < res.min.value ) "+
					"res.min = values[i].min;"+
					"if ( values[i].max.value > res.max.value )"+ 
					"res.max = values[i].max;"+
		
					" res.sum += values[i].sum;"+
					"res.num+= values[i].num;"+
    		"}"+
    		"return res;"+
		"}";
		
		//calculate mean - average
		String f = "function (who,res){"+
			"res.avg = res.sum/res.num;"+
			"return res;"+
			"}";
		
		BasicDBObject dateQuert = new BasicDBObject("$gte",firstDate).append("$lte",lastDate),
			query = new BasicDBObject("project_name",projectResources.getProjectName()).append("date", dateQuert),
					sortQuerty = new BasicDBObject("_id",1);
		
		MapReduceCommand cmd = new MapReduceCommand(dbCollection, m, r,
                null, MapReduceCommand.OutputType.INLINE, query);

		cmd.setLimit(0);
		cmd.setFinalize(f);
		cmd.setSort(sortQuerty);
		
		
		MapReduceOutput out = dbCollection.mapReduce(cmd);
		for (DBObject dbObj : out.results()) {

			DBObject dbLine = (DBObject) dbObj.get("value"),
					dbMax = (DBObject) dbLine.get("max"),
					dbMin = (DBObject) dbLine.get("min"), 
					dbId = (DBObject) dbObj.get("_id");
			
			Integer key = ((Double) dbId.get(keyName)).intValue();
			Integer id = ((Double) dbId.get(objectName)).intValue();

			double[] statistics = new double[]{(double) dbMin.get("value"),(double) dbMax.get("value"),(double) dbLine.get("avg")};
			
			//set keyMap based if it's need to specify in time
			String keyMap="";
			if(needsToSpecifyTime){
				keyMap = id+"-"+key;
			}else{
				sortedMap.put(key,statistics);
				keyMap = ""+key;
			}
			map.put(keyMap, statistics);
		}

		projectResources.setCustomStatistics(map);
		
	}


	/**
	 * Delete project from DB
	 * @param projectName
	 */
	public static void deleteDBProject(String projectName){
		
		BasicDBObject removeQwerty = new BasicDBObject("project_name",projectName);
		dbCollection.remove(removeQwerty);
		
	}

	/**
	 * Checks if in database are records for given date interval. Provides return value for setting previous & next date.
	 * @param firstDate of date interval
	 * @param lastDate of date interval
	 * @return false - none or one;
	 * true - if is more than one record
	 */
	public static boolean checkDateRecords(Date firstDate,Date lastDate) {
		
		dbCollection = db.getCollection("insert_data");
		
		BasicDBObject dateQuerty = new BasicDBObject("$gte",firstDate).append("$lte",lastDate),
				query = new BasicDBObject("project_name",projectResources.getProjectName()).append("date", dateQuerty);
		
		DBCursor dbCursor = dbCollection.find(query);

		if(dbCursor.count()>1 & !dbCursor.equals(null)){
			return true;
		}else
			return false;
	}
	
}