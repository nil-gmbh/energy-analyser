package de.nilgmbh.ea.ui.e3.graphicalComponents;

import java.util.Calendar;
import java.util.Date;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import data.ProjectResources;

/**
 * Model for: DayDialog,MonthDialog,YearDialog & CustomDialog. DateDialogs are used to choose date interval for SWTChart chart.
 * Date interval can be selected only in project record period. 
 * @author Panda
 *
 */
public abstract class BaseDateDialog extends Dialog {

	protected static ProjectResources projectResources;
	
	public BaseDateDialog(Shell parentShell) {
		super(parentShell);
		projectResources = ProjectResources.getInstance();
	}

	protected static Calendar cal = Calendar.getInstance(), cal2 = Calendar.getInstance();
	protected Combo monthChooser, yearChooser;
	protected DateTime firstDateChooser,lastDateChooser;
	protected Date firstDate,lastDate;
	protected final Color white = Display.getCurrent().getSystemColor(SWT.COLOR_WHITE),
			gray = Display.getCurrent().getSystemColor(SWT.COLOR_GRAY);
	
	public Date getFirstDate() {
		return firstDate;
	}

	public Date getLastDate() {
		return lastDate;
	}



	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.FILL).hint(250, SWT.DEFAULT)
				.minSize(350, 300).create());

		cal.set(2000, 0, 1, 0, 0, 0);
		cal2.set(2000, 0, 1, 23, 59, 59);
		
		setUIElements(container);
		loadValuesToFields();
		
		return container;
	}

	

	@Override
	protected void okPressed() {
		firstDate = getStartDate();
		lastDate = getFinalDate();
		super.okPressed();
	}


	/**
	 * Sets up UI Elements for current DateDialog
	 * @param container
	 */
	public abstract void setUIElements(Composite container);
	
	/**
	 * Sets up default values for widgets based on current project
	 */
	public abstract void loadValuesToFields();
	
	/**
	 * Returns selected start date. Used for create chart;
	 * @return (Date) startDate
	 */
	public abstract Date getStartDate();
	
	/**
	 * Returns selected final date. Used for create chart;
	 * @return (Date) finalDate
	 * @return
	 */
	public abstract Date getFinalDate();
	
	/**
	 * Sets date into current dateTime widget
	 * @param dateChooser
	 * @param date
	 */
	protected static void displayDate(DateTime dateChooser, Date date){
		cal.setTime(date);
		dateChooser.setDate(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH));
	}
}
