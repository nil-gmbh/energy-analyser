package de.nilgmbh.ea.ui.e3.graphicalComponents;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;


/**
 * Provides whole date interval selected by user. 
 * @author Panda
 *
 */
public class CustomDateDialog extends BaseDateDialog {

	private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	
	public CustomDateDialog(Shell parentShell) {
		super(parentShell);
		// TODO Auto-generated constructor stub
	}

	protected void configureShell(Shell parentShell) {
		super.configureShell(parentShell);
		parentShell.setText("Choose Date Interval");
	}
	
	@Override
	public void setUIElements(Composite container) {
		GridLayout layout = new GridLayout(2, true);
		layout.marginHeight = 10;
		layout.marginWidth = 20;
		container.setLayout(layout);
		
		GridData gd = new GridData();
		gd.horizontalAlignment = SWT.FILL;
		gd.verticalAlignment = SWT.FILL;
		gd.grabExcessHorizontalSpace = true;
		gd.grabExcessVerticalSpace = false;
		gd.heightHint = 300;
		
		Label label = new Label(container, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		label.setText("FIRST DAY:");
		label = new Label(container, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		label.setText("LAST DAY:");
		
		firstDateChooser = new DateTime(container,SWT.DATE); 
		firstDateChooser.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());	
		
//		firstDateChooser.addListener(SWT.Selection, new Listener(){
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//			Date newDate = null;
//			
//			@Override
//			public void handleEvent(Event event) {
//				
//				
//				Date firstDateRecord = projectResources.getProjectFirstDate(),
//						lastDateRecord = projectResources.getProjectLastDate();
//
//				int day = firstDateChooser.getDay();
//	            int month = firstDateChooser.getMonth() + 1;
//	            int year = firstDateChooser.getYear();
//				try {
//					newDate = sdf.parse(year+"-"+month+"-"+day);
//					
//					if((newDate.equals(firstDateRecord) || newDate.after(firstDateRecord)) & newDate.before(lastDateRecord)
//							& !newDate.after(cal2.getTime())){
//						cal.setTime(newDate);
//						lastDateChooser.setEnabled(true);
//						firstDateChooser.update();
//			         }else {
//			        	 cal.setTime(firstDateRecord);
//			        	 firstDateChooser.update();
//			         }
//				} catch (ParseException | NullPointerException e) {
//					 
//				}
//				
//				displayDate(firstDateChooser,cal.getTime());
//			
//			}
//		
//		});
		lastDateChooser = new DateTime(container,SWT.DATE); 
		lastDateChooser.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());	
		
		Label firstDateRecordLabel = new Label (container, SWT.NONE);
		firstDateRecordLabel.setText("From " + sdf.format(projectResources.getProjectFirstDate()));
		Label lastDateRecordLabel = new Label (container, SWT.NONE);
		lastDateRecordLabel.setText(" to " +sdf.format(projectResources.getProjectLastDate()));
//		lastDateChooser.addListener(SWT.Selection, new Listener(){
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//			Date newDate = null;
//			@Override
//			public void handleEvent(Event event) {
//				
//				
//				
//				Date firstDate = projectResources.getProjectFirstDate(),
//						lastDate = projectResources.getProjectLastDate();
//				int day = lastDateChooser.getDay();
//	            int month = lastDateChooser.getMonth() + 1;
//	            int year = lastDateChooser.getYear();
//				try {
//					
//					newDate = sdf.parse(year+"-"+month+"-"+day);
//					if((newDate.equals(firstDate) || newDate.after(firstDate))&((newDate.equals(lastDate) || newDate.before(lastDate)))
//							& !newDate.before(cal.getTime())){
//						cal2.setTime(newDate);
//						lastDateChooser.update();
//				    }else {
//				     	 cal2.setTime(lastDate);
//				     	cal2.set(Calendar.DAY_OF_MONTH, cal2.get(Calendar.DAY_OF_MONTH)-1);
//				     	lastDateChooser.setEnabled(false);
//				       	 
//				       	lastDateChooser.update();
//				   }
//					
//					
//				} catch (ParseException | NullPointerException e) {
//					 
//				}
//				
//				
//				
//				displayDate(lastDateChooser, cal2.getTime());
//				
//				
//			}
//
//		});
		
		
	}

	@Override
	public void loadValuesToFields() {
		cal.setTime(projectResources.getProjectFirstDate());
		firstDateChooser.setDate(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH));
		cal2.setTime(projectResources.getProjectLastDate());
		lastDateChooser.setDate(cal2.get(Calendar.YEAR),cal2.get(Calendar.MONTH),cal2.get(Calendar.DAY_OF_MONTH));

	}

	@Override
	public Date getStartDate() {
		cal.set(firstDateChooser.getYear(), firstDateChooser.getMonth(), firstDateChooser.getDay(),0,0,0);
		return cal.getTime();
	}

	@Override
	public Date getFinalDate() {
		cal2.set(lastDateChooser.getYear(), lastDateChooser.getMonth(), lastDateChooser.getDay(),23,59,59);
		return cal2.getTime();
	}

}
