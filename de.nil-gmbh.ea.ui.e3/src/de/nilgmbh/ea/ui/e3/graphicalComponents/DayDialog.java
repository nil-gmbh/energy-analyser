package de.nilgmbh.ea.ui.e3.graphicalComponents;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;


public class DayDialog extends BaseDateDialog {
	

	public DayDialog(Shell parentShell) {
		super(parentShell);
	}
	
	protected void configureShell(Shell parentShell) {
		super.configureShell(parentShell);
		parentShell.setText("Choose Day");
	}

	@Override
	public void setUIElements(Composite container) {
		
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 10;
		layout.marginWidth = 10;
		container.setLayout(layout);
		
		GridData gd = new GridData();
		gd.horizontalAlignment = SWT.FILL;
		gd.verticalAlignment = SWT.FILL;
		gd.grabExcessHorizontalSpace = true;
		gd.grabExcessVerticalSpace = false;
		gd.heightHint = 250;
		gd.widthHint = 200;
		

		firstDateChooser = new DateTime(container,SWT.DATE); 
		firstDateChooser.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());	
		
		firstDateChooser.addListener(SWT.Selection, new Listener(){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date newDate = null;
			
			@Override
			public void handleEvent(Event event) {
				
				
				Date firstDateRecord = projectResources.getProjectFirstDate(),
						lastDateRecord = projectResources.getProjectLastDate();

				int day = firstDateChooser.getDay();
	            int month = firstDateChooser.getMonth() + 1;
	            int year = firstDateChooser.getYear();
				try {
					newDate = sdf.parse(year+"-"+month+"-"+day);
					
					if((newDate.equals(firstDateRecord) || newDate.after(firstDateRecord))&((newDate.equals(lastDateRecord) || newDate.before(lastDateRecord)))){
						cal.setTime(newDate);

			         }else {
			        	 cal.setTime(firstDateRecord);
			         }
				} catch (ParseException | NullPointerException e) {
					 
				}
				
				displayDate(firstDateChooser,cal.getTime());
			
			}
		
		});
	}

	@Override
	public void loadValuesToFields() {
		cal.setTime(projectResources.getProjectFirstDate());
		firstDateChooser.setDate(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH));
		
	}

	@Override
	public Date getStartDate() {
		cal.set(firstDateChooser.getYear(),firstDateChooser.getMonth(), firstDateChooser.getDay(),0,0,0);
		return cal.getTime();
	}

	@Override
	public Date getFinalDate() {
		cal.set(firstDateChooser.getYear(),firstDateChooser.getMonth(), firstDateChooser.getDay(),23,59,59);
		return cal.getTime();
	}
}
