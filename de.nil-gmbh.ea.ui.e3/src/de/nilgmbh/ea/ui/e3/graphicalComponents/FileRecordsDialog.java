package de.nilgmbh.ea.ui.e3.graphicalComponents;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import data.CsvData;
import data.ProjectRecord;

/**
 * Displays all converted file data into records.
 * @author Panda
 *
 */
public class FileRecordsDialog extends Dialog {

	public FileRecordsDialog(Shell parentShell) {
		super(parentShell);

	}

	protected void configureShell(Shell parentShell) {
		super.configureShell(parentShell);
		parentShell.setText("Parserd lines");
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.FILL).hint(450, SWT.DEFAULT)
				.minSize(350, 300).create());

		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 5;
		layout.marginWidth = 5;
		container.setLayout(layout);

		GridData gd = new GridData();
		gd.horizontalAlignment = SWT.FILL;
		gd.verticalAlignment = SWT.FILL;
		gd.grabExcessHorizontalSpace = true;
		gd.grabExcessVerticalSpace = false;
		gd.heightHint = 300;

		CsvData csvData = CsvData.getInstance();
		
		// TABLEVIEWER
		TableViewer tv = new TableViewer(container, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		tv.getTable().setLinesVisible(true);
		tv.getTable().setHeaderVisible(true);
		tv.getTable().setLayoutData(gd);
		TableViewerColumn countColumn = new TableViewerColumn(tv, SWT.LEFT);
		countColumn.getColumn().setText("No");
		countColumn.getColumn().setWidth(50);
		TableViewerColumn dateColumn = new TableViewerColumn(tv, SWT.CENTER);
		dateColumn.getColumn().setText("Date");
		dateColumn.getColumn().setWidth(100);
		TableViewerColumn timeColumn = new TableViewerColumn(tv, SWT.CENTER);
		timeColumn.getColumn().setText("Time");
		timeColumn.getColumn().setWidth(100);
		TableViewerColumn powerColumn = new TableViewerColumn(tv, SWT.CENTER);
		powerColumn.getColumn().setText("Energy[W]");
		powerColumn.getColumn().setWidth(100);
		TableViewerColumn unitColumn = new TableViewerColumn(tv, SWT.CENTER);
		unitColumn.getColumn().setText("Units");
		unitColumn.getColumn().setWidth(50);
		tv.setContentProvider(new RecordContentProvider());
		tv.setLabelProvider(new RecordLabelProvider());
		tv.setInput(csvData.getRecords());

		return container;
	}

	private class RecordContentProvider extends ArrayContentProvider {

	}

	private class RecordLabelProvider extends LabelProvider implements ITableLabelProvider {

		private int count = 0;

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			if (element instanceof ProjectRecord) {
				ProjectRecord record = ((ProjectRecord) element);
				switch (columnIndex) {
				case 0:
					return String.valueOf(count++);
				case 1:
					return record.displaySimpleDate();
				case 2:
					return record.displaySimpleTime();
				case 3:
					return String.valueOf(record.getPowerValue());
				case 4:
					return record.getUnit();
				default:
					return null;
				}
			} else
				return "";
		}

	}

}
