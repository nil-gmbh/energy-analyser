package de.nilgmbh.ea.ui.e3.graphicalComponents;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Widget;

import data.ProjectResources;

public class MonthDialog extends BaseDateDialog {

	public MonthDialog(Shell parentShell) {
		super(parentShell);
		//ProjectResources.loadMonthsInYearValues();
	}

	protected void configureShell(Shell parentShell) {
		super.configureShell(parentShell);
		parentShell.setText("Choose Month based on year");
	}
	
	@Override
	public void setUIElements(Composite container) {
		GridLayout layout = new GridLayout(2, true);
		layout.marginHeight = 10;
		layout.marginWidth = 10;
		container.setLayout(layout);
		
		GridData gd = new GridData();
		gd.horizontalAlignment = SWT.FILL;
		gd.verticalAlignment = SWT.FILL;
		gd.grabExcessHorizontalSpace = true;
		gd.grabExcessVerticalSpace = false;
		gd.heightHint = 300;
		
		Label label = new Label(container, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		label.setText("YEAR:");
		label = new Label(container, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		label.setText("MONTH:");
		
		
		
		yearChooser = new Combo(container,SWT.READ_ONLY);
		yearChooser.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		yearChooser.addSelectionListener(new SelectionListener(){
			int year,month;
				@Override
				public void widgetSelected(SelectionEvent e) {
							
					year = Integer.parseInt(yearChooser.getItem(yearChooser.getSelectionIndex()));
					monthChooser.setItems(projectResources.monthsToDisplay(year));
					monthChooser.select(0);
					month = monthChooser.getSelectionIndex();
					cal.set(year, month,1,0,0,0);
					cal2.set(year, month,cal2.getActualMaximum(Calendar.DAY_OF_MONTH),23,59,59);
							
				}
			
				@Override
				public void widgetDefaultSelected(SelectionEvent e) {
					// TODO Auto-generated method stub
							
				}
						
		});
		
		
		monthChooser = new Combo(container,SWT.READ_ONLY);
		monthChooser.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		monthChooser.addSelectionListener(new SelectionListener(){
			int year,month;
			@Override
			public void widgetSelected(SelectionEvent e) {
								
			
				year = Integer.parseInt(yearChooser.getItem(yearChooser.getSelectionIndex()));
				
				
				month = monthChooser.getSelectionIndex();
				
				cal.set(year, month,1,0,0,0);
				
				cal2.set(year, month,cal.getActualMaximum(Calendar.DAY_OF_MONTH),23,59,59);
				
								
								
			}
			
		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			// TODO Auto-generated method stub
							
		}
					
	});

	}

	@Override
	public void loadValuesToFields() {
		yearChooser.setItems(projectResources.yearsToDisplay());
		yearChooser.select(0);
		
		int year = Integer.parseInt(yearChooser.getItem(0)),
				month;
		monthChooser.setItems(projectResources.monthsToDisplay(year));
		monthChooser.select(0);
		
		month = monthChooser.getSelectionIndex();

		cal.set(year, month,1,0,0,0);

		cal2.set(year, month,cal2.getActualMaximum(Calendar.DAY_OF_MONTH),23,59,59);


	}

	@Override
	public Date getStartDate() {
		return cal.getTime();
	}

	@Override
	public Date getFinalDate() {
		// TODO Auto-generated method stub
		return cal2.getTime();
	}

}
