package de.nilgmbh.ea.ui.e3.graphicalComponents;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import data.ProjectResources;
import database.Querty;

/**
 * Provides informations about projects: records' quantity, first & last Date and units values 
 * @author Panda
 *
 */
public class ProjectInfoDialog extends Dialog {

	private ProjectResources projectResources;
	
	private Text recordsQuantityText,firstDateText,lastDateText,unitsText;
	public ProjectInfoDialog(Shell parentShell) {
		super(parentShell);
		
	}
	
	protected void configureShell(Shell parentShell) {
		super.configureShell(parentShell);
		projectResources = ProjectResources.getInstance();
		parentShell.setText("Project Informations: "+projectResources.getProjectName());
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = new Composite(parent, SWT.NONE);
		container.setLayoutData(GridDataFactory.swtDefaults().align(SWT.FILL, SWT.FILL).hint(450, SWT.DEFAULT)
				.minSize(350, 300).create());
		GridLayout layout = new GridLayout(4, true);
		layout.marginHeight = 10;
		layout.marginWidth = 10;
		container.setLayout(layout);

		GridData gd = new GridData();
		gd.horizontalAlignment = SWT.CENTER;
		gd.verticalAlignment = SWT.CENTER;
		gd.grabExcessHorizontalSpace = true;
		gd.grabExcessVerticalSpace = false;
		gd.heightHint = 300;
		
		//Colors
		Color white = Display.getCurrent().getSystemColor(SWT.COLOR_WHITE);
		Color gray = Display.getCurrent().getSystemColor(SWT.COLOR_GRAY);
		container.setBackground(white);
		
		//project graphical components
		//Information Fields
		Label label = new Label(container, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 2).grab(true, false).create());
		label.setText("QUANTITY:");
		label = new Label(container, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 2).grab(true, false).create());
		label.setText("FIRST DAY:");
		label = new Label(container, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 2).grab(true, false).create());
		label.setText("LAST DAY:");
		label = new Label(container, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 2).grab(true, false).create());
		label.setText("UNITS:");
		

		
		//QUANTITY OF RECORDS TEXT
		recordsQuantityText = new Text(container,SWT.BORDER|SWT.READ_ONLY|SWT.CENTER);
		recordsQuantityText.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		recordsQuantityText.setBackground(gray);
		
		//FIRST RECORD'S DAY TEXT
		firstDateText = new Text(container,SWT.BORDER|SWT.READ_ONLY|SWT.CENTER);
		firstDateText.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		firstDateText.setBackground(gray);
		
		//LAST RECORD'S DAY TEXT
		lastDateText = new Text(container,SWT.BORDER|SWT.READ_ONLY|SWT.CENTER);
		lastDateText.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		lastDateText.setBackground(gray);
		
		//UNITS TEXT
		unitsText = new Text(container,SWT.BORDER|SWT.READ_ONLY|SWT.CENTER);
		unitsText.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		unitsText.setBackground(gray);
		
		clean();
		loadValuesIntoFields();
		return container;
	}
	
	
	
	public void clean(){
		recordsQuantityText.setText("");
		firstDateText.setText("");
		lastDateText.setText("");
		unitsText.setText("");
	}
	
	private void loadValuesIntoFields(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		recordsQuantityText.setText(""+projectResources.getQuantity());
		firstDateText.setText(""+sdf.format(projectResources.getProjectFirstDate()));
		lastDateText.setText(""+sdf.format(projectResources.getProjectLastDate()));
		unitsText.setText(""+projectResources.getUnits());
	}
}
