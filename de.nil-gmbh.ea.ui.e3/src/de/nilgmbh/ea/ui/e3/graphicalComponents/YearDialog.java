package de.nilgmbh.ea.ui.e3.graphicalComponents;

import java.util.Calendar;
import java.util.Date;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;

import data.ProjectResources;

public class YearDialog extends BaseDateDialog {

	public YearDialog(Shell parentShell) {
		super(parentShell);
		
		
	}

	protected void configureShell(Shell parentShell) {
		super.configureShell(parentShell);
		parentShell.setText("Choose Year");
	}
	
	@Override
	public void setUIElements(Composite container) {
		GridLayout layout = new GridLayout(1, true);
		layout.marginHeight = 10;
		layout.marginWidth = 10;
		container.setLayout(layout);
		
		GridData gd = new GridData();
		gd.horizontalAlignment = SWT.FILL;
		gd.verticalAlignment = SWT.FILL;
		gd.grabExcessHorizontalSpace = true;
		gd.grabExcessVerticalSpace = false;
		gd.heightHint = 300;
		
		yearChooser = new Combo(container,SWT.READ_ONLY);
		yearChooser.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		yearChooser.addSelectionListener(new SelectionListener(){
					
			int year;
			@Override
			public void widgetSelected(SelectionEvent e) {
									
			year = Integer.parseInt(yearChooser.getItem(yearChooser.getSelectionIndex()));
			cal.set(year, 0,1,0,0,0);
			cal2.set(year, 11,31,23,59,59);
						
			}
					
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			// TODO Auto-generated method stub
									
			}
						
		});
		
	}

	@Override
	public void loadValuesToFields() {

		yearChooser.setItems(projectResources.yearsToDisplay());
		yearChooser.select(0);
		
		int year = Integer.parseInt(yearChooser.getItem(0));
		
		cal.set(year, 0,1,0,0,0);
		cal2.set(year, 11,31,23,59,59);
	}

	@Override
	public Date getStartDate() {
		return cal.getTime();
	}

	@Override
	public Date getFinalDate() {
		// TODO Auto-generated method stub
		return cal2.getTime();
	}

}
