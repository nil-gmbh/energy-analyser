package de.nilgmbh.ea.ui.e3.views;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.part.ViewPart;
import org.swtchart.Chart;
import org.swtchart.IAxis;
import org.swtchart.IAxis.Position;
import org.swtchart.IAxisSet;
import org.swtchart.IAxisTick;
import org.swtchart.IBarSeries;
import org.swtchart.ILineSeries;
import org.swtchart.ILineSeries.PlotSymbolType;
import org.swtchart.ISeries;
import org.swtchart.ISeries.SeriesType;
import org.swtchart.ISeriesLabel;
import org.swtchart.ITitle;
import org.swtchart.internal.series.SeriesLabel;
import org.swtchart.internal.series.SeriesSet;

import data.ProjectResources;
import data.ProjectRecord;
import database.Querty;
import de.nilgmbh.ea.ui.e3.graphicalComponents.BaseDateDialog;
import de.nilgmbh.ea.ui.e3.graphicalComponents.CustomDateDialog;
import de.nilgmbh.ea.ui.e3.graphicalComponents.DayDialog;
import de.nilgmbh.ea.ui.e3.graphicalComponents.MonthDialog;
import de.nilgmbh.ea.ui.e3.graphicalComponents.ProjectInfoDialog;
import de.nilgmbh.ea.ui.e3.graphicalComponents.YearDialog;
import enums.HistogramDisplayMode;
import enums.MonthName;
import enums.StatisticsDisplayMode;

/**
 * Second part of application. 
 * Database records: histogram & statistics View.
 * Simple analysis: histogram, aggregation by time, min,max,mean - statistics
 * @author Panda
 *
 */
public class DBRecordsStatisticsView extends ViewPart{

	public static final String ID = DBRecordsStatisticsView.class.getName();
	public static boolean dbConnection;
	private static ProjectResources projectResources;
	public DBRecordsStatisticsView() {
		projectResources = ProjectResources.getInstance();
	}
	
	private static Calendar cal = Calendar.getInstance(),cal2 = Calendar.getInstance();
	private Button refreshButton,projectInfoButton,deleteDBFile,previousDateButton,nextDateButton;
	private ComboViewer displayHistogramCombo,displayStatisticsCombo,projectsListCombo;
	private Chart histogram;
	

	private String currentHistogramSelection, currentStatisticsChartSelection;
	private StructuredSelection histogramSelection, statisticsChartSelection;

	private static String[] projectsNames;
	private static MessageBox message;

	private GridLayout layout;
	protected static boolean highlight;
//	private static int highlightX;
//    private static int highlightY;
	
	@Override
	public void createPartControl(Composite parent) {
		
		Composite container = new Composite(parent, SWT.BORDER);
		container.setLayout(GridLayoutFactory.fillDefaults().numColumns(5).equalWidth(true).create());
		layout = (GridLayout) container.getLayout();
		container.setLayoutData(GridDataFactory.fillDefaults().span(1, 15).grab(true, true).create());
		layout.marginWidth = 10;
		layout.marginHeight = 10;
		container.setLayout(layout);

		Display dispaly = Display.getCurrent();
		Color white = dispaly.getSystemColor(SWT.COLOR_WHITE);
		container.setBackground(white);
		
		//Refresh button
		refreshButton = new Button(container,SWT.PUSH);
		refreshButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		refreshButton.setText("Refresh");
		
		//project list Combo Viewer
		projectsListCombo = new ComboViewer(container,SWT.BORDER|SWT.READ_ONLY);
		projectsListCombo.setContentProvider(ArrayContentProvider.getInstance());
		projectsListCombo.setLabelProvider(new LabelProvider());
		projectsListCombo.getCombo().setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		projectsListCombo.getCombo().setText("PROJECT LIST");

		//project's simple information Button
		projectInfoButton = new Button(container,SWT.PUSH|SWT.WRAP);
		projectInfoButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		projectInfoButton.setText("Project Informations");
		
		//display Histogram Combo Viewer
		displayHistogramCombo = new ComboViewer(container,SWT.READ_ONLY);
		displayHistogramCombo.setContentProvider(ArrayContentProvider.getInstance());
		displayHistogramCombo.setLabelProvider(new LabelProvider());
		HistogramDisplayMode[] list = HistogramDisplayMode.values();
		displayHistogramCombo.setInput(list);
		displayHistogramCombo.getCombo().setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		displayHistogramCombo.getCombo().setToolTipText("Select chart's display mode");

		//display Statistics Combo Viewer
		displayStatisticsCombo = new ComboViewer(container,SWT.READ_ONLY);
		displayStatisticsCombo.setContentProvider(ArrayContentProvider.getInstance());
		displayStatisticsCombo.setLabelProvider(new LabelProvider());
		displayStatisticsCombo.getCombo().setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		displayStatisticsCombo.getCombo().setToolTipText("Select statistics display mode");
		StatisticsDisplayMode[] list2 = 
			{StatisticsDisplayMode.DIAGRAM,
				StatisticsDisplayMode.YEARS,
				StatisticsDisplayMode.MONTHS};
		displayStatisticsCombo.setInput(list2);
		displayStatisticsCombo.getCombo().select(0);
		
		
		//-------------------------------------------------
		//Previous & Next Date Panel
		Composite panel = new Composite(container, SWT.NULL);
		panel.setLayout(GridLayoutFactory.fillDefaults().numColumns(10).equalWidth(true).create());
		panel.setLayoutData(GridDataFactory.fillDefaults().span(5, 1).grab(true, false).create());
		
		//empty line
		Label emptyLine = new Label(panel, SWT.CENTER);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().span(6, 1).grab(true, false).create());
		
		//previous date Button
		previousDateButton = new Button(panel, SWT.PUSH);
		previousDateButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		previousDateButton.setText("Previous Date");
		
		//next Date Button
		nextDateButton = new Button(panel, SWT.PUSH);
		nextDateButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		nextDateButton.setText("Next Date");
		
		//--------------------------------------------------
		//HISTOGRAM
		histogram = new Chart(container, SWT.NONE);
		GridData gridData = new GridData();
		gridData.verticalSpan = 20;
		gridData.horizontalSpan = 6;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		
		histogram.setLayoutData(gridData);
		histogram.getAxisSet().getXAxis(0).getTitle().setText("Days");
		histogram.getAxisSet().getYAxis(0).getTitle().setText("Power [kW]");
		histogram.getTitle().setText("");
		
		
		//==============================================
		//delete current project from DB -  DELETEDBFILE SELECTION LISTENER
		
//		deleteDBFile = new Button(container, SWT.PUSH);
//		deleteDBFile.setLayoutData(GridDataFactory.fillDefaults().span(5, 1).grab(true, false).create());
//		deleteDBFile.setText("Delete project file from DB");
//		deleteDBFile.addSelectionListener(new SelectionListener(){
//
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				
//				
//				Querty.deleteDBProject(projectResources.getProjectName());
//				cleanFields(true);
//				cleanChart(true);
//				
//			}
//
//			@Override
//			public void widgetDefaultSelected(SelectionEvent e) {
//				// TODO Auto-generated method stub
//				
//			}
//			
//		});
//		
//		deleteDBFile.setVisible(true);
		
		cleanAllFields(true);
		projectsListCombo.setInput(projectsNames);
		
		cal.set(2000, 0, 1, 0, 0, 0);
		cal2.setTime(cal.getTime());
		
		//======================================================
		//EVENTS
		
		//chart mouseListener -> display nearest point witch date & powerValue
		//--------------------------------------
//		final Composite plotArea = chart.getPlotArea();
//
//		plotArea.addListener(SWT.MouseHover, new Listener(){
//
//            public void handleEvent(Event event) {
//                
//            	SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
//            	IAxis xAxis = chart.getAxisSet().getXAxis(0);
//                IAxis yAxis = chart.getAxisSet().getYAxis(0);
//
//                double x = xAxis.getDataCoordinate(event.x);
//                double y = yAxis.getDataCoordinate(event.y);
//
//                ISeries[] series = chart.getSeriesSet().getSeries();
//
//                double closestX = 0;
//                Date date = null;
//                //power value
//                double closestY = 0;
//                double minDist = Double.MAX_VALUE;
//
//                /* over all series */
//                for (ISeries serie : series) {
//                    double[] xS = serie.getXSeries();
//                	Date[] xDate = serie.getXDateSeries();
//                    double[] yS = serie.getYSeries();
//
//                    /* check all data points */
//                    try {
//						for (int i = 0; i < xS.length; i++) {
//						    /* compute distance to mouse position */
//						    double newDist = Math.sqrt(Math.pow((x - xS[i]), 2)
//						            + Math.pow((y - yS[i]), 2));
//
//						    /* if closer to mouse, remember */
//						    if (newDist < minDist) {
//						        minDist = newDist;
//						        closestX = xS[i];
//						        date = xDate[i];
//						        closestY = yS[i];
//						    }
//						}
//						
//						 String dateValue="";
//						 
//			                if(closestY==chart.getSeriesSet().getSeries("Min").getYSeries()[0]){
//			                	dateValue = "MIN:";
//			                }else if(closestY==chart.getSeriesSet().getSeries("Max").getYSeries()[0]){
//			                	dateValue = "MAX:";
//			                }else if(closestY==chart.getSeriesSet().getSeries("Mean").getYSeries()[0]){
//			                	dateValue = "MEAN:";
//			                }else
//			                	dateValue = sdf.format(date)+",";
//			                
//			                /* set tooltip of closest data point */
//			                statisticsChartSelection = (StructuredSelection) displayStatisticsCombo.getSelection();
//			               if(statisticsChartSelection.getFirstElement().equals(StatisticsDisplayMode.DIAGRAM)){
//			            	   plotArea.setToolTipText( dateValue+ " " + closestY+"W");
//			               }
//
//			                /* remember closest data point */
//			                highlightX = xAxis.getPixelCoordinate(closestX);
//			                highlightY = yAxis.getPixelCoordinate(closestY);
//
//			                highlight = true;
//
//			                /* trigger repaint (paint highlight) */
//			                plotArea.redraw();
//					} catch (ArrayIndexOutOfBoundsException e) {
//						
//					}catch(NullPointerException e2){
//						
//					}
//                }
//
//               
//            }
//        });
//
//        plotArea.addListener(SWT.MouseMove, new Listener() {
//
//            public void handleEvent(Event arg0) {
//                highlight = false;
//
//                plotArea.redraw();
//            }
//        });
//
//        plotArea.addListener(SWT.Paint, new Listener() {
//
//            public void handleEvent(Event event) {
//                if (highlight) {
//                    GC gc = event.gc;
//
//                    gc.setBackground(Display.getDefault().getSystemColor(
//                            SWT.COLOR_BLACK));
//                    gc.setAlpha(128);
//
//                    gc.fillOval(highlightX - 5, highlightY - 5, 10, 10);
//                }
//            }
//        });
//        
//        plotArea.addListener(SWT.MouseHover, new Listener() {
//
//            @Override
//            public void handleEvent(final Event event) {
//                IAxis xAxis = chart.getAxisSet().getXAxis(0);
//                IAxis yAxis = chart.getAxisSet().getYAxis(0);
//
//                int x = (int) Math.round(xAxis.getDataCoordinate(event.y));
//                double y = yAxis.getDataCoordinate(event.x);
//
//                ISeries[] series = chart.getSeriesSet().getSeries();
//
//                double currentY = 0.0;
//                ISeries currentSeries = null;
//
//                /* over all series */
//                for (ISeries serie : series) {
//                    double[] yS = serie.getYSeries();
//
//                    if (x < yS.length && x >= 0) {
//                        currentY += yS[x];
//                        currentSeries = serie;
//
//                        if (currentY > y) {
//                            y = yS[x];
//                            break;
//                        }
//                    }
//                }
//                
//                statisticsChartSelection = (StructuredSelection) displayStatisticsCombo.getSelection();
//			    if(!statisticsChartSelection.getFirstElement().equals(StatisticsDisplayMode.DIAGRAM)){
//			    	if (currentY >= y) {
//	                    plotArea.setToolTipText(currentSeries.getDescription() + ": " + y);
//	                    
//	                    
//	                } else {
//	                    plotArea.setToolTipText(null);
//	                }
//			    }
//                
//            }
//    }); 
		//---------------------------------------------------
		
		//REFRESH BUTTON SELECTION LISTENER
        //prepare app for load project List
		refreshButton.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				if(projectsListCombo.getCombo().getSelectionIndex()!=-1)
					cleanChart(true);
				cleanAllFields(true);
				loadProjectsNames();
				DBRecordsStatisticsView.this.projectsListCombo.setInput(projectsNames);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		//PROJECT LIST COMBO SELECTION LISTENER
		//new project -> cleaning graphical components,projectResources,and loading values from DB
		projectsListCombo.getCombo().addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				cleanAllFields(false);
				cleanChart(true);
				projectResources.setNewUnusedProject(true);
				loadValues(null,null);
				projectInfoButton.setEnabled(true);
				displayHistogramCombo.getCombo().setEnabled(true);
				displayStatisticsCombo.getCombo().setEnabled(true);
				projectResources.loadMonthsInYearValues();
				displayHistogramCombo.setSelection(new StructuredSelection(HistogramDisplayMode.PROJECT));
				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		//PROJECT INFO BUTTON SELECTION LISTENER
		//open DialogBox with project's informations
		projectInfoButton.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				ProjectInfoDialog statisticsDialog = new  ProjectInfoDialog(DBRecordsStatisticsView.this.getSite().getShell());
				statisticsDialog.open();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		//DISPLAY HISTOGRAM COMBO SELECTION LISTENER
		//display project's mode + checks next&previous Dates + displayStatisticsCombo's preperation
		displayHistogramCombo.getCombo().addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				BaseDateDialog dialog = null;
				
				histogramSelection = (StructuredSelection) displayHistogramCombo.getSelection();
				
				switch((HistogramDisplayMode)histogramSelection.getFirstElement()){
					case DAY:{
						dialog = new DayDialog(getSite().getShell());
						break;
					}case MONTH:{
						dialog = new MonthDialog(getSite().getShell());
						break;
					}case YEAR:{
						dialog = new YearDialog(getSite().getShell());
						break;
					}case CUSTOM:{
						dialog = new CustomDateDialog(getSite().getShell());
						break;
					}case PROJECT:{
						//protection -> do not execute function if previous user selection is the same selection 
						if(!currentHistogramSelection.equals(histogramSelection.getFirstElement().toString()))
							loadValues(projectResources.getProjectFirstDate(),projectResources.getProjectLastDate());
						break;
					}default:{	
						break;
					}
				}
					
				currentHistogramSelection = histogramSelection.getFirstElement().toString();
				projectResources.clean(false);
					
				try {
					if(dialog.open()==Dialog.OK){
						Date firstDate,lastDate;
						firstDate = dialog.getFirstDate();
						lastDate = dialog.getLastDate();
						cal.setTime(firstDate);
						cleanChart(true);
						loadValues(firstDate, lastDate);
					}

				} catch (NullPointerException e1) {
					
				}catch (IllegalArgumentException e2){
					message = new MessageBox(DBRecordsStatisticsView.this.getSite().getShell(),SWT.OK);
					message.setText("Cannot create chart");
					message.setMessage("Records' date interval is not enough for selected perspective");
					message.open();
				}

				//displayStatisticsCombos's values preparation
				switch((HistogramDisplayMode)histogramSelection.getFirstElement()){
					case DAY:{
						StatisticsDisplayMode[] list = 
							{StatisticsDisplayMode.DIAGRAM,
								StatisticsDisplayMode.HOURS};
						displayStatisticsCombo.setInput(list);
						break;
					}case MONTH:{
						StatisticsDisplayMode[] list = 
							{StatisticsDisplayMode.DIAGRAM,
								StatisticsDisplayMode.WEEKS,
								StatisticsDisplayMode.DAYS};
						displayStatisticsCombo.setInput(list);
						break;
					}case YEAR:{
						StatisticsDisplayMode[] list = 
							{StatisticsDisplayMode.DIAGRAM,
								StatisticsDisplayMode.MONTHS,
								StatisticsDisplayMode.WEEKS};
						displayStatisticsCombo.setInput(list);
						break;
					}case CUSTOM:{
						//Dynamic values based on days' quantity 
						try {
							//Custom date interval
							Date[] dates = histogram.getSeriesSet().getSeries("Energy").getXDateSeries();	
							//first date
							cal.setTime(dates[0]);
							//land date
							cal2.setTime(dates[dates.length-1]);
							
							long millsec = cal2.getTimeInMillis() - cal.getTimeInMillis();
							int days = (int) (millsec/(1000*24*60*60)); //-> days
							
							if(days<8){										
								//week
								StatisticsDisplayMode[] list = 
									{StatisticsDisplayMode.DIAGRAM, //->week
										StatisticsDisplayMode.DAYS,
										//StatisticsDisplayMode.HOURS
										};
								displayStatisticsCombo.setInput(list);
							}else if(days>7 & days<30){						
								//month
								StatisticsDisplayMode[] list = 
									{StatisticsDisplayMode.DIAGRAM,	//->month
										StatisticsDisplayMode.WEEKS,
										StatisticsDisplayMode.DAYS};
									displayStatisticsCombo.setInput(list);
							}else if(days>29 & days<365){					
								//year
								StatisticsDisplayMode[] list = 
									{StatisticsDisplayMode.DIAGRAM,	//->year
										StatisticsDisplayMode.MONTHS,
										StatisticsDisplayMode.WEEKS};
								displayStatisticsCombo.setInput(list);
							}else if(days>364 & days<730){ 					
								//years
								StatisticsDisplayMode[] list = 
									{StatisticsDisplayMode.DIAGRAM,	//->years
										StatisticsDisplayMode.YEARS,
										StatisticsDisplayMode.MONTHS};
								displayStatisticsCombo.setInput(list);
							}else if(days>730){ 							
								//many years
								StatisticsDisplayMode[] list = 
									{StatisticsDisplayMode.DIAGRAM,	//->many years
										StatisticsDisplayMode.YEARS};
								displayStatisticsCombo.setInput(list);
							}
						} catch (NullPointerException e1) {	}
							break;
					}case PROJECT:{
							StatisticsDisplayMode[] list = 
								{StatisticsDisplayMode.DIAGRAM,
									StatisticsDisplayMode.YEARS,
									StatisticsDisplayMode.MONTHS};
							displayStatisticsCombo.setInput(list);
							break;
						}
					}
			
					
					displayStatisticsCombo.getCombo().select(0);
					currentStatisticsChartSelection = displayStatisticsCombo.getCombo().getItem(0);
					
					//check if there is previous & next date for new Date
					checkDates();
				
				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		//DISPLAY STATISTICS COMBO SELECTION LISTENER
		//display statistics's mode
		displayStatisticsCombo.getCombo().addSelectionListener(new SelectionListener(){

			
			@Override
			public void widgetSelected(SelectionEvent e) {
				histogramSelection = (StructuredSelection) displayHistogramCombo.getSelection();
				statisticsChartSelection= (StructuredSelection) displayStatisticsCombo.getSelection();
				boolean needsToSpecifyTime = false;
				Date firstDate = projectResources.getCurrentDbRecords().get(0).getDate(),
				lastDate = projectResources.getCurrentDbRecords().get(projectResources.getCurrentDbRecords().size()-1).getDate();
			
				if(histogramSelection.getFirstElement().equals(HistogramDisplayMode.PROJECT) || 
						histogramSelection.getFirstElement().equals(HistogramDisplayMode.CUSTOM) || 
						statisticsChartSelection.getFirstElement().equals(StatisticsDisplayMode.WEEKS))
					//Some of date interval needs to be determine in time. 
					//Example: Week in Year, days for months, years in project.
					needsToSpecifyTime = true;
				if(statisticsChartSelection.getFirstElement().equals(StatisticsDisplayMode.YEARS))
					needsToSpecifyTime = false;
				
				//protection -> do not execute function if previous user selection is the same selection 
				if(!currentStatisticsChartSelection.equals(statisticsChartSelection.getFirstElement().toString())){
					if(!statisticsChartSelection.getFirstElement().equals(StatisticsDisplayMode.DIAGRAM)){
						Querty.loadStatisticsValues(firstDate,lastDate,statisticsChartSelection,needsToSpecifyTime);
						cleanChart(false);
						try {
							createStatisticsChart();
						} catch (Exception e2) {
							message = new MessageBox(DBRecordsStatisticsView.this.getSite().getShell(),SWT.OK);
							message.setText("Cannot to generated statistics");
							message.setMessage("There are problems to get records");
							message.open();
						} 
					}else{
						Querty.loadStatisticsValues(firstDate,lastDate);
						createHistogram(firstDate, lastDate);
					}
					currentStatisticsChartSelection = statisticsChartSelection.getFirstElement().toString();
				}

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		//PREVIOUS DATE BUTTON SELECTION LISTENER
		//change display date into checked previous date & load date's values
		previousDateButton.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				Date firstDate = null,lastDate = null;
				
				histogramSelection = (StructuredSelection) displayHistogramCombo.getSelection();		
				switch((HistogramDisplayMode)histogramSelection.getFirstElement()){
					case DAY:{
						firstDate = projectResources.getHistogramPrevDay();
						lastDate = firstDate;
						break;
					}case MONTH:{
						cal.setTime(projectResources.getHistogramPrevDay());
						firstDate = cal.getTime();
						cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
						lastDate = cal.getTime();
						break;
					}case YEAR:{
						cal.setTime(projectResources.getHistogramPrevDay());
						firstDate = cal.getTime();
						cal.set(Calendar.MONTH,11);
						cal.set(Calendar.DAY_OF_MONTH,31);
						lastDate = cal.getTime();
						break;
					}
				default:
					break;
				}
				
				
				firstDate = convertDate(firstDate, 1);
				lastDate = convertDate(firstDate, -1);
				
				
				cleanChart(true);
				statisticsChartSelection = (StructuredSelection) displayStatisticsCombo.getSelection();
				if(statisticsChartSelection.getFirstElement().equals(StatisticsDisplayMode.DIAGRAM)){
					loadValues(firstDate, lastDate);
					displayStatisticsCombo.getCombo().select(0);
				}
				else{
					
					boolean needsToSpecifyTime = false; 
					if(histogramSelection.getFirstElement().equals(HistogramDisplayMode.PROJECT) || 
							histogramSelection.getFirstElement().equals(HistogramDisplayMode.CUSTOM) || 
							statisticsChartSelection.getFirstElement().equals(StatisticsDisplayMode.WEEKS))
						//Some of date interval needs to be determine in time. 
						//Example: Week in Year, days for months, years in project.
						needsToSpecifyTime = true;
					if(statisticsChartSelection.getFirstElement().equals(StatisticsDisplayMode.YEARS))
						needsToSpecifyTime = false;
					Querty.loadRecords(firstDate,lastDate);
					Querty.loadStatisticsValues(firstDate, lastDate,(StructuredSelection) displayStatisticsCombo.getSelection(),needsToSpecifyTime);
					createStatisticsChart();
					displayStatisticsCombo.setSelection(displayStatisticsCombo.getSelection());
				}
				
				checkDates();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				
				
			}
			
		});
		
		//NEXT DATE BUTTON SELECTION LISTENER
		//change display date into checked next date & load date's values
		nextDateButton.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				Date firstDate = null,lastDate = null;
				
				histogramSelection = (StructuredSelection) displayHistogramCombo.getSelection();		
				switch((HistogramDisplayMode)histogramSelection.getFirstElement()){
					case DAY:{
						firstDate = projectResources.getHistogramNextDate();
						lastDate = firstDate;
						break;
					}case MONTH:{
						cal.setTime(projectResources.getHistogramNextDate());
						firstDate = cal.getTime();
						cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
						lastDate = cal.getTime();
						break;
					}case YEAR:{
						cal.setTime(projectResources.getHistogramNextDate());
						firstDate = cal.getTime();
						cal.set(Calendar.MONTH,11);
						cal.set(Calendar.DAY_OF_MONTH,31);
						lastDate = cal.getTime();
						break;
					}
				default:
					break;
				}
				
				
				firstDate = convertDate(firstDate, 1);
				lastDate = convertDate(lastDate, -1);
				
				cleanChart(true);
				statisticsChartSelection = (StructuredSelection) displayStatisticsCombo.getSelection();
				if(statisticsChartSelection.getFirstElement().equals(StatisticsDisplayMode.DIAGRAM)){
					loadValues(firstDate, lastDate);
					displayStatisticsCombo.getCombo().select(0);
				}
				else{
					boolean needsToSpecifyTime = false;
					if(histogramSelection.getFirstElement().equals(HistogramDisplayMode.PROJECT) || 
							histogramSelection.getFirstElement().equals(HistogramDisplayMode.CUSTOM) || 
							statisticsChartSelection.getFirstElement().equals(StatisticsDisplayMode.WEEKS))
						//Some of date interval needs to be determine in time. 
						//Example: Week in Year, days for months, years in project.
						needsToSpecifyTime = true;
					if(statisticsChartSelection.getFirstElement().equals(StatisticsDisplayMode.YEARS))
						needsToSpecifyTime = false;
					
					Querty.loadRecords(firstDate,lastDate);
					Querty.loadStatisticsValues(firstDate, lastDate,(StructuredSelection) displayStatisticsCombo.getSelection(),needsToSpecifyTime);
					createStatisticsChart();
					displayStatisticsCombo.setSelection(displayStatisticsCombo.getSelection());;
				}
				
				checkDates();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
	}
	
	/**
	 * Gets current database's projects
	 */
	public static void loadProjectsNames(){
		projectsNames = Querty.loadProjectsList();
		
	}
	
	/**
	 * Loads data from database and create histogram.
	 * @param firstDate of date interval
	 * @param lastDate of date interval
	 */
	private void loadValues(Date firstDate, Date lastDate){
		
		//project information
		if(projectResources.isNewUnusedProject()){
			//Name
			projectResources.setProjectName(projectsListCombo.getCombo().
					getItem(projectsListCombo.getCombo().
					getSelectionIndex()));
			
			//Quantity
			Querty.countProjectRecords();
			
			//first date of Project
			Querty.loadProjectDate(0);
			firstDate = projectResources.getProjectFirstDate();
			
			//last date of Project
			Querty.loadProjectDate(-1);
			lastDate = projectResources.getProjectLastDate();
			
			//Units
			Querty.loadUnitRecord();
			
			//load data records from DB using dates loaded from projectResources
			Querty.loadRecords(firstDate,lastDate);
			//loading min,max&mean values from database using dates loaded from projectResources
			Querty.loadStatisticsValues(firstDate,lastDate);
			
			projectResources.setNewUnusedProject(false);
		}else{
			//load data records from DB
			Querty.loadRecords(firstDate,lastDate);
			//loading min,max&mean values from database
			Querty.loadStatisticsValues(firstDate,lastDate);
		}

		try {
			if(createHistogram(firstDate,lastDate)){
				message = new MessageBox(this.getSite().getShell(),SWT.OK);
				message.setText("Warning:");
				message.setMessage("Not enough data record for this date interval");
				message.open();
			}
			
		} catch(IllegalArgumentException e1){
			message = new MessageBox(this.getSite().getShell(),SWT.OK);
			message.setText("Could not create histogram ");
			message.setMessage("IllegalArgumentException\nRestart application");
			message.open();
		}catch (Exception e) {
			message = new MessageBox(this.getSite().getShell(),SWT.OK);
			message.setText("Could not create histogram ");
			message.setMessage("There are problems to get records");
			message.open();
		} 
			
	}
		
	/**
	 * Creates histogram based on current database's records.
	 * Records' power value in in Watt, but for purpose of display is in kW.
	 * Series -> Lines
	 * @param firstDate of date interval
	 * @param lastDate of date interval
	 * @return 
	 */
	private boolean createHistogram(Date firstDate,Date lastDate) {
		
		SeriesSet seriesSet = (SeriesSet) histogram.getSeriesSet();
		histogram.getAxisSet().getXAxis(0).enableCategory(false);
		histogram.getLegend().setVisible(true);
		
		int number=0;
		
		//=============================
		//Arrays to store values
		ArrayList<ProjectRecord> records = projectResources.getCurrentDbRecords();
		Date[] dateTime = new Date[records.size()];
		double[] powerValue = new double[records.size()],
				// tables below to write constant line for statistics
				min = new double[records.size()],
				max = new double[records.size()],
				mean = new double[records.size()];
		//powerValues in kW -> "/1000"
		double minValue = projectResources.getMin()/1000,
				maxValue = projectResources.getMax()/1000,
				meanValue = projectResources.getMean()/1000;
		
		for(ProjectRecord r: records){
			dateTime[number] = r.getDate();
			powerValue[number] = r.getPowerValue()/1000;
			min[number] = minValue;
			max[number] = maxValue;
			mean[number] = meanValue;
			number++;
		}
		
		//-------------------------------------------
		//PROTECTION ->checking if dateValues (from db records) are appropriate to date interval 
		
		boolean changedFirstDate = false,changedLastDate = false;
		if(!displayHistogramCombo.getSelection().equals(HistogramDisplayMode.PROJECT)){
			
			//catching Exception
			int lastIndex = dateTime.length-1;
			
			int dateValue, dateRecordValue;
			int state = 0; //setting condition to compare date from record to date from UI
			
			histogramSelection = (StructuredSelection) displayHistogramCombo.getSelection();
			switch((HistogramDisplayMode)histogramSelection.getFirstElement()){
				case DAY:{
					state = Calendar.HOUR_OF_DAY;
					break;
				}case MONTH:{
					state = Calendar.DAY_OF_MONTH;
					break;
				}case YEAR:{
					state = Calendar.MONTH;
					break;
				}case CUSTOM:{
					state = Calendar.DAY_OF_MONTH;
					break;
				}default:{
					
					break;
				}
			}
			
			cal.setTime(firstDate);
			dateValue = cal.get(state);
			cal2.setTime(dateTime[0]);
			dateRecordValue = cal2.get(state);
			
			if(dateValue != dateRecordValue){
				changedFirstDate = true;
			}
			
			if(displayHistogramCombo.getSelection().equals(HistogramDisplayMode.YEAR)){
				dateValue = 11;
			}else{
				cal.setTime(lastDate);
				dateValue = cal.get(state);
			}
			
			cal2.setTime(dateTime[lastIndex]);
			dateRecordValue = cal2.get(state);
			
			if(dateValue != dateRecordValue){
				changedLastDate = true;
			}
			
		}
		//---------------------------------------
		
		
		//===============================
		//Create visualization for chart
		ILineSeries energySeries =  (ILineSeries) seriesSet.createSeries(SeriesType.LINE, "Energy");
		energySeries.setYSeries(powerValue);
		energySeries.setXDateSeries(dateTime);
		energySeries.setLineColor(DBRecordsStatisticsView.this.getSite().getShell().getDisplay().getSystemColor(SWT.COLOR_RED));
		energySeries.setAntialias(SWT.ON);
		energySeries.setSymbolType(PlotSymbolType.NONE);
		energySeries.enableStep(true);
		
		
		//===============================		
		//setting pattern to display xDateSeries & XAxis TITLE
		
		histogramSelection = (StructuredSelection) displayHistogramCombo.getSelection();
		
		SimpleDateFormat sdf;
		String pattern = null,title = null,title2 = null;
		
		String displayAddnotation = "";
		switch((HistogramDisplayMode)histogramSelection.getFirstElement()) {
			case MONTH:{
				sdf = new SimpleDateFormat("MM");
				displayAddnotation = MonthName.valueOf(Integer.parseInt(sdf.format(firstDate))-1);
				break;
			}case YEAR:{
				sdf = new SimpleDateFormat("yyyy");
				displayAddnotation = "YEAR "+sdf.format(firstDate);
				break;
			}case PROJECT:{
				displayAddnotation = "PROJECT";
				break;
			}default:{
				
				break;
			}			
		}
		
		sdf = new SimpleDateFormat("dd.MM.yyyy");
		if(histogramSelection.getFirstElement().equals(HistogramDisplayMode.DAY)){
			title = sdf.format(dateTime[0]);
			histogram.getAxisSet().getXAxis(0).getTitle().setText(title);
			
			pattern = "HH:mm";
		}else {
			title = sdf.format(dateTime[0]);
			title2 = sdf.format(dateTime[dateTime.length-1]);
			histogram.getAxisSet().getXAxis(0).getTitle().setText(displayAddnotation+" ("+title+" - "+title2+")");
			
			pattern = "dd.MM.yyyy";
		}
		
		
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		histogram.getAxisSet().getXAxis(0).getTick().setFormat(format);

		
		//===================================
		//Settings Statistics
		
		//min
		ILineSeries minSeries =  (ILineSeries) histogram.getSeriesSet().createSeries(SeriesType.LINE, "Min");
		minSeries.setYSeries(min);
		minSeries.setLineColor(Display.getDefault().getSystemColor(SWT.COLOR_BLUE));
		minSeries.setLineWidth(2);
		minSeries.setAntialias(SWT.ON);
		minSeries.setSymbolType(PlotSymbolType.NONE);
		minSeries.setXDateSeries(dateTime);
						

		//max
		ILineSeries maxSeries = (ILineSeries) histogram.getSeriesSet().createSeries(SeriesType.LINE, "Max");
		maxSeries.setYSeries(max);
		maxSeries.setLineColor(Display.getDefault().getSystemColor(SWT.COLOR_DARK_MAGENTA));
		maxSeries.setLineWidth(2);
		maxSeries.setAntialias(SWT.ON);
		maxSeries.setSymbolType(PlotSymbolType.NONE);
		maxSeries.setXDateSeries(dateTime);
						
		//mean
		ILineSeries meanSeries = (ILineSeries) histogram.getSeriesSet().createSeries(SeriesType.LINE, "Mean");
		meanSeries.setYSeries(mean);        
		meanSeries.setLineColor(Display.getDefault().getSystemColor(SWT.COLOR_DARK_GREEN));
		meanSeries.setLineWidth(2);
		meanSeries.setAntialias(SWT.ON);
		meanSeries.setSymbolType(PlotSymbolType.NONE);
		meanSeries.setXDateSeries(dateTime);	
		
		//===============================
		//setting vaLues to chart
		histogram.getAxisSet().adjustRange();
		energySeries.enableArea(true);
		cal.setTime(dateTime[0]);
		histogram.redraw();
		histogram.updateLayout();
		
		return changedFirstDate || changedLastDate;

	}
	
	
	/**
	 * Creates Statistics Chart based on statisticsChartSelection.
	 * Series -> Bar
	 */
	private void createStatisticsChart(){

		int i;
		String[] dateValue = projectResources.statisticsDateValuesKeys();
		
		double[] min = new double[dateValue.length],
				max = new double[dateValue.length],
				mean = new double[dateValue.length];
				
		//getting values to tables
		for(i=0;i<dateValue.length;i++){
			double[] statistics = projectResources.statisticsValues(dateValue[i]);
			//Values to display in kW -> "/1000"
			min[i] = statistics[0]/1000;
			max[i] = statistics[1]/1000;
			mean[i] = statistics[2]/1000;
		} 

		
		
		//===============================		
		//setting pattern to display
		
		statisticsChartSelection = (StructuredSelection) displayStatisticsCombo.getSelection();
		if(statisticsChartSelection.getFirstElement().equals(StatisticsDisplayMode.HOURS)){
			for(i=0;i<dateValue.length;i++){
				dateValue[i]+=":00";
			}
		}
		
		histogram.getLegend().setVisible(true);
		histogram.getAxisSet().getXAxis(0).setCategorySeries(dateValue);
		histogram.getAxisSet().getXAxis(0).enableCategory(true);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		String title = sdf.format(projectResources.getCurrentDbRecords().get(0).getDate());
		if(histogramSelection.getFirstElement().equals(HistogramDisplayMode.DAY)){ 
			histogram.getAxisSet().getXAxis(0).getTitle().setText(title);
		}else {
			String title2 = sdf.format(projectResources.getCurrentDbRecords().get(projectResources.getCurrentDbRecords().size()-1).getDate());
			histogram.getAxisSet().getXAxis(0).getTitle().setText(" ("+title+" - "+title2+")");
		}
		boolean stack = true;
		if(stack){
			for(i = 0; i<max.length; i++){
				max[i] = max[i] - mean[i];
				mean[i] = mean[i] - min[i];
			}
		}
		
		//min
		IBarSeries minSeries = (IBarSeries) histogram.getSeriesSet().createSeries(SeriesType.BAR, "Min");
		minSeries.setYSeries(min);
		minSeries.setBarColor(Display.getDefault().getSystemColor(SWT.COLOR_BLUE));
		minSeries.enableStack(stack);
		
		//mean
		IBarSeries meanSeries = (IBarSeries) histogram.getSeriesSet().createSeries(SeriesType.BAR, "Mean");
		meanSeries.setYSeries(mean);        
		meanSeries.setBarColor(Display.getDefault().getSystemColor(SWT.COLOR_DARK_GREEN));
		meanSeries.enableStack(stack);

		//max
		IBarSeries maxSeries = (IBarSeries) histogram.getSeriesSet().createSeries(SeriesType.BAR, "Max");
		maxSeries.setYSeries(max);
		maxSeries.setBarColor(Display.getDefault().getSystemColor(SWT.COLOR_DARK_RED));
		maxSeries.enableStack(stack);
		
		histogram.getAxisSet().adjustRange();
		histogram.redraw();
		histogram.updateLayout();
	}
	
	/**
	 * Deletes needed diagrams
	 * @param disposeAllSeries -> all diagrams
	 * 
	 */
	private void cleanChart(boolean disposeAllSeries){

		
		histogram.getLegend().setVisible(false);
		try {
			histogram.getSeriesSet().deleteSeries("Energy");
			
		} catch (IllegalArgumentException e) {
			
		}
		
		if(disposeAllSeries){

			try {
				histogram.getSeriesSet().deleteSeries("Min");
			} catch (IllegalArgumentException e) {
				
			}
			
			try {
				histogram.getSeriesSet().deleteSeries("Max");
			} catch (IllegalArgumentException e) {
				
			}
			
			try {
				histogram.getSeriesSet().deleteSeries("Mean");
			} catch (IllegalArgumentException e) {
				
			}

		}
		
	}
	
	
	/**
	 * Modifies widgets when is new project is loaded from database or projects' names are refreshed.
	 * @param loadProjectNamesFromDB -> refresh projectListCombo
	 */
	private void cleanAllFields(boolean loadProjectNamesFromDB){
		
		if(loadProjectNamesFromDB){
			projectsListCombo.getCombo().removeAll();	
		}
		
		
		StatisticsDisplayMode[] list = 
			{StatisticsDisplayMode.DIAGRAM,
				StatisticsDisplayMode.YEARS,
				StatisticsDisplayMode.MONTHS};
		displayStatisticsCombo.setInput(list);
		
		projectInfoButton.setEnabled(false);
		displayHistogramCombo.getCombo().setEnabled(false);
		displayStatisticsCombo.getCombo().setEnabled(false);
		previousDateButton.setEnabled(false);
		nextDateButton.setEnabled(false);
		
		projectResources.clean(true);
		displayHistogramCombo.getCombo().select(0);
		displayStatisticsCombo.getCombo().select(0);
		
		currentHistogramSelection = "PROJECT";
		currentStatisticsChartSelection = "DIAGRAM";
	
	}
	
	/**
	 * Converts date based on selected mode.
	 * @param date
	 * @param mode : 1 - first second of day, -1 - last second of day
	 * @return converted date
	 */
	private Date convertDate(Date date,int mode){
		cal.setTime(date);
		
		histogramSelection = (StructuredSelection) displayHistogramCombo.getSelection();
		
		switch((HistogramDisplayMode)histogramSelection.getFirstElement()){
			case YEAR:{
				if(mode ==1){
					cal.set(Calendar.MONTH, 0);
					cal.set(Calendar.DAY_OF_MONTH, 1);
				}else if(mode == -1){
					cal.set(Calendar.MONTH, 11);
					cal.set(Calendar.DAY_OF_MONTH, 31);
				}
				break;
			}case MONTH:{
				if(mode ==1){
					cal.set(Calendar.DAY_OF_MONTH, 1);
				}else if(mode == -1){
					cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
				}
				break;
			}default:{
				
			}
		}
		
		
		//date mode starting from first sec
		if(mode == 1){
			cal2.set(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH),0,0,0);
			return cal2.getTime();
		//date mode ending till last sec
		}else if(mode == -1){
			cal2.set(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH),23,59,59);
			return cal2.getTime();
		}
		//if is mode is wrong
		else return date;
	}
	
	
	/**
	 * Checks if the date has previous & next date in the DB; buttons' settings
	 */
	private void checkDates(){
		
		Date prevDate = null,nextDate = null;
		histogramSelection = (StructuredSelection) displayHistogramCombo.getSelection();

		switch((HistogramDisplayMode)histogramSelection.getFirstElement()){
			case DAY:{
				
				cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)-1);
				prevDate = cal.getTime();
				
				cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)+2);
				nextDate = cal.getTime();
				
				break;
			}case MONTH:{
				
				cal.set(Calendar.DAY_OF_MONTH, 1);
				cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)-1);
				prevDate = cal.getTime();
				
				
				cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)+2);
				nextDate = cal.getTime();
				
				break;
			}case YEAR:{

				cal.set(Calendar.DAY_OF_MONTH, 1);
				cal.set(Calendar.MONTH,0);
				cal.set(Calendar.YEAR, cal.get(Calendar.YEAR)-1);
				prevDate = cal.getTime();
				cal.set(Calendar.YEAR, cal.get(Calendar.YEAR)+2);
				nextDate = cal.getTime();
				
				break;
			}
		default:
			break;
		}
		
		//buttons' settings
		if(displayHistogramCombo.getCombo().getItem(displayHistogramCombo.getCombo().getSelectionIndex()).equals("PROJECT") 
				|| displayHistogramCombo.getCombo().getItem(displayHistogramCombo.getCombo().getSelectionIndex()).equals("CUSTOM")){
			previousDateButton.setEnabled(false);
			nextDateButton.setEnabled(false);
		}else{
			
			
			//PREVOUS DATE
				//first date of the Date interval
				cal.setTime(convertDate(prevDate, 1));
				//last date of the Date interval
				cal2.setTime(convertDate(prevDate, -1));
				
				if(displayHistogramCombo.getCombo().getItem(displayHistogramCombo.getCombo().getSelectionIndex()).equals("MONTH")){
					cal2.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
				}else if(displayHistogramCombo.getCombo().getItem(displayHistogramCombo.getCombo().getSelectionIndex()).equals("YEAR")){
					
					cal2.set(Calendar.MONTH,11);
					cal2.set(Calendar.DAY_OF_MONTH,31);
				}
				
				//check in DB
				if(Querty.checkDateRecords(cal.getTime(), cal2.getTime())){
					previousDateButton.setEnabled(true);
					projectResources.setHistogramPrevDay(prevDate);
				}else
					previousDateButton.setEnabled(false);
			
			//NEXT DATE
				//firstDate
				cal.setTime(convertDate(nextDate, 1));
				//lastDate
				cal2.setTime(convertDate(nextDate, -1));
				
				if(displayHistogramCombo.getCombo().getItem(displayHistogramCombo.getCombo().getSelectionIndex()).equals("MONTH")){
					cal2.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
				}else if(displayHistogramCombo.getCombo().getItem(displayHistogramCombo.getCombo().getSelectionIndex()).equals("YEAR")){
					
					cal2.set(Calendar.MONTH,11);
					cal2.set(Calendar.DAY_OF_MONTH,31);
				}
				
				//check in DB
				if(Querty.checkDateRecords(cal.getTime(), cal2.getTime())){
					nextDateButton.setEnabled(true);
					projectResources.setHistogramNextDate(nextDate);
				}else
					nextDateButton.setEnabled(false);
		}
	}
	
	@Override
	public void setFocus() {

	}

}

