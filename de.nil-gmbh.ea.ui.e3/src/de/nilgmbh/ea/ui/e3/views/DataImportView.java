/******************************************************************************
 *
 *							Copyright (c) 2015
 *					NIL Nachhaltige Industrielösungen GmbH
 *					 Schulstraße 3, 38486 Klötze, Germany
 *							info(at)nil-gmbh.de 
 *
 *		The contents  of  this  file is an unpublished work  protected
 *		under the copyright law of the  Federal  Republic  of  Germany
 *
 *		This software  is  proprietary to  and  emboddies confidential
 *		technology of NIL Nachhaltige Industrielösungen GmbH. The use, 
 *		possession and copying of the software and media is authorized 
 *      only pursuant to a  valid written license from NIL Nachhaltige
 *		Industrielösungen  GmbH.  This  copyright  statement  must  be 
 *		visibly included in all copies.
 *
 ******************************************************************************/ 

/******************************************************************************
 * 
 *
 * filename : DataImportView.java
 *
 * contents : 
 *
 * created on : 02.10.2015
 * created by : Kiril Aleksandrov
 * 				NIL Nachhaltige Industrielösungen GmbH, Klötze, Germany
 *
 *****************************************************************************/
package de.nilgmbh.ea.ui.e3.views;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Shell;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.DeviceData;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

import com.mongodb.MongoException;
import com.mongodb.MongoSocketException;
import com.mongodb.MongoSocketOpenException;

import data.CsvData;
import database.Insert;
import database.Querty;
import de.nilgmbh.ea.ui.e3.graphicalComponents.FileRecordsDialog;
import exceptions.DateException;
import exceptions.Exceptions;
import exceptions.ValueException;


/**
 * First part of application.
 * CSVFileReader. Reads line by line, getting correct values and creates records (into database).
 * @author Panda
 *
 */
public class DataImportView extends ViewPart {

	public static final String ID = DataImportView.class.getName();

	
	public final static List<String> stepsList = new ArrayList<String>(){
		{
			add("Load CSV File");
			add("Match values to correct fields or 'Skip the line'");
			add("'Try to Parse'");
			add("Correct data values");
			add("'Convert to Record'");
			add("'Check Database Connection'");
			add("'Insert into Database'");
			add("End your work or load another CSV File");
		}
	};
	public static String message;
	public Text stepText,fileNameText,logText;

	public StyledText fileLineText;
	public Button browseFileButton,skipLineButton,tryToParseButton,rememberChoiceButton,tillEndButton,convertToRecordButton,
		checkParsedLinesButton,DBConnectionButton,insertIntoDatabaseButton,stepButton1,stepButton2,stepButton3,stepButton4,stepButton5;
	public Combo dateCombo,timeCombo,powerValueCombo,unitCombo;
	public Label emptyLine,dateColumnLabel,timeColumnLabel,powerColumnLabel,unitColumnLabel,stateOfConnection;

	public MessageBox messageBox,infoBox;
	public int step=0;
	Display dispaly = Display.getCurrent();
	Color white = dispaly.getSystemColor(SWT.COLOR_WHITE);
	
	CsvData csvData;
	public DataImportView() {
		csvData = CsvData.getInstance();
	}

	
	@Override
	public void createPartControl(Composite parent) {
		Composite container = new Composite(parent, SWT.BORDER);
		
		container.setLayout(GridLayoutFactory.fillDefaults().numColumns(5).equalWidth(true).create());
		GridLayout layout = (GridLayout) container.getLayout();
		layout.marginWidth = 10;
		layout.marginHeight = 10;
		
		container.setLayout(layout);
		
		Display dispaly = Display.getCurrent();
		Color white = dispaly.getSystemColor(SWT.COLOR_WHITE);
		Color gray = dispaly.getSystemColor(SWT.COLOR_GRAY);
		container.setBackground(white);
		
		Font lineFont = new Font( dispaly, new FontData( "Monospace", 14, SWT.BOLD ) );
		Font infoFont = new Font( dispaly, new FontData( "Monospace", 10, SWT.BOLD ) );
		
		stepButton1 = new Button(container, SWT.CENTER);
		stepButton1.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		stepButton1.setText("Step 1");
		stepButton2 = new Button(container, SWT.CENTER);
		stepButton2.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		stepButton2.setText("Step 2");
		stepButton3 = new Button(container, SWT.CENTER);
		stepButton3.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		stepButton3.setText("Step 3");
		stepButton4 = new Button(container, SWT.CENTER);
		stepButton4.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		stepButton4.setText("Step 4");
		stepButton5 = new Button(container, SWT.CENTER);
		stepButton5.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		stepButton5.setText("Step 5");
		
		//step text
		stepText = new Text(container, SWT.BORDER|SWT.READ_ONLY|SWT.CENTER);
		stepText.setText(message);
		stepText.setLayoutData(GridDataFactory.fillDefaults().span(5, 5).grab(false, false).create());
		stepText.setFont(infoFont);
		stepText.setBackground(gray);
		
		//----------------------------------------------
		//next line
		emptyLine = new Label(container, SWT.CENTER);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().span(5, 1).grab(true, false).create());
		//----------------------------------------------
		
		//file name
		fileNameText = new Text(container,SWT.CENTER|SWT.BORDER|SWT.READ_ONLY);
		fileNameText.setLayoutData(GridDataFactory.fillDefaults().span(4, 5).grab(true, false).create());
		
		//browse file button
		browseFileButton = new Button(container, SWT.CENTER| SWT.PUSH);
		browseFileButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		browseFileButton.setText("Browse file");
		
		//----------------------------------------------
		//next line
		emptyLine = new Label(container, SWT.CENTER);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().span(5, 1).grab(true, false).create());
		//----------------------------------------------
		
		
		//file line file
		fileLineText = new StyledText(container,SWT.WRAP | SWT.MULTI | SWT.BORDER|SWT.READ_ONLY|SWT.CENTER);
		fileLineText.setLayoutData(GridDataFactory.fillDefaults().span(5, 10).grab(true, false).create());
		fileLineText.setFont(lineFont);
		fileLineText.setBackground(gray);

		
		//next line
		emptyLine = new Label(container, SWT.CENTER);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().span(5, 1).grab(true, false).create());
		//----------------------------------------------
		
		//----------------------------------------------
		//next line
		emptyLine = new Label(container, SWT.CENTER);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().span(1, 3).grab(true, false).create());
		emptyLine.setText("DATE");
		emptyLine = new Label(container, SWT.CENTER);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().span(1, 3).grab(true, false).create());
		emptyLine.setText("TIME");
		emptyLine = new Label(container, SWT.CENTER);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().span(1, 3).grab(true, false).create());
		emptyLine.setText("POWER");
		emptyLine = new Label(container, SWT.CENTER);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().span(1, 3).grab(true, false).create());
		emptyLine.setText("UNIT");
		//skip the line button
		skipLineButton = new Button(container, SWT.CENTER| SWT.PUSH);
		skipLineButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 3).grab(true, false).create());
		skipLineButton.setText("Skip the Line");
		//----------------------------------------------

		//date value combo
		dateCombo = new Combo(container, SWT.CENTER|SWT.BORDER);
		dateCombo.setLayoutData(GridDataFactory.fillDefaults().span(1, 3).grab(true, false).create());
		//time value combo
		timeCombo = new Combo(container, SWT.CENTER|SWT.BORDER);
		timeCombo.setLayoutData(GridDataFactory.fillDefaults().span(1, 3).grab(true, false).create());
		//power value combo
		powerValueCombo = new Combo(container, SWT.CENTER|SWT.BORDER);
		powerValueCombo.setLayoutData(GridDataFactory.fillDefaults().span(1,3).grab(true, false).create());
		//unit value combo
		unitCombo = new Combo(container, SWT.CENTER|SWT.BORDER);
		unitCombo.setLayoutData(GridDataFactory.fillDefaults().span(1, 3).grab(true, false).create());
		//try to parse button
		tryToParseButton = new Button(container, SWT.CENTER| SWT.PUSH);
		tryToParseButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 3).grab(true, false).create());
		tryToParseButton.setText("Try to Parse");
		
		//----------------------------------------------
		//date column index label
		dateColumnLabel  = new Label(container, SWT.CENTER);
		dateColumnLabel.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		//time column index label
		timeColumnLabel  = new Label(container, SWT.CENTER);
		timeColumnLabel.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		//power column index label
		powerColumnLabel  = new Label(container, SWT.CENTER);
		powerColumnLabel.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		//unit column index label
		unitColumnLabel  = new Label(container, SWT.CENTER);
		unitColumnLabel.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		//remember the choice check button
		rememberChoiceButton = new Button(container, SWT.CHECK | SWT.CENTER);
		rememberChoiceButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		rememberChoiceButton.setText("Remember the Choice");
		//----------------------------------------------
		
		//log text
		logText = new Text(container, SWT.CENTER|SWT.BORDER|SWT.READ_ONLY|SWT.MULTI| SWT.V_SCROLL);
		logText.setLayoutData(GridDataFactory.fillDefaults().span(2, 10).grab(true, false).create());
		logText.setBackground(gray);
		//filling the line
		emptyLine = new Label(container, SWT.NONE);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().span(2, 5).grab(true, false).create());
		//do it till the end
		tillEndButton = new Button(container, SWT.CHECK | SWT.CENTER);
		tillEndButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		tillEndButton.setText("Do It Till the End");
		tillEndButton.setToolTipText("Ignores mistakes to the end of the file");
		//convert to record 
		convertToRecordButton = new Button(container, SWT.CENTER| SWT.PUSH);
		convertToRecordButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		convertToRecordButton.setText("Convert to Record");
		
		//----------------------------------------------
		//next line
		emptyLine = new Label(container, SWT.CENTER);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().span(3, 3).grab(true, false).create());
		//----------------------------------------------
		
		//filling the line
		emptyLine = new Label(container, SWT.NONE);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		//check Previous Lines button
		checkParsedLinesButton = new Button(container, SWT.CENTER| SWT.PUSH);
		checkParsedLinesButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		checkParsedLinesButton.setText("Check Parsed Lines");
		//filling the line
		emptyLine = new Label(container, SWT.NONE);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		
		//----------------------------------------------
		//next line
		emptyLine = new Label(container, SWT.CENTER);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().span(5, 1).grab(true, false).create());
		//----------------------------------------------
		
		//Check DB Connection Button
		DBConnectionButton = new Button(container, SWT.CENTER| SWT.PUSH|SWT.WRAP);
		DBConnectionButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		DBConnectionButton.setText("Connect With Database");
		//Connection's state label
		stateOfConnection = new Label(container, SWT.CENTER);
		stateOfConnection.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		//filling the line 
		emptyLine = new Label(container, SWT.NONE);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().span(2, 5).grab(true, false).create());
		//insert Into Database
		insertIntoDatabaseButton = new Button(container, SWT.CENTER| SWT.PUSH);
		insertIntoDatabaseButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 5).grab(true, false).create());
		insertIntoDatabaseButton.setText("Insert into Database");
		
		
		//================================================================
		// BUTTONS SET ENABLED, SET SELECTION
		skipLineButton.setEnabled(false);
		tryToParseButton.setEnabled(false);
		convertToRecordButton.setEnabled(false);
		checkParsedLinesButton.setEnabled(false);
		insertIntoDatabaseButton.setEnabled(false);
		rememberChoiceButton.setEnabled(false);
		tillEndButton.setEnabled(false);
		tillEndButton.setSelection(false);
		rememberChoiceButton.setSelection(false);
		DBConnectionButton.setEnabled(false);

		
		dateCombo.setEnabled(false);
		timeCombo.setEnabled(false);
		powerValueCombo.setEnabled(false);
		unitCombo.setEnabled(false);
		
		stepButton1.setEnabled(true);
		stepButton2.setEnabled(false);
		stepButton3.setEnabled(false);
		stepButton4.setEnabled(false);
		stepButton5.setEnabled(false);
		//================================================================
		//EVENTS
		
		
		
		//BROWSE BUTTON CLICK EVENT
		browseFileButton.addSelectionListener(new SelectionListener(){
			String path = null,currentPath="";
			int lineNumber=0,currentLineNumber = 0;
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				DataImportView.this.processState();
				
				//file dialog
				FileDialog fd = new FileDialog (DataImportView.this.getSite().getShell(),SWT.OPEN);
				fd.setText("Browse File");
				
				//seting variables
				path = null;
				currentLineNumber = csvData.getCurrentLineNumber();
				
				//checking if we browse file
				while(path==null) {
					path = fd.open();
					if (path==null){
						infoBox = new MessageBox(DataImportView.this.getSite().getShell(), SWT.OK);
						infoBox.setMessage("You have not selected any file!");
						infoBox.setText("Browse File");
						infoBox.open();
						path = null;
					}
					else if(path.equals(currentPath)) {
						lineNumber=currentLineNumber;
						csvData.setCurrentLineNumber(lineNumber);
						DataImportView.this.cleanFields(2);
						//DataImportView.this.cleanFields(t);
					}
					else lineNumber = 0;
					
				}
					
				//if there are some converted values 
				if(!csvData.getRecords().isEmpty()){
					messageBox = new MessageBox(DataImportView.this.getSite().getShell(), SWT.ICON_QUESTION | SWT.YES | SWT.NO);
					messageBox.setMessage("Do you really want to load another File?\nAll record will be removed");
					messageBox.setText("Changing File");
					if(!path.equals(currentPath)) {
						int response = messageBox.open();
						if (response == SWT.YES){
							currentLineNumber=0;
						}
						if(response == SWT.NO){
							path = currentPath;
							lineNumber=currentLineNumber;
							csvData.setCurrentLineNumber(lineNumber);
							loadValues(false);
						}
						DataImportView.this.cleanFields(2);
					}
					
				} 
				
				
				DataImportView.this.fileNameText.setText(path);
				//saving information about current file
				currentPath = path;
				currentLineNumber = lineNumber;
				
				//first use of file
				if(currentLineNumber==0)
				{
					csvData.firstUse();
					try {
						csvData.loadDataFromFile(path);
						DataImportView.this.cleanFields(2);
						if(loadValues(true)){
							DataImportView.this.stepText.setText(stepsList.get(1));
						}else DataImportView.this.logText.setText("File is empty");
					} catch (FileNotFoundException e1) {
						//info dialog
					} catch (IOException e1) {
						//info dialog
					}
					
				}else{
					loadValues(false);
				}


			}
				
			

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
			
		});
		
		//SKIP THE LINE BUTTON EVENT
		skipLineButton.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				DataImportView.this.processState();
				//if file has next line
				if(csvData.nextLine()){
					step=2;
					DataImportView.this.stepText.setText(stepsList.get(1));
				}
				else{ //if it's end of line
					step=5;
					DataImportView.this.stepText.setText(stepsList.get(5));
					DataImportView.this.logText.setText("");
				}
				DataImportView.this.cleanFields(step);
				loadValues(false);
				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
			
		});
		
		//DATE VALUE EVENT LISTENER
		dateCombo.addModifyListener(new ModifyListener(){

			@Override
			public void modifyText(ModifyEvent e) {
				int index = dateCombo.getSelectionIndex();
				
				//checking if there is any value to date 
				if(checkValuesFields(1)) {
					//checking if there is assign any file column index
					if(index!=-1){
						DataImportView.this.logText.setText("");
						DataImportView.this.dateColumnLabel.setText("File's column: "+(index+1));
					}
					else {
						DataImportView.this.dateColumnLabel.setText("Custom value");
					}
					
				}
				if(checkValuesFields(0)) {
					DataImportView.this.tryToParseButton.setEnabled(true);
					DataImportView.this.stepText.setText(stepsList.get(2));
					stepButton1.setEnabled(false);
					stepButton2.setEnabled(false);
					stepButton3.setEnabled(true);
					stepButton4.setEnabled(false);
					stepButton5.setEnabled(false);
					
				}
			}
		});
			
		//TIME VALUE EVENT LISTENET
		timeCombo.addModifyListener(new ModifyListener(){

			@Override
			public void modifyText(ModifyEvent e) {
				int index = timeCombo.getSelectionIndex();
				
				//checking if there is any value to time
				if(checkValuesFields(2)) {
					//DataImportView.this.timeValueCombo.setBackground(white);
					//checking if there is assign any file column index
					if(index!=-1){
						DataImportView.this.timeColumnLabel.setText("File's column: "+(index+1));
						DataImportView.this.logText.setText("");
					}
					else {
						DataImportView.this.timeColumnLabel.setText("Custom value");
//						if(rememberChoiceButton.getSelection()) DataImportView.this.logText.setText("WARNING!!!\n Custom value will change on all records!");
					}
					
				}
				if(checkValuesFields(0)) {
					DataImportView.this.tryToParseButton.setEnabled(true);
					DataImportView.this.stepText.setText(stepsList.get(2));
					stepButton1.setEnabled(false);
					stepButton2.setEnabled(false);
					stepButton3.setEnabled(true);
					stepButton4.setEnabled(false);
					stepButton5.setEnabled(false);
				}
			}

			
		});
		
		//POWER VALUE TEXT EVENT LISTENER
		powerValueCombo.addModifyListener(new ModifyListener(){

			@Override
			public void modifyText(ModifyEvent e) {
				int index = powerValueCombo.getSelectionIndex();
				
				//checking if there is any value to power 
				if(checkValuesFields(3)) {
					//DataImportView.this.powerValueCombo.setBackground(white);
					//checking if there is assign any file column index
					if(index!=-1){
						DataImportView.this.powerColumnLabel.setText("File's column: "+(index+1));
						DataImportView.this.logText.setText("");
					}
					else {
						DataImportView.this.powerColumnLabel.setText("Custom value");
//						if(rememberChoiceButton.getSelection()) DataImportView.this.logText.setText("WARNING!!!\n Custom value will change on all records!");
					}
					
					
				}
				if(checkValuesFields(0)) {
					DataImportView.this.tryToParseButton.setEnabled(true);
					DataImportView.this.stepText.setText(stepsList.get(2));
					stepButton1.setEnabled(false);
					stepButton2.setEnabled(false);
					stepButton3.setEnabled(true);
					stepButton4.setEnabled(false);
					stepButton5.setEnabled(false);
				}
			}

			
		});
		
		//DATE VALUE TEXT EVENT LISTENER
		unitCombo.addModifyListener(new ModifyListener(){

			@Override
			public void modifyText(ModifyEvent e) {
				
				
				int index = unitCombo.getSelectionIndex();
				
				//checking if there is any value to unit
				if(checkValuesFields(4)) {
					//DataImportView.this.unitValueCombo.setBackground(white);
					//checking if there is assign any file column index
					if(index!=-1){
						DataImportView.this.unitColumnLabel.setText("File's column: "+(index+1));
						DataImportView.this.logText.setText("");
					}
					else {
						DataImportView.this.unitColumnLabel.setText("Custom value");
//						if(rememberChoiceButton.getSelection()) DataImportView.this.logText.setText("WARNING!!!\n Custom value will change on all records!");
					}
				}
				
				//checking if all values were assign
				if(checkValuesFields(0)) {
					DataImportView.this.tryToParseButton.setEnabled(true);
					DataImportView.this.stepText.setText(stepsList.get(2));
					stepButton1.setEnabled(false);
					stepButton2.setEnabled(false);
					stepButton3.setEnabled(true);
					stepButton4.setEnabled(false);
					stepButton5.setEnabled(false);
				}
				
			}

			
		});
		
		//TRY TO PARSE BUTTON EVENT
		tryToParseButton.addSelectionListener(new SelectionListener(){

			
			@Override
			public void widgetSelected(SelectionEvent e) {
				DataImportView.this.processState();
				//save selected item from combos
				csvData.saveColumnsIndex(dateCombo,timeCombo,powerValueCombo,unitCombo);
				
				infoBox = new MessageBox(DataImportView.this.getSite().getShell(), SWT.OK);
				infoBox.setText("Parse");
				
				//trying to parse values
				if(csvData.tryToParse(dateCombo,timeCombo,powerValueCombo,unitCombo,logText,tillEndButton.getSelection(),
						rememberChoiceButton.getSelection())){
					step = 3;
					infoBox.setMessage("Values are correct");
					if(csvData.recordsSize()==0){
						infoBox.open();
					}
					
					DataImportView.this.stepText.setText(stepsList.get(4));
				}else{
					
					step = 0;
					infoBox.setMessage("Process has failed\nIncorrect data values");
					infoBox.open();
					DataImportView.this.stepText.setText(stepsList.get(3));
					
					
				}
				DataImportView.this.cleanFields(step);
			}

			
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
			
		});

		
		//REMEMBER CHOICE BUTTON CLICK BUTTON
		rememberChoiceButton.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				if(rememberChoiceButton.getSelection()){
					DataImportView.this.tillEndButton.setEnabled(true);
					if(DataImportView.this.dateColumnLabel.getText().equals("Custom value") | 
							DataImportView.this.timeColumnLabel.getText().equals("Custom value") | 
							DataImportView.this.powerColumnLabel.getText().equals("Custom value") | 
							DataImportView.this.unitColumnLabel.getText().equals("Custom value")) {
						DataImportView.this.logText.setText("WARNING!!!\n Custom value will change on all records!");
					}
				}else {
					DataImportView.this.logText.setText("");
					DataImportView.this.tillEndButton.setEnabled(false);
				}
				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			
		});
		
		//CONVERT TO RECORD CLICK EVENT
		convertToRecordButton.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				DataImportView.this.processState();
				DataImportView.this.logText.setText("");
				int countLines=1;
				long recordSize = csvData.recordsSize();
				boolean convertOneTime = true, isHasLine = true, tillTheEnd = false;
				String[] errorStatistic = null;
				
				csvData.loadDataIntoRecords();
				
				step = 2;
				
				while(DataImportView.this.rememberChoiceButton.getSelection()){
					tillTheEnd = DataImportView.this.tillEndButton.getSelection();
					convertOneTime = false;
					isHasLine = csvData.nextLine();
					if (!isHasLine)  DataImportView.this.tillEndButton.setSelection(false);
					if(csvData.tryToParse(dateCombo,timeCombo,powerValueCombo,unitCombo,logText,tillEndButton.getSelection(),
							rememberChoiceButton.getSelection())){
							csvData.loadDataIntoRecords();
							countLines++;
					}else{
						
						if(!tillEndButton.getSelection())	{
							
							DataImportView.this.rememberChoiceButton.setSelection(false);
							DataImportView.this.stepText.setText(stepsList.get(3));
							step = 0;
							errorStatistic = new String[]{"\n\nWhole errors made in this file: "+Exceptions.getCount(),
									"\nDate Exceptions: "+DateException.getCount(),"\nValue Exceptions: "+ValueException.getCount()};
						}
					}
					
				}
				
				DataImportView.this.cleanFields(step);
				if(convertOneTime == true) isHasLine = csvData.nextLine();
				
				
				
				if(!isHasLine){
					DataImportView.this.processState();
					DataImportView.this.dateCombo.select(-1);
					DataImportView.this.stepText.setText(stepsList.get(5));
					DataImportView.this.cleanFields(5);
				}
				loadValues(false);
				infoBox = new MessageBox(DataImportView.this.getSite().getShell(), SWT.OK);
				infoBox.setText("Converting...");
				infoBox.setMessage("Recort was created "+countLines+" times");
				if(recordSize==0){
					infoBox.open();
				}

				
				logText.setText("\nLine: "+csvData.getCurrentLineNumber()+"/"+csvData.fileSize()+"\n=====================");
				DataImportView.this.logText.append("\nRecort was created "+countLines+" times");
				if(tillTheEnd){
					for(String error: errorStatistic){
						DataImportView.this.logText.append(error);
					}
				}
				
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
			
		});
		
		//SEE PARSED LINES BUTTON CLICK EVENT
		checkParsedLinesButton.addSelectionListener(new SelectionListener(){
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				FileRecordsDialog recordDialog = new FileRecordsDialog(DataImportView.this.getSite().getShell());
				
				recordDialog.open();
				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
			
		});
		
		//CHECK DATABASE CONNECTION BUTTON CLICK EVENT
		DBConnectionButton.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				DataImportView.this.processState();
				
				try {
					Insert.connectToDB();
					DataImportView.this.cleanFields(6);
					stateOfConnection.setText("Connected");
				} catch (MongoException mongoEx) {
					stateOfConnection.setText("Not connected");
				} catch (Exception q){
					stateOfConnection.setText("Not connected");
				}
					
						
				DataImportView.this.stepText.setText(stepsList.get(6));		
						
					
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
			
		});
		
		//INSERT INTO DATABASE BUTTON CLICK EVENT
		insertIntoDatabaseButton.addSelectionListener(new SelectionListener(){
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				DataImportView.this.processState();
				
				String[] filePath = DataImportView.this.fileNameText.getText().split("\\\\");
				String fileName = filePath[filePath.length-1];
				String[] dbProjectsNames = Querty.loadProjectsList();
				
				for(String projectName: dbProjectsNames){
					if(fileName.equals(projectName)){
						Querty.deleteDBProject(projectName);
					}
				}
				
				try {
					Insert.loadProjectFileRecordsIntoDatabase(fileName);
					infoBox = new MessageBox(DataImportView.this.getSite().getShell(), SWT.OK);
					infoBox.setText("Insert into Database...");
					infoBox.setMessage("Records are in the database");
					infoBox.open();

					step = 6;
					
					DataImportView.this.stepText.setText(stepsList.get(7));
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					infoBox.setMessage("Could not insert into database");
					infoBox.open();
					step = 5;
				}
				DataImportView.this.cleanFields(step);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {				
			}
			
		});

	}



	/**
	 * Checks available lines and sets values into combos.
	 * @param filefirstUse
	 * @return false - file is empty, true - data are assigned
	 */
	private boolean loadValues(boolean filefirstUse)
	{
		String mark = ";";
		fileLineText.setText("\n");
		String line = csvData.displayLine(csvData.getCurrentLineNumber());
		if(line.startsWith(";")) mark="";
		if(line!=null & !line.startsWith("End")){
			String[] partsOfLine = line.split(mark);
			
			
			dateCombo.setItems(partsOfLine);
			timeCombo.setItems(partsOfLine);
			powerValueCombo.setItems(partsOfLine);
			unitCombo.setItems(partsOfLine);
			
			for(String part: partsOfLine){
				fileLineText.append(part+"\t");
			}
			fileLineText.append("\n");
			//auto - allocation saved indexes of potencial values
			if(filefirstUse==false & csvData.getRecords().size()!=0){
				
				dateCombo.select(csvData.dateIndex);
				timeCombo.select(csvData.timeIndex);
				powerValueCombo.select(csvData.powerIndex);
				unitCombo.select(csvData.unitIndex);
				
				if(csvData.dateIndex==-1){
					dateCombo.setText("");
					dateColumnLabel.setText("Custom Value");
				}
				if(csvData.timeIndex==-1){
					timeCombo.setText("");
					timeColumnLabel.setText("Custom Value");
				}
				if(csvData.powerIndex==-1){
					powerValueCombo.setText("");
					powerColumnLabel.setText("Custom Value");
				}
				if(csvData.unitIndex==-1){
					unitCombo.setText("");
					unitColumnLabel.setText("Custom Value");
				}	
				
			}
			
			return true;
		}
		else {
			fileLineText.setText(line);
			dateCombo.setEnabled(false);
			timeCombo.setEnabled(false);
			powerValueCombo.setEnabled(false);
			unitCombo.setEnabled(false);
			return false;
			
		}
	}
	
	/**
	 * Checks value in current combo
	 * @param numberColumn: 1 - dateValueCombo, 2 - timeValueCombo, 3 - powerValueCombo, 4 - unitValueCombo, 0 - all Combos
	 * @return true - if no values assigned; false - there some value 
	 */
	private boolean checkValuesFields(int numberColumn)
	{
		boolean result=false;
		
		switch(numberColumn){
			case 1: {
				if(!dateCombo.getText().equals("")) result = true;
				else result = false;
				break;
			}
			case 2: {
				if(!timeCombo.getText().equals("")) result = true;
				else result = false;
				break;
			}
			case 3: {
				if(!powerValueCombo.getText().equals("")) result = true;
				else result = false;
				break;
			}
			case 4: {
				if(!unitCombo.getText().equals("")) result = true;
				else result = false;
				break;
			}
			case 0: {
				if(!dateCombo.getText().equals("") & 
						!timeCombo.getText().equals("") & 
						!powerValueCombo.getText().equals("") & 
						!unitCombo.getText().equals("")) result = true;
				else result = false;
				break;
			}
			default: break;
		}
		
		return result;
	}
	
	/**
	 * Sets behavior of widgets on current program step.
	 * @param step
	 */
	private void cleanFields(int step){

		switch(step){
			case 1:{
				logText.setText("");
				fileLineText.setText("");
				dateColumnLabel.setText("");
				timeColumnLabel.setText("");
				powerColumnLabel.setText("");
				unitColumnLabel.setText("");
				
				skipLineButton.setEnabled(true);
				rememberChoiceButton.setSelection(false);
				
				stepButton1.setEnabled(true);
				stepButton2.setEnabled(false);
				stepButton3.setEnabled(false);
				stepButton4.setEnabled(false);
				stepButton5.setEnabled(false);
				break;
			}
			case 2:{
				logText.setText("");
				fileLineText.setText("");
				DataImportView.this.convertToRecordButton.setEnabled(false);
				DataImportView.this.skipLineButton.setEnabled(true);
				DataImportView.this.tryToParseButton.setEnabled(false);
				DataImportView.this.rememberChoiceButton.setEnabled(false);
				
				dateCombo.setBackground(white);
				timeCombo.setBackground(white);
				powerValueCombo.setBackground(white);
				unitCombo.setBackground(white);
				
				stepButton1.setEnabled(false);
				stepButton2.setEnabled(true);
				stepButton3.setEnabled(false);
				stepButton4.setEnabled(false);
				stepButton5.setEnabled(false);
				break;
			}
			case 3:{
				logText.setText("");
				DataImportView.this.convertToRecordButton.setEnabled(true);
				DataImportView.this.skipLineButton.setEnabled(false);
				DataImportView.this.tryToParseButton.setEnabled(false);
				DataImportView.this.rememberChoiceButton.setEnabled(true);
				dateCombo.setBackground(white);
				timeCombo.setBackground(white);
				powerValueCombo.setBackground(white);
				unitCombo.setBackground(white);
				stepButton1.setEnabled(false);
				stepButton2.setEnabled(false);
				stepButton3.setEnabled(false);
				stepButton4.setEnabled(true);
				stepButton5.setEnabled(false);
				break;
			}
			case 4: {
				
				DataImportView.this.skipLineButton.setEnabled(true);
				DataImportView.this.convertToRecordButton.setEnabled(false);
				DataImportView.this.tillEndButton.setEnabled(false);
				
				stepButton1.setEnabled(false);
				stepButton2.setEnabled(true);
				stepButton3.setEnabled(false);
				stepButton4.setEnabled(false);
				stepButton5.setEnabled(false);
				
				dateCombo.setBackground(white);
				timeCombo.setBackground(white);
				powerValueCombo.setBackground(white);
				unitCombo.setBackground(white);
				
				break;
			}
			case 5:{
				DataImportView.this.DBConnectionButton.setEnabled(true);
				DataImportView.this.tillEndButton.setSelection(false);
				DataImportView.this.rememberChoiceButton.setEnabled(false);
				DataImportView.this.rememberChoiceButton.setSelection(false);
				DataImportView.this.skipLineButton.setEnabled(false);
				DataImportView.this.insertIntoDatabaseButton.setEnabled(false);
				
				dateCombo.setEnabled(false);
				timeCombo.setEnabled(false);
				powerValueCombo.setEnabled(false);
				unitCombo.setEnabled(false);
				dateCombo.setText("");
				dateColumnLabel.setText("");
				timeColumnLabel.setText("");
				powerColumnLabel.setText("");
				unitColumnLabel.setText("");
				dateCombo.setBackground(white);
				timeCombo.setBackground(white);
				powerValueCombo.setBackground(white);
				unitCombo.setBackground(white);
				stepButton1.setEnabled(false);
				stepButton2.setEnabled(false);
				stepButton3.setEnabled(false);
				stepButton4.setEnabled(false);
				stepButton5.setEnabled(true);
				
				break;
			}
			case 6: {
				DataImportView.this.insertIntoDatabaseButton.setEnabled(true);
				browseFileButton.setEnabled(true);
				checkParsedLinesButton.setEnabled(true);
				dateCombo.setEnabled(false);
				timeCombo.setEnabled(false);
				powerValueCombo.setEnabled(false);
				unitCombo.setEnabled(false);
				stepButton1.setEnabled(false);
				stepButton2.setEnabled(false);
				stepButton3.setEnabled(false);
				stepButton4.setEnabled(false);
				stepButton5.setEnabled(true);
				break;
			}
			case 0:{
				DataImportView.this.tryToParseButton.setEnabled(true);
				DataImportView.this.skipLineButton.setEnabled(true);
				
				DataImportView.this.rememberChoiceButton.setEnabled(false);
				
				stepButton1.setEnabled(false);
				stepButton2.setEnabled(false);
				stepButton3.setEnabled(true);
				stepButton4.setEnabled(false);
				stepButton5.setEnabled(false);
				break;
			}
		}
		
		if(step!=5 | step!=1 |step!=6) {
			dateCombo.setEnabled(true);
			timeCombo.setEnabled(true);
			powerValueCombo.setEnabled(true);
			unitCombo.setEnabled(true);
			browseFileButton.setEnabled(true);
			stateOfConnection.setText("");
			
		}
		
		if(csvData.getRecords().size()!=0) DataImportView.this.checkParsedLinesButton.setEnabled(true);
		
	}
	
	/**
	 * state when process is invoked
	 */
	private void processState(){
		browseFileButton.setEnabled(false);
		skipLineButton.setEnabled(false);
		dateCombo.setEnabled(false);
		timeCombo.setEnabled(false);
		powerValueCombo.setEnabled(false);
		unitCombo.setEnabled(false);
		tryToParseButton.setEnabled(false);
		tillEndButton.setEnabled(false);
		rememberChoiceButton.setEnabled(false);
		convertToRecordButton.setEnabled(false);
		checkParsedLinesButton.setEnabled(false);
		DBConnectionButton.setEnabled(false);
		insertIntoDatabaseButton.setEnabled(false);

		
		
	}
	
	@Override
	public void setFocus() {

	}

}
