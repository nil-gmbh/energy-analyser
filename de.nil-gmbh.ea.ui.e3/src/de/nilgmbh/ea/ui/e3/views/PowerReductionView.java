package de.nilgmbh.ea.ui.e3.views;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IColorProvider;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IFontProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;
import org.swtchart.Chart;
import org.swtchart.ILineSeries;
import org.swtchart.ILineSeries.PlotSymbolType;
import org.swtchart.ISeries.SeriesType;

import data.ProjectRecord;
import data.ProjectResources;
import data.ReductionChartRecord;
import data.ReductionPeakRecord;
import database.Insert;
import de.nilgmbh.ea.ui.e3.graphicalComponents.BaseDateDialog;
import de.nilgmbh.ea.ui.e3.graphicalComponents.CustomDateDialog;
import de.nilgmbh.ea.ui.e3.graphicalComponents.DayDialog;
import de.nilgmbh.ea.ui.e3.graphicalComponents.MonthDialog;
import de.nilgmbh.ea.ui.e3.graphicalComponents.ReductionPeaksDialog;
import enums.ReductionDisplayMode;
import nilgmbh.ea.ui.e3.Activator;

/**
 * Last part of application. Power Reduction: sets peaks cut line and generate
 * new power consumption. Complex analysis: Threshold analysis, Overlapped
 * charts: original power consumption vs reduction chart, reduction peaks &
 * import into database.
 * 
 * @author Panda
 * @author Kiril Aleksandrov, NIL-GMBH
 * @email info{at}nil-gmbh.de
 * @version 18.02.2016
 */
public class PowerReductionView extends ViewPart {

	public static final String ID = PowerReductionView.class.getName();
	private ProjectResources projectResources;

	private Composite rightComposite;
	private Text recordQuantityText, reductionPeaksQuantityText, totalPowerAmountText, savedPowerAmountText,
			totalCostText, savedCostText, constPeaksSeriesQuantityText, criticalConstPeaksSeriesQuantityText;
	private ComboViewer reductionChartDisplayCombo;
	private Button insertIntoDBButton, generateButton, previousDateButton, nextDateButton, displayNewRecordsButton,
			printChartImageButton;
	private Spinner peaksCutLineSpinner, priceSpinner, constPeaksSpinner;
	private TableViewer reductionRecordsTableViewer;
	private Chart powerReductionChart;

	private StructuredSelection reductionChartComboSelection;
	private String currentReductionComboSelection;

	private Calendar cal = Calendar.getInstance(), cal2 = Calendar.getInstance();
	private MessageBox message;
	private Text projectNameText;
	private Image reduceImage;
	private Image increaseImage;
	private Font newRecordFont;
	private Font oldRecordFont;
	private Color colorReduciton = new Color(display(), 157, 209, 165); // display().getSystemColor(SWT.COLOR_GREEN);
	public Color colorIncrease = new Color(display(), 163, 109, 109); // display().getSystemColor(SWT.COLOR_RED);
	public Color colorNeutral = new Color(display(), 153, 204, 255); // display().getSystemColor(SWT.COLOR_GRAY);

	public PowerReductionView() {
		projectResources = ProjectResources.getInstance();

		reduceImage = Activator.getImageDescriptor("icons/reduction_icon_small.png").createImage();
		increaseImage = Activator.getImageDescriptor("icons/increase_icon_small.png").createImage();
		newRecordFont = new Font(display(), new FontData("Monospace", 8, SWT.BOLD | SWT.ITALIC));
		oldRecordFont = new Font(display(), new FontData("Monospace", 8, SWT.NORMAL));
	}

	public Display display() {
		return Display.getCurrent();
	}

	@Override
	public void createPartControl(Composite parent) {

		Composite top = new Composite(parent, SWT.BORDER);
		top.setLayout(GridLayoutFactory.fillDefaults().numColumns(4).equalWidth(true).margins(10, 10).create());

		createTopControls(top);
		createSashForm(top);

		cleanFields(true);
		printChartImageButton.setEnabled(false);
		peaksCutLineSpinner.setEnabled(false);
		constPeaksSpinner.setEnabled(false);
		priceSpinner.setEnabled(false);

		setUpObserver();

		// EVENTS
		// ======================================

		// PRINT CHART IMAGE BUTTON
		// save chart image as png image file
		printChartImageButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				FileDialog fd = new FileDialog(getSite().getShell(), SWT.SAVE);
				fd.setFilterNames(new String[] { "PNG Files", "All Files (*.*)" });
				fd.setFilterExtensions(new String[] { "*.png", "*.*" });
				fd.setFilterPath("c:\\");
				fd.setFileName("chart.png");

				Image image = new Image(Display.getDefault(), PowerReductionView.this.rightComposite.getBounds().width,
						PowerReductionView.this.rightComposite.getBounds().height);
				ImageLoader loader = new ImageLoader();
				GC gc = new GC(image);
				PowerReductionView.this.rightComposite.print(gc);
				gc.dispose();

				loader.data = new ImageData[] { image.getImageData() };
				String path = fd.open();
				try {
					loader.save(path, SWT.IMAGE_PNG);
				} catch (IllegalArgumentException e1) {

				}

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		// PEAKS CUT LINE SPINNER MODIFY LISTENER
		// check price and peaks cut value to generate reduction chart
		// also generate project chart with peaks cut line
		// sets peakCutLine double value
		peaksCutLineSpinner.addModifyListener(new ModifyListener() {
			int digits = peaksCutLineSpinner.getDigits();

			@Override
			public void modifyText(ModifyEvent e) {
				int cutLineValue = peaksCutLineSpinner.getSelection();
				generateButton.setEnabled(false);
				cleanFields(false);
				if (cutLineValue != 0 & !(cutLineValue < peaksCutLineSpinner.getMinimum())) {
					cleanChart();
					projectResources.setPeaksCutLineValue(cutLineValue);
					createChart(projectResources.getProjectFirstDate(), projectResources.getProjectLastDate(),
							cutLineValue / Math.pow(10, digits));
					if (priceSpinner.getSelection() != 0)
						generateButton.setEnabled(true);

				}
			}
		});

		// PRICE SPINNER MODIFY LISTENER
		// check price and peaks cut value to generate reduction chart
		// sets price
		priceSpinner.addModifyListener(new ModifyListener() {

			int digits = priceSpinner.getDigits();

			@Override
			public void modifyText(ModifyEvent e) {
				Double cutLineValue = (double) peaksCutLineSpinner.getSelection();
				Double priceValue = (double) priceSpinner.getSelection();

				generateButton.setEnabled(false);
				if (cutLineValue != 0 & priceValue != 0) {
					if (priceValue < 100)
						projectResources.setPrice(priceValue / 100);
					else
						projectResources.setPrice(priceValue / Math.pow(10, digits));
					generateButton.setEnabled(true);
				}
			}
		});

		// GENERATE CHART BUTTON SELECTION LISTENER
		// components' settings, cleaning, create reduction chart, sets
		// statistics,etc.
		generateButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				projectResources.setNewConsumptionChart(true);
				projectResources.setConstPeaksNumber(constPeaksSpinner.getSelection());
				boolean praticalReduce = cut();
				cleanChart();
				cleanFields(false);

				if (reductionChartDisplayCombo.getCombo()
						.getItem(reductionChartDisplayCombo.getCombo().getSelectionIndex()).equals("DAY")
						|| reductionChartDisplayCombo.getCombo()
								.getItem(reductionChartDisplayCombo.getCombo().getSelectionIndex()).equals("MONTH")) {
					reductionChartDisplayCombo.getCombo().select(0);
				}
				createReductionChart(null, null);
				loadStatistics();

				// int digits = peaksCutLineSpinner.getDigits();
				// projectResources.setPeaksCutLine((projectResources.getPeaksCutLineValue()/Math.pow(10,digits))*1000);

				displayNewRecordsButton.setSelection(false);
				if (!reductionChartDisplayCombo.getCombo()
						.getItem(reductionChartDisplayCombo.getCombo().getSelectionIndex()).equals("DAY")
						& !reductionChartDisplayCombo.getCombo()
								.getItem(reductionChartDisplayCombo.getCombo().getSelectionIndex()).equals("MONTH")) {
					previousDateButton.setEnabled(false);
					nextDateButton.setEnabled(false);
				}

				reductionChartDisplayCombo.getCombo().setEnabled(true);
				currentReductionComboSelection = reductionChartDisplayCombo.getSelection().toString();

				displayNewRecordsButton.setEnabled(true);
				displayNewRecordsButton.setSelection(false);
				insertIntoDBButton.setEnabled(true);

				reductionRecordsTableViewer.setInput(projectResources.getReductionChartRecords());

				if (!praticalReduce) {
					message = new MessageBox(PowerReductionView.this.getSite().getShell(), SWT.OK);
					message.setText("Reduction");
					message.setMessage("Constant peaks number was exceeded");
					message.open();
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}

		});

		// DISPLAY NEW RECORDS CHECK BUTTON SELECTION LISTENER
		// display all records or if is checked -> NEW
		displayNewRecordsButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				ArrayList<ReductionChartRecord> currentRecords = new ArrayList<ReductionChartRecord>();
				if (displayNewRecordsButton.getSelection()) {

					for (ReductionChartRecord record : projectResources.getCurrentReductionChartRecords()) {
						if (record.getMode().equals("NEW")) {
							currentRecords.add(record);
						}
					}
					reductionRecordsTableViewer.setInput(currentRecords);
				} else {
					reductionRecordsTableViewer.setInput(projectResources.getCurrentReductionChartRecords());
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		// REDUCTION RECORDS TABLE VIEWER IDOUBLE CLICK LISTENER
		// open dialog box with reduction peaks' data
		reductionRecordsTableViewer.addDoubleClickListener(new IDoubleClickListener() {
			ReductionPeaksDialog dialog;

			@Override
			public void doubleClick(DoubleClickEvent event) {

				IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				ReductionChartRecord element = (ReductionChartRecord) selection.getFirstElement();

				if (element.getReductionPeaks() != null) {
					dialog = new ReductionPeaksDialog(PowerReductionView.this.getSite().getShell(), element);
					dialog.open();
				}

			}
		});

		// REDUCTION CHART DISPLAY COMBO SELECTION LISTENER
		// changes perspective of reduction chart
		reductionChartDisplayCombo.getCombo().addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				BaseDateDialog dialog = null;
				reductionChartComboSelection = (StructuredSelection) reductionChartDisplayCombo.getSelection();

				switch ((ReductionDisplayMode) reductionChartComboSelection.getFirstElement()) {
				case MONTH: {
					dialog = new MonthDialog(getSite().getShell());
					break;
				}
				case DAY: {
					dialog = new DayDialog(getSite().getShell());
					break;
				}
				case CUSTOM: {
					dialog = new CustomDateDialog(getSite().getShell());
					break;
				}
				case OVERLAPPED: {
					if (!currentReductionComboSelection
							.equals(reductionChartComboSelection.getFirstElement().toString())) {
						cleanChart();
						createReductionChart(null, null);
						loadStatistics();
						reductionRecordsTableViewer.setInput(projectResources.getReductionChartRecords());
					}
					break;
				}
				case CUTTED_PEAKS: {
					if (!currentReductionComboSelection
							.equals(reductionChartComboSelection.getFirstElement().toString()))
						cleanChart();
					createCuttedPeaksChart();
					break;
				}
				}
				currentReductionComboSelection = reductionChartDisplayCombo.getSelection().toString();

				try {
					if (dialog.open() == Dialog.OK) {
						Date firstDate, lastDate;
						firstDate = dialog.getFirstDate();
						lastDate = dialog.getLastDate();
						cleanChart();
						createReductionChart(firstDate, lastDate);
						loadStatistics();
						checkDates();

						reductionRecordsTableViewer.setInput(projectResources.getCurrentReductionChartRecords());

					}

				} catch (NullPointerException e1) {

				} catch (IllegalArgumentException e2) {
					message = new MessageBox(PowerReductionView.this.getSite().getShell(), SWT.OK);
					message.setText("Cannot create chart");
					message.setMessage("Reduction records date interval is not enough for selected perspective");
					message.open();
				}

				cleanFields(false);
				displayNewRecordsButton.setSelection(false);
				if (!reductionChartDisplayCombo.getCombo()
						.getItem(reductionChartDisplayCombo.getCombo().getSelectionIndex()).equals("DAY")
						& !reductionChartDisplayCombo.getCombo()
								.getItem(reductionChartDisplayCombo.getCombo().getSelectionIndex()).equals("MONTH")) {
					previousDateButton.setEnabled(false);
					nextDateButton.setEnabled(false);
				}
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});

		// PREVIOUS DATE BUTTON SELECTION LISTENER
		// change display date into checked previous date & load date's values
		previousDateButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				Date firstDate = null, lastDate = null;

				reductionChartComboSelection = (StructuredSelection) reductionChartDisplayCombo.getSelection();
				switch ((ReductionDisplayMode) reductionChartComboSelection.getFirstElement()) {
				case DAY: {
					firstDate = projectResources.getReductionChartPrevDate();
					lastDate = firstDate;
					break;
				}
				case MONTH: {
					cal.setTime(projectResources.getReductionChartPrevDate());
					firstDate = cal.getTime();
					cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
					lastDate = cal.getTime();
					break;
				}
				default:
					break;
				}

				firstDate = convertDate(firstDate, 1);
				lastDate = convertDate(firstDate, -1);

				cleanChart();
				if (reductionChartComboSelection.getFirstElement().equals(ReductionDisplayMode.CUTTED_PEAKS)) {
					createCuttedPeaksChart();
				} else {
					createReductionChart(firstDate, lastDate);
					loadStatistics();
					reductionRecordsTableViewer.setInput(projectResources.getCurrentReductionChartRecords());
				}

				checkDates();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}

		});

		// NEXT DATE SELECTION LISTENER
		// change display date into checked next date & load date's values
		nextDateButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				Date firstDate = null, lastDate = null;

				reductionChartComboSelection = (StructuredSelection) reductionChartDisplayCombo.getSelection();
				switch ((ReductionDisplayMode) reductionChartComboSelection.getFirstElement()) {
				case DAY: {
					firstDate = projectResources.getReductionChartNextDate();
					lastDate = firstDate;
					break;
				}
				case MONTH: {
					cal.setTime(projectResources.getReductionChartNextDate());
					firstDate = cal.getTime();
					cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
					lastDate = cal.getTime();
					break;
				}
				default:
					break;
				}

				firstDate = convertDate(firstDate, 1);
				lastDate = convertDate(firstDate, -1);

				cleanChart();
				if (reductionChartComboSelection.getFirstElement().equals(ReductionDisplayMode.CUTTED_PEAKS)) {
					createCuttedPeaksChart();
				} else {
					createReductionChart(firstDate, lastDate);
					loadStatistics();
					reductionRecordsTableViewer.setInput(projectResources.getCurrentReductionChartRecords());
				}

				checkDates();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}

		});

		// INSERT INTO DATABASE BUTTON SELECTION LISTENER
		// load project reduction statistics into database
		insertIntoDBButton.addSelectionListener(new SelectionListener() {
			MessageBox message = new MessageBox(PowerReductionView.this.getSite().getShell());

			@Override
			public void widgetSelected(SelectionEvent e) {

				int digits = peaksCutLineSpinner.getDigits();
				projectResources
						.setPeaksCutLine((projectResources.getPeaksCutLineValue() / Math.pow(10, digits)) * 1000);

				try {
					Insert.connectToDB();
					Insert.loadReductionChartRecordsIntoDatabase();
					message.setText("Insert into Database...");
					message.setMessage("Records are in the database");
					message.open();
				} catch (Exception e1) {
					message.setText("Error");
					message.setMessage("Could not insert into database");
					message.open();
				}

				cleanFields(true);
				cleanChart();

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub

			}
		});

	}

	private void createTopControls(Composite top) {
		// project name text
		projectNameText = new Text(top, SWT.READ_ONLY | SWT.BORDER);
		projectNameText.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());

		// empty line
		Label emptyLine = new Label(top, SWT.CENTER);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).create());

		// insert into database button
		insertIntoDBButton = new Button(top, SWT.PUSH);
		insertIntoDBButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		insertIntoDBButton.setText("Insert Project Into Database");

		// print chart image button
		printChartImageButton = new Button(top, SWT.PUSH);
		printChartImageButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		printChartImageButton.setText("Print");
		printChartImageButton.setToolTipText("Print current chart image");
	}

	private void createSashForm(Composite top) {
		SashForm topSashForm = new SashForm(top, SWT.HORIZONTAL);
		topSashForm.setLayoutData(GridDataFactory.fillDefaults().span(4, 1).grab(true, true).create());
		topSashForm.setSashWidth(5);

		createLeftSideComposite(topSashForm);
		createRightSideComposite(topSashForm);
		topSashForm.setWeights(new int[] { 35, 65 });
	}

	private void createLeftSideComposite(SashForm topSashForm) {
		Composite leftComposite = new Composite(topSashForm, SWT.NONE);
		leftComposite.setLayout(GridLayoutFactory.fillDefaults().numColumns(4).equalWidth(true).create());

		createStatisticsOutputPanel(leftComposite);
		createAnalysisInputControls(leftComposite);
		createTableViewer(leftComposite);
	}

	private void createStatisticsOutputPanel(Composite leftComposite) {
		Composite statisticsPanel = new Composite(leftComposite, SWT.BORDER);
		statisticsPanel
				.setLayout(GridLayoutFactory.fillDefaults().numColumns(4).equalWidth(true).margins(10, 10).create());
		statisticsPanel.setLayoutData(GridDataFactory.fillDefaults().span(4, 2).grab(true, false).create());
		statisticsPanel.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

		// record quantity label
		Label label = new Label(statisticsPanel, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		label.setText("Total count");
		label.setToolTipText("Total number of measurements");

		// current constant peaks series label
		label = new Label(statisticsPanel, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		label.setText("Constant peaks series");

		// power amount label
		label = new Label(statisticsPanel, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		label.setText(" Total Power Amount");

		// total cost label
		label = new Label(statisticsPanel, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		label.setText("Total Cost");

		// record quantity text
		recordQuantityText = new Text(statisticsPanel, SWT.CENTER | SWT.READ_ONLY);
		recordQuantityText.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());

		// constant peaks series quantity text
		constPeaksSeriesQuantityText = new Text(statisticsPanel, SWT.CENTER | SWT.READ_ONLY);
		constPeaksSeriesQuantityText
				.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());

		// total power amount text
		totalPowerAmountText = new Text(statisticsPanel, SWT.CENTER | SWT.READ_ONLY);
		totalPowerAmountText.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());

		// total cost text
		totalCostText = new Text(statisticsPanel, SWT.CENTER | SWT.READ_ONLY);
		totalCostText.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());

		// ---------------------

		// current reduction peaks quantity label
		label = new Label(statisticsPanel, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		label.setText("Total Reduction Peaks");

		// critical constant peaks series quantity label
		label = new Label(statisticsPanel, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		label.setText("Critical peaks series");

		// current power reduction label
		label = new Label(statisticsPanel, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		label.setText("Reduction");

		// saved cost label
		label = new Label(statisticsPanel, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		label.setText("Saved Cost");

		// reduction peaks quantity text
		reductionPeaksQuantityText = new Text(statisticsPanel, SWT.CENTER | SWT.READ_ONLY);
		reductionPeaksQuantityText.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());

		// critical constant peaks series quantity text
		criticalConstPeaksSeriesQuantityText = new Text(statisticsPanel, SWT.CENTER | SWT.READ_ONLY);
		criticalConstPeaksSeriesQuantityText
				.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());

		// saved power amount text
		savedPowerAmountText = new Text(statisticsPanel, SWT.CENTER | SWT.READ_ONLY);
		savedPowerAmountText.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());

		// saved cost text
		savedCostText = new Text(statisticsPanel, SWT.CENTER | SWT.READ_ONLY);
		savedCostText.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
	}

	private void createAnalysisInputControls(Composite leftComposite) {
		Label emptyLine;
		// peaks cut line label
		Label label = new Label(leftComposite, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).create());
		label.setText("Peaks cut line [kW]");

		// constant peaks number label
		label = new Label(leftComposite, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).create());
		label.setText("Constant peaks number");

		// price label
		label = new Label(leftComposite, SWT.CENTER);
		label.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).create());
		label.setText("kW price [�]");

		// empty line
		emptyLine = new Label(leftComposite, SWT.CENTER);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).create());

		// --------------------
		// peaks cut line spinner
		peaksCutLineSpinner = new Spinner(leftComposite, SWT.BORDER);
		peaksCutLineSpinner.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		peaksCutLineSpinner.setDigits(2);
		peaksCutLineSpinner.setIncrement(10);

		// constant peaks number spinner
		constPeaksSpinner = new Spinner(leftComposite, SWT.BORDER);
		constPeaksSpinner.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		constPeaksSpinner.setMaximum(8);
		constPeaksSpinner.setMinimum(0);

		// price spinner
		priceSpinner = new Spinner(leftComposite, SWT.BORDER);
		priceSpinner.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		priceSpinner.setDigits(2);
		priceSpinner.setIncrement(100);
		priceSpinner.setMaximum(10000000);

		// generate chart button
		generateButton = new Button(leftComposite, SWT.PUSH);
		generateButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		generateButton.setText("Calculate");
		generateButton.setToolTipText("Generate Reduction Chart");

		// ---------------------------------------------------
		// // empty line
		// emptyLine = new Label(leftComposite, SWT.BORDER);
		// emptyLine.setLayoutData(GridDataFactory.fillDefaults().span(1,
		// 1).grab(true, false).create());

		// display new records button
		displayNewRecordsButton = new Button(leftComposite, SWT.CHECK | SWT.LEFT);
		displayNewRecordsButton.setLayoutData(
				GridDataFactory.fillDefaults().align(SWT.CENTER, SWT.FILL).span(4, 1).grab(true, false).create());
		displayNewRecordsButton.setText("Display changes only");
	}

	private void createTableViewer(Composite leftComposite) {
		reductionRecordsTableViewer = new TableViewer(leftComposite,
				SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.FULL_SELECTION);
		reductionRecordsTableViewer.getTable().setLinesVisible(false);
		reductionRecordsTableViewer.getTable().setHeaderVisible(true);
		reductionRecordsTableViewer.getTable()
				.setLayoutData(GridDataFactory.fillDefaults().span(4, 1).grab(true, true).create());
		TableViewerColumn iconColumn = new TableViewerColumn(reductionRecordsTableViewer, SWT.CENTER);
		iconColumn.getColumn().setAlignment(SWT.CENTER);
		iconColumn.getColumn().setText("");
		iconColumn.getColumn().setWidth(25);
		TableViewerColumn countColumn = new TableViewerColumn(reductionRecordsTableViewer, SWT.CENTER);
		countColumn.getColumn().setText("No");
		countColumn.getColumn().setWidth(50);
		TableViewerColumn dateColumn = new TableViewerColumn(reductionRecordsTableViewer, SWT.CENTER);
		dateColumn.getColumn().setText("Date");
		dateColumn.getColumn().setWidth(70);
		TableViewerColumn timeColumn = new TableViewerColumn(reductionRecordsTableViewer, SWT.CENTER);
		timeColumn.getColumn().setText("Time");
		timeColumn.getColumn().setWidth(50);
		TableViewerColumn powerColumn = new TableViewerColumn(reductionRecordsTableViewer, SWT.CENTER);
		powerColumn.getColumn().setText("[kW]");
		powerColumn.getColumn().setToolTipText("Power Value [kW]");
		powerColumn.getColumn().setWidth(50);
		TableViewerColumn powerDescriptionColumn = new TableViewerColumn(reductionRecordsTableViewer, SWT.CENTER);
		powerDescriptionColumn.getColumn().setText("Change [kW]");
		powerDescriptionColumn.getColumn().setWidth(70);
		TableViewerColumn priceColumn = new TableViewerColumn(reductionRecordsTableViewer, SWT.CENTER);
		priceColumn.getColumn().setText("Cost");
		priceColumn.getColumn().setWidth(75);
		TableViewerColumn oldLocation = new TableViewerColumn(reductionRecordsTableViewer, SWT.CENTER);
		oldLocation.getColumn().setText("Chain");
		oldLocation.getColumn().setToolTipText("Consecutive plan modifications");
		oldLocation.getColumn().setWidth(30);
		reductionRecordsTableViewer.setContentProvider(new ArrayContentProvider());
		reductionRecordsTableViewer.setLabelProvider(new RecordLabelProvider());
	}

	private void createRightSideComposite(SashForm topSashForm) {
		Label emptyLine;
		rightComposite = new Composite(topSashForm, SWT.BORDER);
		rightComposite
				.setLayout(GridLayoutFactory.fillDefaults().numColumns(4).equalWidth(true).margins(10, 10).create());
		rightComposite.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

		// reduction chart display combo viewer
		reductionChartDisplayCombo = new ComboViewer(rightComposite, SWT.READ_ONLY | SWT.BORDER);
		reductionChartDisplayCombo.setContentProvider(ArrayContentProvider.getInstance());
		reductionChartDisplayCombo.setLabelProvider(new LabelProvider());
		reductionChartDisplayCombo.getCombo()
				.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		reductionChartDisplayCombo.setInput(ReductionDisplayMode.values());

		// empty line
		emptyLine = new Label(rightComposite, SWT.CENTER);
		emptyLine.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).create());

		// previous date button
		previousDateButton = new Button(rightComposite, SWT.PUSH);
		previousDateButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		previousDateButton.setText("Previous Date");

		// next date button
		nextDateButton = new Button(rightComposite, SWT.PUSH);
		nextDateButton.setLayoutData(GridDataFactory.fillDefaults().span(1, 1).grab(true, false).create());
		nextDateButton.setText("Next Date");

		// power reduction chart
		powerReductionChart = new Chart(rightComposite, SWT.NONE);
		powerReductionChart.setLayoutData(GridDataFactory.fillDefaults().grab(true, true).span(4, 1).create());
		powerReductionChart.getLegend().setPosition(SWT.TOP);
		powerReductionChart.getAxisSet().getXAxis(0).getTitle().setText("Days");
		powerReductionChart.getAxisSet().getYAxis(0).getTitle().setText("Power [kW]");
		powerReductionChart.getTitle().setText("");
	}

	private void setUpObserver() {
		// OBSERVER - IMPLEMENTATION & UPDATE
		// =============================================
		Observer powerReducView = new Observer() {

			@Override
			public void update(Observable o, Object arg) {
				projectNameText.setText(projectResources.getProjectName());
				cleanChart();
				cleanFields(true);
				createChart(projectResources.getProjectFirstDate(), projectResources.getProjectLastDate(), null);

				peaksCutLineSpinner.setEnabled(true);
				constPeaksSpinner.setEnabled(true);
				priceSpinner.setEnabled(true);
				// display in spinner -> "*100" & display in kW -> "/1000"
				peaksCutLineSpinner.setMinimum((int) ((projectResources.getMin() / 1000) * 100));
				peaksCutLineSpinner.setMaximum((int) ((projectResources.getMax() / 1000) * 100));
				peaksCutLineSpinner.setSelection(peaksCutLineSpinner.getMinimum());

				printChartImageButton.setEnabled(true);
			}
		};

		ProjectResources.getInstance().addObserver(powerReducView);
	}

	private class RecordLabelProvider extends LabelProvider
			implements ITableLabelProvider, IFontProvider, IColorProvider {

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			if (element instanceof ReductionChartRecord) {
				ReductionChartRecord record = ((ReductionChartRecord) element);
				if (columnIndex == 0) {
					if (record.hadConstansPeak()) {
						return reduceImage;
					} else if (record.reductionPeaksQuantity() > 0) {
						return increaseImage;
					} else
						return null;
				} else
					return null;
			}
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {

			if (element instanceof ReductionChartRecord) {
				ReductionChartRecord record = ((ReductionChartRecord) element);

				switch (columnIndex) {
				case 0:
					return "";
				case 1:
					return String.valueOf(projectResources.getCurrentReductionChartRecords().indexOf(record));
				case 2:
					return record.displaySimpleDate();
				case 3:
					return record.displaySimpleTime();
				case 4:
					return String.valueOf(round(record.getPower() / 1000));
				case 5:
					return String.valueOf(record.getPowerModification() / 1000);
				// if (record.hadConstansPeak()) {
				// return "REDUCTION";
				// } else if (record.reductionPeaksQuantity() > 0) {
				// return "INCREASE";
				// } else
				// return "";
				case 6:
					return String.valueOf(record.getCost());
				case 7: {
					int size;
					try {
						size = record.getReductionPeaks().size();
						return String.valueOf(size);
					} catch (NullPointerException e) {
						return "";
					}

				}

				default:
					return null;
				}
			} else
				return "";

		}

		@Override
		public Font getFont(Object element) {
			//

			if (element instanceof ReductionChartRecord) {
				ReductionChartRecord record = ((ReductionChartRecord) element);
				if (record.getMode().equals("NEW")) {
					return newRecordFont;
				} else
					return oldRecordFont;
			}
			return null;
		}

		@Override
		public Color getForeground(Object element) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Color getBackground(Object element) {
			if (element instanceof ReductionChartRecord) {
				ReductionChartRecord record = ((ReductionChartRecord) element);
				try {
					if (record.hadConstansPeak()) {
						return colorReduciton; //
					} else if (projectResources.getCriticalConstPeaks().contains(record.getDate())) {
						return colorIncrease;
					} else if (record.getMode().equals("NEW") & !record.getReductionPeaks().equals(null)) {
						return colorNeutral;
					}
				} catch (NullPointerException e) {

				}

			}
			return null;

		}
	}

	/**
	 * Generates project chart to set peaks cut line
	 * 
	 * @param firstDate
	 *            of date interval
	 * @param lastDate
	 *            of date interval
	 * @param peaksCutLineValue
	 *            Function based on function from DBRecordsStatisticsView. i ->
	 *            increase
	 */
	private void createChart(Date firstDate, Date lastDate, Double peaksCutLineValue) {

		int i = 0;

		// =============================
		// Arrays to store values
		ArrayList<ProjectRecord> records = projectResources.getProjectRecords();
		Date[] dateTime = new Date[records.size()];
		double[] powerValue = new double[records.size()];
		double[] cutLine = null;

		for (ProjectRecord r : records) {
			dateTime[i] = r.getDate();
			powerValue[i] = r.getPowerValue() / 1000; // kW
			i++;
		}

		// ===============================
		// Create visualization for chart
		ILineSeries series = (ILineSeries) powerReductionChart.getSeriesSet().createSeries(SeriesType.LINE, "Energy");
		series.setYSeries(powerValue);
		series.setXDateSeries(dateTime);
		series.setLineColor(PowerReductionView.this.getSite().getShell().getDisplay().getSystemColor(SWT.COLOR_RED));
		series.setAntialias(SWT.ON);
		series.setSymbolType(PlotSymbolType.NONE);
		series.enableStep(true);
		powerReductionChart.getLegend().setVisible(true);
		powerReductionChart.getSeriesSet().getSeries("Energy").setVisible(true);

		// ===============================
		// setting pattern to display to XAxis

		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		powerReductionChart.getAxisSet().getXAxis(0).getTitle()
				.setText(sdf.format(dateTime[0]) + " - " + sdf.format(dateTime[dateTime.length - 1]));
		powerReductionChart.getAxisSet().getXAxis(0).getTick().setFormat(sdf);

		// ===================================
		// Settings cutLine

		if (peaksCutLineValue != null) {
			cutLine = new double[records.size()];

			for (i = 0; i < cutLine.length; i++) {
				cutLine[i] = peaksCutLineValue;
			}

			ILineSeries cutLineSeries = (ILineSeries) powerReductionChart.getSeriesSet().createSeries(SeriesType.LINE,
					"CutLine");
			cutLineSeries.setYSeries(cutLine);
			cutLineSeries.setLineColor(Display.getDefault().getSystemColor(SWT.COLOR_BLUE));
			cutLineSeries.setLineWidth(2);
			cutLineSeries.setAntialias(SWT.ON);
			cutLineSeries.setSymbolType(PlotSymbolType.NONE);
			cutLineSeries.setXDateSeries(dateTime);
			cutLineSeries.setDescription("Cut Line");

			powerReductionChart.getSeriesSet().bringForward("CutLine");
			powerReductionChart.getSeriesSet().getSeries("CutLine").setVisible(true);
		}

		// ===============================
		// setting vaLues to chart
		powerReductionChart.getAxisSet().adjustRange();
		series.enableArea(true);

		powerReductionChart.redraw();
		powerReductionChart.updateLayout();

		cal.setTime(dateTime[0]);
		cal2.setTime(cal.getTime());
	}

	/**
	 * Generate powerReduction chart based on current date interval.
	 * 
	 * @param firstDate
	 *            of date interval
	 * @param lastDate
	 *            of date interval i -> increase
	 */
	private void createReductionChart(Date firstDate, Date lastDate) {

		int i = 0;
		powerReductionChart.getLegend().setVisible(true);
		// =============================
		// Arrays to store values
		Date[] dateTime;
		double[] cuttedPowerValue;
		double[] projectEnergy;
		double[] maxEnergyValues;
		double[] minEnergyValues;
		
		ArrayList<ProjectRecord> projectRecords = projectResources.getProjectRecords();
		ArrayList<ReductionChartRecord> currentChartRecords = new ArrayList<ReductionChartRecord>();

		reductionChartComboSelection = (StructuredSelection) reductionChartDisplayCombo.getSelection();
		if (reductionChartComboSelection.getFirstElement().equals(ReductionDisplayMode.OVERLAPPED)) {
			currentChartRecords = projectResources.getReductionChartRecords();

			dateTime = new Date[currentChartRecords.size()];
			cuttedPowerValue = new double[currentChartRecords.size()];
			projectEnergy = new double[currentChartRecords.size()];

			for (ReductionChartRecord record : currentChartRecords) {
				dateTime[i] = record.getDate();
				cuttedPowerValue[i] = record.getPower() / 1000;
				i++;
			}
			i = 0;
			for (ProjectRecord record : projectRecords) {
				projectEnergy[i] = record.getPowerValue() / 1000;
				i++;
			}

		} else {
			ArrayList<ReductionChartRecord> records = projectResources.getReductionChartRecords();
			ArrayList<Date> dates = new ArrayList<Date>();
			ArrayList<Double> powerValues = new ArrayList<Double>();

			for (ReductionChartRecord record : records) {
				Date date = record.getDate();
				if ((date.equals(firstDate) || date.after(firstDate))
						& (date.equals(lastDate) || date.before(lastDate))) {
					dates.add(date);
					powerValues.add(record.getPower());
					currentChartRecords.add(record);
				}
			}

			dateTime = dates.toArray(new Date[dates.size()]);
			cuttedPowerValue = new double[dates.size()];
			i = 0;
			for (double value : powerValues) {
				cuttedPowerValue[i] = value / 1000;
				i++;
			}

			powerValues = new ArrayList<Double>();
			for (ProjectRecord record : projectRecords) {
				Date date = record.getDate();
				if ((date.equals(firstDate) || date.after(firstDate))
						& (date.equals(lastDate) || date.before(lastDate))) {
					powerValues.add(record.getPowerValue());
				}
			}

			projectEnergy = new double[dates.size()];
			i = 0;
			for (double value : powerValues) {
				projectEnergy[i] = value / 1000;
				i++;
			}

		}
		
		maxEnergyValues = new double[projectEnergy.length];	
		minEnergyValues = new double[projectEnergy.length];	
		for(int y=0;y<projectEnergy.length;y++){
			maxEnergyValues[y] = projectResources.getMax()/1000;
			minEnergyValues[y] = projectResources.getMin()/1000;
		}

		projectResources.setCurrentReductionChartRecords(currentChartRecords);
		projectResources.loadCurrentReductionPeaksQuantity();
		projectResources.loadCurrentConstPeaksSeriesQuantity();
		projectResources.loadCriticalConstPeaks();
		cal.setTime(dateTime[0]);
		cal2.setTime(cal.getTime());

		// ===============================
		// Create visualization for chart
		ILineSeries series = (ILineSeries) powerReductionChart.getSeriesSet().createSeries(SeriesType.LINE,	"Power Reduction");
		series.setXDateSeries(dateTime);
		series.setYSeries(cuttedPowerValue);
		series.setLineColor(
				PowerReductionView.this.getSite().getShell().getDisplay().getSystemColor(SWT.COLOR_DARK_GREEN));
		series.setAntialias(SWT.ON);
		series.setSymbolType(PlotSymbolType.NONE);
		series.enableStep(true);
		series.enableArea(true);

		ILineSeries projectSeries = (ILineSeries) powerReductionChart.getSeriesSet().createSeries(SeriesType.LINE, "Project");
		projectSeries.setYSeries(projectEnergy);
		projectSeries.setXDateSeries(dateTime);
		projectSeries.setLineColor(
				PowerReductionView.this.getSite().getShell().getDisplay().getSystemColor(SWT.COLOR_DARK_RED));
		projectSeries.setAntialias(SWT.ON);
		projectSeries.setSymbolType(PlotSymbolType.NONE);
		projectSeries.enableStep(true);
		// projectSeries.enableArea(true);
		
		
				
		ILineSeries maxLine = (ILineSeries) powerReductionChart.getSeriesSet().createSeries(SeriesType.LINE, "Old max value "+projectResources.getMax()/1000);
		maxLine.setYSeries(maxEnergyValues);
		maxLine.setXDateSeries(dateTime);
		maxLine.setLineColor(PowerReductionView.this.getSite().getShell().getDisplay().getSystemColor(SWT.COLOR_DARK_RED));
		maxLine.setSymbolType(PlotSymbolType.NONE);
		maxLine.setAntialias(SWT.ON);
		maxLine.enableArea(false);
		
		ILineSeries minLine = (ILineSeries) powerReductionChart.getSeriesSet().createSeries(SeriesType.LINE, "minLine");
		minLine.setYSeries(minEnergyValues);
		minLine.setXDateSeries(dateTime);
		minLine.setVisibleInLegend(false);
		minLine.setLineColor(PowerReductionView.this.getSite().getShell().getDisplay().getSystemColor(SWT.COLOR_DARK_BLUE));
		minLine.setSymbolType(PlotSymbolType.NONE);
		minLine.setAntialias(SWT.ON);
		minLine.enableArea(false);
		
		powerReductionChart.getLegend().setVisible(true);

		// ===============================
		// setting pattern to display to XAxis

		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		String pattern = "";

		if (reductionChartComboSelection.getFirstElement().equals(ReductionDisplayMode.DAY)) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(dateTime[0]);
			powerReductionChart.getAxisSet().getXAxis(0).getTitle().setText(sdf.format(dateTime[0]) + " "
					+ cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.ENGLISH));

			pattern = "HH:mm";
		} else {
			powerReductionChart.getAxisSet().getXAxis(0).getTitle()
					.setText(sdf.format(dateTime[0]) + " - " + sdf.format(dateTime[dateTime.length - 1]));

			pattern = "dd.MM.yyyy";
		}

		SimpleDateFormat format = new SimpleDateFormat(pattern);
		powerReductionChart.getAxisSet().getXAxis(0).getTick().setFormat(format);

		// ===================================
		// ===============================
		// setting vaLues to chart
		powerReductionChart.getAxisSet().adjustRange();

		powerReductionChart.getSeriesSet().bringForward("Power Reduction");
		powerReductionChart.redraw();
		powerReductionChart.updateLayout();
	}

	/**
	 * Generate chart with original cut peaks values.
	 */
	private void createCuttedPeaksChart() {
		ArrayList<ReductionPeakRecord> cuts = projectResources.getProjectPeaks();
		Date[] dateTime = projectResources.projectDates();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		double[] cuttedPowerValue = new double[dateTime.length];
		int number = 0;

		for (int i = 0; i < dateTime.length; i++) {
			try {
				if (dateTime[i].equals(cuts.get(number).getDate())) {
					cuttedPowerValue[i] = cuts.get(number).getReduction() / 1000;
					number++;
				} else
					cuttedPowerValue[i] = 0.0;
			} catch (IndexOutOfBoundsException e) {
				cuttedPowerValue[i] = 0.0;
			}

		}

		ILineSeries series = (ILineSeries) powerReductionChart.getSeriesSet().createSeries(SeriesType.LINE, "CutPeaks");
		series.setYSeries(cuttedPowerValue);
		series.setXDateSeries(dateTime);
		series.setLineColor(
				PowerReductionView.this.getSite().getShell().getDisplay().getSystemColor(SWT.COLOR_DARK_MAGENTA));
		series.setAntialias(SWT.ON);
		series.setSymbolType(PlotSymbolType.NONE);
		series.enableStep(true);
		series.setDescription("Cut Peaks");
		series.enableArea(true);

		powerReductionChart.getSeriesSet().getSeries("CutPeaks").setVisible(true);
		powerReductionChart.getLegend().setVisible(true);

		powerReductionChart.getAxisSet().getXAxis(0).getTitle()
				.setText(sdf.format(dateTime[0]) + " - " + sdf.format(dateTime[dateTime.length - 1]));
		powerReductionChart.getAxisSet().getXAxis(0).getTick().setFormat(sdf);

		powerReductionChart.getAxisSet().adjustRange();
		powerReductionChart.redraw();
		powerReductionChart.updateLayout();
	}

	/**
	 * Creates new power consumption records below peaks cut line to
	 * PowerReduction chart. const peak numb -> 0 - does bring information if
	 * reduce is pratical
	 * 
	 * @return false if constant peaks number was exceeded, true if reduce is
	 *         practical
	 */
	private boolean cut() {
		boolean practicalReduce = true;
		// surplus -> amount of current reduction peaks
		double surplus = 0.0, peaksCutLine;
		int spinnerFraction = (int) Math.pow(10, peaksCutLineSpinner.getDigits());
		int selection = peaksCutLineSpinner.getSelection();
		if (selection < 100)
			peaksCutLine = selection * 10;
		else
			peaksCutLine = (selection / spinnerFraction) * 1000;

		double price = projectResources.getPrice();

		int currentConstPeaksNumb = 0, constPeaksProcessQuantity = 0;
		double[] powerValue = projectResources.projectEnergy();
		Date[] dates = projectResources.projectDates();
		ArrayList<ReductionPeakRecord> peaks = new ArrayList<ReductionPeakRecord>(),
				currentReductionPeaks = new ArrayList<ReductionPeakRecord>();
		ArrayList<ReductionChartRecord> reductionRecords = new ArrayList<ReductionChartRecord>();

		for (int i = 0; i < powerValue.length; i++) {
			double energy = powerValue[i];
			double cutValue = surplus + energy - peaksCutLine;

			if (cutValue > 0) {
				cutValue = round(cutValue);
				double projectPeak = round(cutValue - surplus);
				if (projectPeak > 0.0) {
					peaks.add(new ReductionPeakRecord(dates[i], projectPeak, round((projectPeak / 1000) * price)));
				}

				reductionRecords.add(new ReductionChartRecord(dates[i], peaksCutLine, peaksCutLine - energy,
						round((peaksCutLine / 1000) * price), // price for kW
						"NEW", null, true));

				surplus = cutValue;
				currentReductionPeaks.add(new ReductionPeakRecord(dates[i], surplus, (surplus / 1000) * price));

				constPeaksProcessQuantity++;
			} else {
				if (surplus == 0.0 & currentReductionPeaks.size() == 0) {
					reductionRecords.add(
							new ReductionChartRecord(dates[i], powerValue[i], 0, round((powerValue[i] / 1000) * price), // price
																														// for
																														// kW
									"OLD", null, false));
				} else if (surplus != 0.0 & currentReductionPeaks.size() != 0) {

					if (constPeaksProcessQuantity > constPeaksSpinner.getSelection()
							&& constPeaksSpinner.getSelection() != 0) {
						practicalReduce = false;

					}

					double newPowerValue = round(powerValue[i] + surplus);
					reductionRecords.add(new ReductionChartRecord(dates[i], newPowerValue, newPowerValue - energy,
							round((newPowerValue / 1000) * price), // price for
																	// kW
							"NEW", currentReductionPeaks, false));

					if (dates[i].equals(dates[dates.length - 1])) {
						break;
					} else {
						surplus = 0.0;
						currentReductionPeaks = new ArrayList<ReductionPeakRecord>();
						currentConstPeaksNumb++;
					}
				}
			}
		}
		if (surplus > 0.0) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(dates[dates.length - 1]);
			cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE) + 15);

			reductionRecords
					.add(new ReductionChartRecord(cal.getTime(), surplus, surplus, round((surplus / 1000) * price), // price
																													// for
																													// kW
							"NEW", currentReductionPeaks, false));
		}

		projectResources.setProjectPeaks(peaks);
		projectResources.setReductionChartRecords(reductionRecords);
		projectResources.loadCurrentReductionPeaksQuantity();
		projectResources.setConstPeaksProcessQuantity(currentConstPeaksNumb);

		return practicalReduce;
	}

	/**
	 * Sets current reduction chart statistics.
	 */
	private void loadStatistics() {
		Double amount;
		String unit;

		recordQuantityText.setText("" + projectResources.getCurrentReductionChartRecords().size());

		reductionPeaksQuantityText.setText("" + projectResources.getCurrentReductionPeaksQuantity());

		constPeaksSeriesQuantityText.setText("" + projectResources.getConstPeaksProcessQuantity());

		criticalConstPeaksSeriesQuantityText.setText("" + projectResources.getConstCriticalPeakSeriesQuantity());

		amount = round(projectResources.totalPowerAmount());
		if (amount < 1000000) {
			amount = round(amount / 1000);
			unit = "kW";
		} else {
			amount = round(amount / 1000000);
			unit = "MW";
		}
		totalPowerAmountText.setText(amount + " " + unit);

		savedPowerAmountText.setText(round(projectResources.reduction() / 1000) + " kW");

		totalCostText.setText("" + round(projectResources.totalCost()));

		savedCostText.setText("" + round(projectResources.savedCost()));

	}

	/**
	 * Returns rounded double value with second decimal place.
	 * 
	 * @param value
	 * @return (double) rounded value
	 */
	private double round(double value) {

		value = value * 100;
		value = Math.round(value);
		value = value / 100;

		DecimalFormat df = new DecimalFormat();
		df.applyPattern("##.##");
		df.setMaximumFractionDigits(2);
		String stringValue = df.format(value);
		stringValue = stringValue.replace(",", ".");
		value = Double.parseDouble(stringValue);

		return value;
	}

	/**
	 * Converts date based on selected mode.
	 * 
	 * @param date
	 * @param mode
	 *            : 1 - first second of day, -1 - last second of day
	 * @return converted date Function from DBRecordsStatisticsView
	 */
	private Date convertDate(Date date, int mode) {

		cal.setTime(date);

		reductionChartComboSelection = (StructuredSelection) reductionChartDisplayCombo.getSelection();

		switch ((ReductionDisplayMode) reductionChartComboSelection.getFirstElement()) {
		case MONTH: {
			if (mode == 1) {
				cal.set(Calendar.DAY_OF_MONTH, 1);
			} else if (mode == -1) {
				cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
			}
			break;
		}
		default: {

		}
		}

		// date mode starting from first sec
		if (mode == 1) {
			cal2.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
			return cal2.getTime();
			// date mode ending till last sec
		} else if (mode == -1) {
			cal2.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), 23, 59, 59);
			return cal2.getTime();
		}
		// if is mode is wrong
		else
			return date;
	}

	/**
	 * Checks if the date has previous & next date in the records
	 * (ProjectResource); buttons' settings Function based on function from
	 * DBRecordsStatisticsView.
	 */
	private void checkDates() {

		Date currPrevDate = null, currNextDate = null;
		reductionChartComboSelection = (StructuredSelection) reductionChartDisplayCombo.getSelection();

		switch ((ReductionDisplayMode) reductionChartComboSelection.getFirstElement()) {
		case DAY: {

			cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) - 1);
			currPrevDate = cal.getTime();

			cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 2);
			currNextDate = cal.getTime();

			break;
		}
		case MONTH: {

			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - 1);
			currPrevDate = cal.getTime();

			cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + 2);
			currNextDate = cal.getTime();

			break;

		}
		default:
			break;
		}

		// buttons' settings
		if (!reductionChartDisplayCombo.getCombo().getItem(reductionChartDisplayCombo.getCombo().getSelectionIndex())
				.equals("DAY")
				& !reductionChartDisplayCombo.getCombo()
						.getItem(reductionChartDisplayCombo.getCombo().getSelectionIndex()).equals("MONTH")) {
			previousDateButton.setEnabled(false);
			nextDateButton.setEnabled(false);
		} else {

			// PREVOUS DATE
			// first date of the Date interval
			cal.setTime(convertDate(currPrevDate, 1));
			// last date of the Date interval
			cal2.setTime(convertDate(currPrevDate, -1));

			if (reductionChartDisplayCombo.getCombo().getItem(reductionChartDisplayCombo.getCombo().getSelectionIndex())
					.equals("MONTH")) {
				cal2.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
			}

			// check in DB
			if (projectResources.checkDateRecords(cal.getTime(), cal2.getTime())) {
				previousDateButton.setEnabled(true);
				projectResources.setReductionChartPrevDate(currPrevDate);
			} else
				previousDateButton.setEnabled(false);

			// NEXT DATE
			// firstDate
			cal.setTime(convertDate(currNextDate, 1));
			// lastDate
			cal2.setTime(convertDate(currNextDate, -1));

			if (reductionChartDisplayCombo.getCombo().getItem(reductionChartDisplayCombo.getCombo().getSelectionIndex())
					.equals("MONTH")) {
				cal2.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
			}
			// check in DB
			if (projectResources.checkDateRecords(cal.getTime(), cal2.getTime())) {
				nextDateButton.setEnabled(true);
				projectResources.setReductionChartNextDate(currNextDate);
			} else
				nextDateButton.setEnabled(false);
		}
	}

	/**
	 * Moderates widgets in current state.
	 * 
	 * @param newProject
	 *            occurs
	 */
	private void cleanFields(boolean newProject) {

		if (newProject) {

			reductionChartDisplayCombo.getCombo().setEnabled(false);
			reductionChartDisplayCombo.getCombo().select(0);
			previousDateButton.setEnabled(false);
			nextDateButton.setEnabled(false);
			displayNewRecordsButton.setEnabled(false);
			insertIntoDBButton.setEnabled(false);

			recordQuantityText.setText("");
			reductionPeaksQuantityText.setText("");
			totalPowerAmountText.setText("");
			savedPowerAmountText.setText("");
			totalCostText.setText("");
			savedCostText.setText("");

			try {
				reductionRecordsTableViewer.getTable().removeAll();
			} catch (Exception e) {

			}
		}

		// new cutted chart
		generateButton.setEnabled(false);
		displayNewRecordsButton.setSelection(false);

	}

	/**
	 * Deletes needed diagrams Function based on function from
	 * DBRecordsStatisticsView.
	 * 
	 */
	private void cleanChart() {

		powerReductionChart.getLegend().setVisible(false);

		deleteSeries("Energy");
		deleteSeries("CutLine");
		deleteSeries("CutPeaks");
		deleteSeries("Power Reduction");
		deleteSeries("Project");
		deleteSeries("Old max value "+projectResources.getMax()/1000);
		deleteSeries("minLine");

	}

	private void deleteSeries(String seriesName) {
		try {
			powerReductionChart.getSeriesSet().deleteSeries(seriesName);

		} catch (IllegalArgumentException e) {
			System.err.println(e.getStackTrace());
		} catch (NullPointerException e1) {
			System.err.println(e1.getStackTrace());
		}
	}

	@Override
	public void setFocus() {
		reductionRecordsTableViewer.getControl().setFocus();
	}

	@Override
	public void dispose() {
		super.dispose();
		reduceImage.dispose();
		increaseImage.dispose();
		newRecordFont.dispose();
		oldRecordFont.dispose();
		colorReduciton.dispose();
		colorIncrease.dispose();
		colorNeutral.dispose();
	}

}
