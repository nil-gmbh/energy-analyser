package nilgmbh.ea.ui.e3;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import database.Querty;
import de.nilgmbh.ea.ui.e3.views.DBRecordsStatisticsView;
import de.nilgmbh.ea.ui.e3.views.DataImportView;

public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "de.nil-gmbh.ea.ui.e3";
	
	private static Activator plugin;

	@Override
	public void start(BundleContext context) throws Exception {

		super.start(context);
		DataImportView.message = DataImportView.stepsList.get(0);

		Querty.connectToDB();
		DBRecordsStatisticsView.loadProjectsNames();
		plugin = this;
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		// TODO Auto-generated method stub
		super.stop(context);
		plugin = null;
	}

	@Override
	protected void initializeImageRegistry(ImageRegistry reg) {
		super.initializeImageRegistry(reg);

		reg.put("increase", ImageDescriptor.createFromURL(
				FileLocator.find(getDefault().getBundle(), new Path("icons/increase_icon_small.png"), null)));
		reg.put("reduce", ImageDescriptor.createFromURL(
				FileLocator.find(getDefault().getBundle(), new Path("icons/reduction_icon_small .png"), null)));

	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path
	 *
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
}
