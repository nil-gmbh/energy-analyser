package nilgmbh.ea.ui.e3;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import de.nilgmbh.ea.ui.e3.views.DBRecordsStatisticsView;
import de.nilgmbh.ea.ui.e3.views.DataImportView;
import de.nilgmbh.ea.ui.e3.views.PowerReductionView;

public class Perspective implements IPerspectiveFactory {

	public static final String ID = Perspective.class.getName();

	// private static IFolderLayout views;
	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(true);
		layout.setFixed(false);
		
		IFolderLayout stackedFolder = layout.createFolder("stacked-views", IPageLayout.TOP, 1.0f, editorArea);
//		stackedFolder.addPlaceholder(DataImportView.ID);
		stackedFolder.addView(DataImportView.ID);
//		stackedFolder.addPlaceholder(DBRecordsStatisticsView.ID);
		stackedFolder.addView(DBRecordsStatisticsView.ID);
//		stackedFolder.addPlaceholder(PowerReductionView.ID);
		stackedFolder.addView(PowerReductionView.ID);

//		layout.addStandaloneView(DBRecordsStatisticsView.ID, true, IPageLayout.TOP, 1.0f, editorArea);
//		layout.getViewLayout(DBRecordsStatisticsView.ID).setCloseable(false);
//
//		layout.addStandaloneView(DataImportView.ID, true, IPageLayout.RIGHT, 0.5f, DBRecordsStatisticsView.ID);
//		layout.getViewLayout(DataImportView.ID).setCloseable(false);
//		
//		layout.addStandaloneView(PowerReductionView.ID, true, IPageLayout.BOTTOM, 0.25f, DataImportView.ID);
//		layout.getViewLayout(PowerReductionView.ID).setCloseable(false);
////		
		

	}
}
